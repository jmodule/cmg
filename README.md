# NAME

cmg - Character Manager for GURPS

# SUMMARY

A collection of python scripts that allow web entry and display of
GURPS character sheets.  Eventually will support exporting the
characters as XML files for OpenRPG1.

The two main reasons for this project is that the venerable GCA is no
longer free, and I wanted something accessible to Internet players
that stored data in a central location.

# REQUIREMENTS

- Python 2.3
- MySQL
- Apache or a similar web server (ideally with mod\_python)
- Cheetah - Python-based templates
- SQLObject - SQL object model for Python
- jmodule - a custom python library for web development

# TODO

1. Deal with attributes or skills that are modified by an advantage or
disadvantage.
2. Add a skill/trait improvement page where you can add points to your character,
i.e. buy skill levels.
3. Complete the XML export by adding the missing fields: unused points, age,
weight, parry, block, kick, luck(?), armor.
4. Need to add fields to specify what weapons are being used to parry, and if
there is a shield for blocking.  These need to be entered by the user, but
should have a drop-down or other box to select from.  Also they would be the
last steps of entering the character and may work best on a separate page.
5. Account for Racial packages, where 75 points, for example, purchases
advantages, attributes, skills, etc., at a discount.  Probably best handled as
a cost where you add the package cost and then check if you need to subtract
levels from a trait before calculating it's cost.

# DATABASE RELATIONSHIPS

TRAIT MASTER

The trait master table contains the basic attributes, plus advantages and
disadvantages.  Each has a base point cost and an incremental cost for each
level bought.  The levels are stored in the trait character table.  The key is
10 characters long and should be based on the trait name.  The depends-on
field would be used for requiring prerequisites, but is optional for now.

SKILL MASTER

Skills have an auto-generated ID.  Each skill has a trait ID and difficulty
code, normally the trait will be either DX or IQ, but there are a few skills
which use ST or HT, so I had to make it open.  The difficulty code can be
(e)asy, (h)ard, or (v)ery hard.  For prerequisites, you have the
requires\_table field and requires\_id field to link the skill to either another
skill or a trait record.  So far this has not been implemented either.

CHARACTER INFO

Straight forward table with simple text fields covering the name,
description, etc.

CHARACTER QUIRKS

The quirk key is made up of a character id and auto-generated number.
The description is the meat of the quirk, and since all quirks are
worth -1 points, it wasn't necessary to have a level field.

CHARACTER SKILLS

This just links a character id with a skill id and holds the skill
level.  One thing to note is that the level is going to be +1, +2,
etc., and the target number for rolling against is a calculated value.

CHARACTER TRAITS

This also just links a character with a trait, and holds the level.

# CREDITS

Animated clip art from the free collection at:
<http://www.jamsarts.com/>

# AUTHOR

Jakim Friant <jakim@friant.org>

# CHANGES

See the log in the CHANGES file.
