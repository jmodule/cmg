#!/usr/bin/env python
"""Combat simulator for CMG

Ideally this will become a way to resolve combat between two or more
characters, including NPC's.

"""
import datetime
from cli import StandardController
from . import view

import site
from pathlib import Path

here = Path(__file__).parent
cmg_path = here.joinpath(Path("../../cmg/")).resolve()
site.addsitedir(str(cmg_path))

import model
from dice import Dice
import const

m_dashes = '-' * 79
m_prompt = m_dashes + "\n%s (0-%d,q): "
m_pause = "Press <enter> to continue..."
m_comment = "Enter a comment or <enter> to finish: "


def getWeapons(char):
    wpn_categ = [c.id for c in model.category.select(model.category.q.groupName == 'combat')]
    wpn_list = []
    for item in char.myInv:
        if item.eqpt.categoryID in wpn_categ:
            wpn_list.append(item)
    return wpn_list


def weaponDamage(char):
    """Select and print the damage a weapon does"""
    wpn_list = getWeapons(char)

    finished = False
    while not finished:
        print(m_dashes)
        for i, wpn in enumerate(wpn_list):
            print("%d: %s" % (i, wpn.eqpt.description))
        which = input(m_prompt % ("Select a weapon", len(wpn_list) - 1))
        if which.lower() == 'q':
            finished = True
        else:
            try:
                which = int(which)
            except ValueError:
                which = -1

            if which in range(len(wpn_list)):
                dice = Dice(wpn_list[which].dmgDice())
                dice.roll()
                print()
                print(wpn_list[which].eqpt.description, dice, "points of damage")
                print()
                input(m_pause)


def skillRoll(char):
    """Select and 'roll' against a skill and print the result"""
    all_skills = char.getAllSkills()
    skill_list = list(all_skills.keys())
    skill_list.sort()

    finished = False
    while not finished:
        print(m_dashes)
        for i, skill in enumerate(skill_list):
            print("%d: %s [%s]" % (i, skill, all_skills[skill]))
        which = input(m_prompt % ("Select a skill", len(skill_list) - 1))

        if which.lower() == 'q':
            finished = True
        else:
            try:
                which = int(which)
            except ValueError:
                which = -1
            if which in range(len(skill_list)):
                mod = input("Enter a modifier [0]: ")
                try:
                    mod = int(mod)
                except ValueError:
                    mod = 0
                dice = Dice("3d6", all_skills[skill_list[which]], modifier=mod)
                dice.roll()
                print()
                print("%s, rolled %s, and %s." % (skill_list[which],
                                                  dice,
                                                  dice.resultMsg()))
                print()
                comment = input(m_comment).strip()
                if comment != "":
                    today = datetime.datetime.now()
                    model.log(
                        charID=char.id,
                        categ=const.log_id_roll,
                        logtime=today,
                        skill=skill_list[which],
                        level=all_skills[skill_list[which]],
                        roll=dice.result(),
                        comment=comment)
                    print("Saved to log")
                    input(m_pause)


class RollController(StandardController):

    def __init__(self):
        super().__init__()
        records = model.charInfo.select(model.charInfo.q.status == True,
                                        orderBy=model.charInfo.q.charName)
        call_backs = {'ok': self.resolve,
                      'select': self.select,
                      'get_char': self.getCharData,
                      }
        self.screen = view.RollView(call_backs, char_list=records)
        self.commands = {'f1': self.screen.help,
                         'f2': self.save,
                         'f5': self.mode,
                         }
        self.skill_mode = True

    def resolve(self, button):
        """Resolve the die roll"""
        pass

    def select(self, button):
        # *FIXME* ignoring the exception for now
        char = model.charInfo.get(button.char_id)

        if self.skill_mode:
            self.skillRoll(char)
        else:
            self.weaponDamage(char)

    def mode(self):
        """Toggle the mode"""
        self.skill_mode = not self.skill_mode
        self.screen.showMode(self.skill_mode)

    def getCharData(self, char_id):
        info = model.charInfo.get(char_id)
        if self.skill_mode:
            all_skills = info.getAllSkills()
            data = list(all_skills.keys())
            data.sort()
        else:
            data = []
            for d in getWeapons(info):
                data.append(d.eqpt.description)
        return data

    def save(self):
        pass
