"""Decision view using urwid"""

import urwid
import urwid.curses_display

from cli import StandardScreen
from cli import dialog

M_title = "CLI Rolls"
M_help = """

Skill and Combat Rolls.
"""

blank = urwid.Text("")


class RollView(StandardScreen):
    header_text = ('header',
                   ['Decision      ',
                    ('key', 'F1'), ' help  ',
                    ('key', 'F2'), ' save  ',
                    ('key', 'ESC'), ' exit  ',
                    ]
                   )

    def __init__(self, callbacks, *args, **kwargs):
        """Initialize the screen elements

        Keywords:
          char_list = list of CharInfo records

        """
        super().__init__(*args, **kwargs)
        self.callbacks = callbacks # save for later
        self.btn_ok = urwid.Button("Roll", callbacks['ok'])
        self.btn_select = urwid.Button("Select", callbacks['select'])

        if 'char_list' in kwargs:
            char_list = kwargs['char_list']
        else:
            char_list = []

        self.char_radio_list = []

        left = []
        for info in char_list:
            w = urwid.RadioButton(self.char_radio_list, info.charName, False, self.onCharChange)
            # this will change in urwid 0.9.8 to being an argument above
            w.char_id = info.id
            left.append(w)

##       left.append(urwid.Padding(urwid.AttrWrap(self.btn_select, 'button'),
##                                   'center',
##                                   ('relative', 26)))
        self.right = [blank,]
##         right.append(urwid.Padding(urwid.AttrWrap(self.btn_ok, 'button'),
##                                   'center',
##                                   ('relative', 23)))
        
        self.contents = [left, self.right]
        self.createScreen('split')

    def onCharChange(self, radio_button, new_state, user_data=None):
        """Change the character info on the right panel"""
        if user_data is None:
            data_list = self.callbacks['get_char'](radio_button.char_id)
        else:
            data_list = self.callbacks['get_char'](user_data)
        self.data_radio_list = []
        self.right = [blank, ]
        for d in data_list:
            w = urwid.RadioButton(self.data_radio_list, d, False)
            self.right.append(w)
        
    def help(self):
        pass
