"""Decision view using urwid"""

import urwid
import urwid.curses_display

from cli import StandardScreen
from cli import dialog

M_title = "CLI Decision"
M_help = """

========================
The Basic Matrix Game
By Chris Engle

Programming by J.Friant
========================
"""

blank = urwid.Text("")


class DecisionView(StandardScreen):
    header_text = ('header',
                   ['Decision      ',
                    ('key', 'F1'), ' help  ',
                    ('key', 'F2'), ' save  ',
                    ('key', 'ESC'), ' exit  ',
                    ]
                   )

    def __init__(self, ok_callback):
        """Initialize the screen elements"""
        super().__init__()
        self.arguments = [
            [ urwid.Edit(('editcp', "pro:"), ""),
              urwid.IntEdit(default="3") ],
            [ urwid.Edit(('editcp', "con:"), ""),
              urwid.IntEdit(default='3') ],
            [ urwid.Edit(('editcp', "alt:"), ""),
              urwid.IntEdit() ],
            ]

        self.results = urwid.Text("")

        self.btn_ok = urwid.Button("Decide", ok_callback)

        for row in self.arguments:
            self.contents.append(
                urwid.Columns([urwid.AttrWrap(row[0], 'editbx', 'editfc'),
                               ('fixed', 2, urwid.AttrWrap(row[1], 'editbx', 'editfc'))], 1))
            self.contents.append(urwid.Divider())

        self.contents.extend([
            urwid.Padding(urwid.AttrWrap(self.btn_ok, 'button'),
                          'center', ('relative', 14)),
            urwid.Divider('-', 1, 0),
            urwid.Text(('editcp', "Results:")),
            urwid.Divider(),
            self.results,
            ])
        
        self.createScreen()

    def help(self):
        """Display a help dialog"""

        d = dialog.do_msgbox(M_title + M_help, 22, 0)
        d.main(self.ui)

    def getArguments(self):
        """Return a list of tuples with the argument and strength"""
        arg_list = []
        for a in self.arguments:
            text = a[0].get_edit_text()
            if text.strip() != "":
                arg_list.append((a[0].caption,
                                 text,
                                 a[1].value()))
        return arg_list

    def update(self, results):
        """Report the results of the arugments"""
        self.results.set_text(('editbx', "\n".join(results)))
