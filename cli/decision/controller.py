#!/usr/bin/env python

"""A console based (or cli) interface to the decision/matrix game database"""

import datetime
import argument
import const
import model
from cli import StandardController
from . import view


class DecisionController(StandardController):
    """Displays the screen and handles user input"""

    def __init__(self):
        """Create the screen and set up the command hash"""
        super().__init__()
        self.screen = view.DecisionView(self.resolve)
        self.commands = {'f1': self.screen.help,
                         'f2': self.save,
                         }
        self.arg_list = {}
        self.msg = []

    def resolve(self, button):
        """Resolve the arguments"""
        self.msg = []
        self.arg_list.clear()
        # first convert the list to a hash of Argument objects
        for a in self.screen.getArguments():
            self.arg_list[a[0]] = argument.Argument(a[1], a[2])
        keys = list(self.arg_list.keys())
        if len(keys) > 1:
            keys.sort()
            if not argument.decide(self.arg_list):
                self.msg.append("Unable to come to a decision!")

            for k in keys:
                if k in self.arg_list:
                    self.msg.append(str(self.arg_list[k]))

        self.screen.update(self.msg)

    def save(self):
        today = datetime.datetime.now()
        for arg in list(self.arg_list.values()):
            model.log(
                charID=None,
                categ=const.log_id_decision,
                logtime=today,
                skill=today.strftime("%Y%m%dT%H%M%S"),
                level=arg.strength,
                roll=arg.result,
                comment=arg.argument)

        self.msg.append("Saved to log database")
        self.screen.update(self.msg)
        self.arg_list.clear()
