"""The Command Line Interface module with controller and views for Urwid

These classes handle the standard commands and have the main loop

Both should be used as parent classes and be inherited from by the
classes in view.  For example:

class City(StandardScreen)
class Crafts(StandardDialog)

"""
import urwid
import urwid.curses_display

# regular color scheme (blue bg, light gray text)
standard_palette = [
    ('background', 'light gray', 'dark blue'),
    ('plain', 'light gray', 'dark blue'),
    ('bright', 'white', 'dark blue'),
    ('button', 'dark blue', 'light gray'),
    ('editbx', 'yellow', 'dark blue'),
    ('editcp', 'light gray', 'dark blue'),
    ('editfc', 'white', 'black'),
    ('editbox', 'white', 'dark blue'),
    ('focus', 'white', 'dark blue'),
    ('header', 'black', 'dark cyan'),
    ('title', 'light blue', 'black'),
    ('key', 'light cyan', 'dark blue'),
]

# alternate color scheme
alternate_palette = [
    ('background', 'light gray', 'black'),
    ('plain', 'light gray', 'black'),
    ('bright', 'white', 'black', ('bold', 'standout')),
    ('button', 'dark blue', 'light gray', 'standout'),
    ('editbx', 'white', 'black'),
    ('editcp', 'yellow', 'black'),
    ('editfc', 'white', 'dark cyan', 'bold'),
    ('editbox', 'white', 'black'),
    ('focus', 'white', 'dark cyan', 'bold'),
    ('header', 'white', 'dark blue', 'standout'),
    ('title', 'light blue', 'black', 'bold'),
    ('key', 'light cyan', 'dark blue', 'underline'),
]

dlg_h = 22
dlg_w = 76

num_len = 3
name_len = 20

M_welcome = "Welcome the the CMG CLI (Press ESC or CTRL+Q to exit)"


class StandardController:
    """A prototype controller with the main loop"""

    def __init__(self, this_screen=None):
        self.screen = this_screen  # a StandardScreen class or child Class
        self.commands = {}  # a dictionary mapping keys to commands

    def run(self):
        size = self.screen.ui.get_cols_rows()

        while True:
            self.screen.draw_screen(size)
            keys = self.screen.ui.get_input()
            print(repr(keys))
            if 'esc' in keys or 'ctrl q' in keys:
                break
            for k in keys:
                if k == 'window resize':
                    size = self.screen.ui.get_cols_rows()
                    continue
                if k in self.commands:
                    self.commands[k]()
                else:
                    if self.screen.top.selectable():
                        self.screen.top.keypress(size, k)
        self.on_exit()

    def on_exit(self):
        """Blank function for things that need to be done before exiting"""
        pass

    def main(self):
        """Create the curses display and then run the main loop"""
        self.screen.main()
        return self.screen.ui.run_wrapper(self.run)


class StandardScreen:
    """A prototype screen with some basic initialization"""

    def __init__(self, *args, **kwargs):
        self.top = None  # the main window widget
        self.palette = standard_palette  # standard colors
        if 'contents' in kwargs:
            self.contents = kwargs['contents']
        else:
            self.contents = []
        if 'header_text' in kwargs:
            self.header_text = kwargs['header_text']
        else:
            self.header_text = ''
        self.header = None
        self.status = None
        self.footer = None

    def createScreen(self, style='list'):
        """Initialize the standard elements of a screen"""
        global M_welcome
        self.header = urwid.AttrWrap(urwid.Text(self.header_text), 'header')
        self.status = urwid.Text(M_welcome)
        self.footer = urwid.AttrWrap(self.status, 'header')

        if style == 'list':
            w = urwid.ListBox(self.contents)
        elif style == 'split':
            parts = [urwid.ListBox(self.contents[0]),
                     urwid.ListBox(self.contents[1])]
            w = urwid.Columns(parts, 1)

        w = urwid.Padding(w, 'center', 78)
        w = urwid.AttrWrap(w, 'background')
        self.top = urwid.Frame(w, self.header, footer=self.footer)

    def main(self, ui=None):
        if ui is None:
            self.ui = urwid.curses_display.Screen()
        else:
            self.ui = ui
        self.ui.register_palette(self.palette)

    def draw_screen(self, size):
        canvas = self.top.render(size, focus=True)
        self.ui.draw_screen(size, canvas)


class StandardDialog(StandardScreen):
    """A prototype dialog class with slightly different default keys"""

    def run(self):
        """This is the main loop of the dialog"""

        size = self.ui.get_cols_rows()
        okay = False
        while not okay:
            self.draw_screen(size)
            keys = self.ui.get_input()
            for k in keys:
                k = k.lower()
                if k == 'window resize':
                    size = self.ui.get_cols_rows()
                    continue
                if k == 'enter' or k == 'esc':
                    okay = True
                    continue
                self.top.keypress(size, k)
