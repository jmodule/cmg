#!/usr/bin/env python3

import argparse
import getpass
import sys

from jmodule.utils import debug
from jmodule.web.user import *
from jmodule.db import passwords

def main():
    """Prompt for user info and save it to the user file."""

    parser = argparse.ArgumentParser()

    parser.add_argument('filename')

    args = parser.parse_args()

    user_list = cPasswordFile()
    user_list.load(args.filename)

    user_rec = cUserRec()
    user_name = ''

    okay = 0
    while not okay:
        try:
            user_name = input('Enter a user name: ')
            if user_name == '':
                break
            user_rec.setUsername(user_name)
            user_list.checkDuplicate(user_name)
            okay = 1
        except Exception as val:
            debug.report('Error', 'main', val)
    if user_name == '':
        sys.exit(1)

    okay = 0
    while not okay:
        pwd_in = getpass.getpass('Enter a password: ')
        if pwd_in == '':
            sys.exit(1)
        pwd_again = getpass.getpass('Enter the password again: ')
        if pwd_in == pwd_again:
            try:
                user_rec.encryptPassword(pwd_in)
                okay = 1
            except Exception as val:
                debug.report('Error', 'main', val)
        else:
            print('Passwords did not match, try again.')

    okay = 0
    group = ''
    while not okay:
        try:
            while group not in passwords.logins.keys():
                group = input('Enter a group, (A)dmin, (R)estricted, or (G)uest: ')
                if group == '':
                    break
            if group == '':
                break
            user_rec.setAccess(group)
            okay = 1
        except Exception as val:
            debug.report('Error', 'main', val)
    if group == '':
        sys.exit(1)

    user_list.add(user_rec)
    user_list.save(args.filename)


if __name__ == "__main__":
    main()
