#!/usr/bin/env python
#$Id: cmglog.py,v 1.5 2007-02-09 20:27:17 jfriant Exp $
"""Quick (sort of) way to list the log entries by category."""

import sys
from pathlib import Path
import site
import sqlobject as db

cmg_path = Path('./cmg').resolve()
site.addsitedir(str(cmg_path))
site.addsitedir(".")

from cli.decision.controller import DecisionController

from model.connect import (init_db, load_db_config)
import model

show_all = True
show_decisions = False
show_rolls = False
show_points = False

init_db(load_db_config(Path('./test-cmg.conf')), db_driver='pymysql')

for arg in sys.argv:
    if arg == '-d':
        show_all = False
        show_decisions = True
    elif arg == '-r':
        show_all = False
        show_rolls = True
    elif arg == '-p':
        show_all = False
        show_points = True

if show_decisions or show_all:
    # print the last 10 decisions
    #
    categ_list = model.log.select(model.log.q.categID == "dec",
                                  orderBy=-model.log.q.logtime)

    print("========== Decision (in reverse) ==========")
    result = ""
    hold = ""
    first = True
    for rec in categ_list[:10]:
        if hold != rec.skill:
            if first:
                first = False
            else:
                print("---")
            hold = rec.skill
        if rec.roll <= rec.level:
            result = " Y)"
        else:
            result = " N)"
        print(result, rec.comment)

if show_rolls or show_points or show_all:
    # now print the roll results and spent points
    #
    out_hdr = """
Date Stamp       Character          Skill                          Level Roll
---------------- ------------------ ------------------------------ ----- -----"""
    out_fmt = "%-16s %-18s %-30s   %3s   %3s"

    categ_list = model.category.select(db.OR(model.category.q.id == "rol", model.category.q.id == "spp"))

    for categ in categ_list:
        print("\n==========", categ.description, "==========")
        print(out_hdr)
        for rec in model.log.select(db.AND(model.log.q.categID == categ.id, model.log.q.charID == model.charInfo.q.id), orderBy=[model.log.q.logtime, model.log.q.id]):
            print(out_fmt % (str(rec.logtime)[:16], rec.char.charName, rec.skill, rec.level, rec.roll))
