#!/usr/bin/env python
"""Combat simulator for CMG

Ideally this will become a way to resolve combat between two or more
characters, including NPC's.

"""

from pathlib import Path
import site

cmg_path = Path('./cmg').resolve()
site.addsitedir(str(cmg_path))
site.addsitedir(".")

import datetime
import sys

from model.connect import (init_db, load_db_config)

import const
import model
from dice import Dice

m_dashes = '-' * 79
m_prompt = m_dashes + "\n%s (0-%d,q): "
m_pause = "Press <enter> to continue..."
m_comment = "Enter a comment or <enter> to finish: "


class MenuItem:
    def __init__(self, desc, cmd):
        """Set up the menu item with a description and function pointer"""
        self.description = desc
        self.command = cmd


def getWeapons(char):
    wpn_categ = [c.id for c in model.category.select(model.category.q.groupName == 'combat')]
    wpn_list = []
    for item in char.myInv:
        if item.eqpt.categoryID in wpn_categ:
            wpn_list.append(item)
    return wpn_list


def weaponDamage(char):
    """Select and print the damage a weapon does"""
    wpn_list = getWeapons(char)

    finished = False
    while not finished:
        print(m_dashes)
        for i, wpn in enumerate(wpn_list):
            print("%d: %s" % (i, wpn.eqpt.description))
        which = input(m_prompt % ("Select a weapon", len(wpn_list) - 1))
        if which.lower() == 'q':
            finished = True
        else:
            try:
                which = int(which)
            except ValueError:
                which = -1
            
            if which in range(len(wpn_list)):
                dice = Dice(wpn_list[which].dmgDice())
                dice.roll()
                print()
                print(wpn_list[which].eqpt.description, dice, "points of damage")
                print()
                input(m_pause)


def skillRoll(char):
    """Select and 'roll' against a skill and print the result"""
    all_skills = char.getAllSkills()
    skill_list = sorted(all_skills.keys())

    finished = False
    while not finished:
        print(m_dashes)
        for i, skill in enumerate(skill_list):
            print("%d: %s [%s]" % (i, skill, all_skills[skill]))
        which = input(m_prompt % ("Select a skill", len(skill_list) - 1))

        if which.lower() == 'q':
            finished = True
        else:
            try:
                which = int(which)
            except ValueError:
                which = -1
            if which in range(len(skill_list)):
                mod = input("Enter a modifier [0]: ")
                try:
                    mod = int(mod)
                except ValueError:
                    mod = 0
                dice = Dice("3d6", all_skills[skill_list[which]],modifier=mod)
                dice.roll()
                print()
                print("%s, rolled %s, and %s." % (skill_list[which],
                                                  dice,
                                                  dice.resultMsg()))
                print()
                comment = input(m_comment).strip()
                if comment != "":
                    today = datetime.datetime.now()
                    model.log(
                        charID = char.id,
                        categ = const.log_id_roll,
                        logtime = today,
                        skill = skill_list[which],
                        level = all_skills[skill_list[which]],
                        roll = dice.result(),
                        comment = comment)
                    print("Saved to log")
                    input(m_pause)


def main():
    init_db(load_db_config(Path('./test-cmg.conf')), db_driver='pymysql')
    menu = (MenuItem("Weapon Damage", weaponDamage),
            MenuItem("Skill Roll", skillRoll))

    records = model.charInfo.select(model.charInfo.q.status == True,
                                    orderBy = model.charInfo.q.charName)
    while True:
        char = None
        while char is None:
            print(m_dashes)
            for i, rec in enumerate(records):
                print("%d: %s" % (i, rec.charName))
            which = input(m_prompt % ("Select a character",
                                      len(list(records)) - 1))

            if which.lower() == 'q':
                sys.exit()

            try:
                which = int(which)
            except ValueError:
                which = -1

            if which in range(len(list(records))):
                char = records[which]
            else:
                char = None

            print(m_dashes)
            for i, item in enumerate(menu):
                print("%d: %s" % (i, item.description))
            which = input(m_prompt % ("Enter choice", len(menu) - 1))

            if which.lower() == 'q':
                sys.exit()
            else:
                try:
                    which = int(which)
                except ValueError:
                    which = -1
                if which in range(len(menu)):
                    menu[which].command(char)


if __name__ == "__main__":
    main()
