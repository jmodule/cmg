-- 2005/08/??
-- change and update the primary keys
--alter table char_quirks drop primary key, add primary key (quirk_id);
--alter table char_skills add id int auto_increment not null, drop primary key, add primary key (id);
--alter table char_traits add id int not null auto_increment, drop primary key, add primary key (id);
--alter table inventory add id int not null auto_increment, drop primary key, add primary key (id);
--alter table traits drop primary key, change id code varchar(10) default "", add id int not null auto_increment, add primary key (id);

--2005/08/17
--alter table skills add parry float default 0.0;

--2005/08/19
--alter table char_traits change trait_id trait_code varchar(10) default '';
--alter table add trait_id int default 0;
--update char_traits,traits set traits_id = traits.id where trait_code = traits.code;
--alter table char_info add height int default 0, add weight int default 0, add sex char(1) default "";
--alter table char_info add unspent_points int default 0, add parry_id int default 0, add health int default 0;
--update char_info,char_addtl set char_info.unspent_points = char_addtl.unspent_points, char_info.parry_id = char_addtl.parry_id, char_info.health = char_addtl.health where char_info.id = char_addtl.char_id;

--2005/08/24
--alter table char_info add genre char(1) default 'a';

--2005/09/15
--alter table char_quirks add comments text default "";
--alter table char_skills add detail varchar(30) default "";
--alter table char_traits add detail varchar(30) default "";

--alter table `equipment` add `head_pd` int default '0';
--alter table `equipment` add `head_dr` int default '0';
--alter table `equipment` add `torso_pd` int default '0';
--alter table `equipment` add `torso_dr` int default '0';
--alter table `equipment` add `arm_pd` int default '0';
--alter table `equipment` add `arm_dr` int default '0';
--alter table `equipment` add `leg_pd` int default '0';
--alter table `equipment` add `leg_dr` int default '0';

--2005/09/16
--alter table inventory add `in_use` tinyint default 0;

--2005/09/20
--alter table equipment change category category_id char(3);
--alter table skills change category category_id char(3);
--alter table traits change category category_id char(3);

--2005/11/23
--alter table char_info add (fatigue int default 0);

--2005/12/05
--Adding spell information to the skill table
--alter table skills add (casting_cost int default 0, casting_time varchar(10) default '', maintain_cost int default 0, duration varchar(10) default '', notes text);

--2005/12/06
--Since I don't want to delete skills and traits from the master lists, I need
--a way to mark them as invalid or unused
--alter table skills add (invalid int(1) default 0);
--alter table traits add (invalid int(1) default 0);

--2005/12/19
--alter table char_info add (title varchar(30) default "", campaign varchar (30), created_on datetime, size int(2) default 0, tl int(2) default 0, religion varchar(30) default "", hair varchar(15) default "", eyes varchar(15) default "", skin varchar(15) default "", handed char(1) default 'R');

--2005/12/20
--alter table char_traits add (racial_discount int default 0);
--alter table char_info add (profession varchar(30) default '');

--2005/12/26
--alter table char_info add (status int(1) default '1');
--alter table equipment add (str_for_rng int(1) default '0');
--alter table equipment change max_rng max_rng float default 0.0;
--alter table equipment change half_dmg half_dmg float default 0.0;

--2005/12/28
--alter table log change categ_id categ_id char(3);

--2006/02/01
--alter table skills add (college varchar(30) default "");

--2006/02/12
--alter table equipment add (weapon_skills_id int);

--2006/02/23
--alter table char_info add (block_id int default 0);

--2006/03/06
--alter table category add (group_name varchar(10) default "");

--2007/01/23
--alter table equipment add (show_on_inv int(1) default 1);
