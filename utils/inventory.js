// -*-java-*-
// This has helper functions for inventory maintenance
//
function doEdit()
{
    // get the variables we need
    //
    inv_idx = document.inventory.sel_inventory.selectedIndex;
    item_name = document.inventory.sel_inventory[inv_idx].text;
    //alert("index,name:" + inv_idx + ", " + item_name);
    session_id = document.inventory.session_id.value;
    char_id = document.inventory.id.value;
    
    // display the edit window
    //
    inv_win = window.open('inv_item_edit.py?char_id='+char_id+'&eqpt_name='+item_name+'&session_id='+session_id, 'Edit Inventory Item', 'toolbar=no,scrollbars=yes,width=512,height=320');
    inv_win.focus();
}
    

