//-*- Java -*-
//
// Utilities to help populate the contest of skills page.
//
// See <http://www.quirksmode.org/js/options.html> for information on
// dynamically populating a select object.
//
// See <http://www.w3schools.com/dom/default.asp> and
// <http://www.w3schools.com/htmldom/default.asp> for information on
// the JS DOM for HTML and XML.
//

function getOpSkills(op_field)
{
    let ajax = new Ajax();
    let skill_list = document.getElementById('sel_op_skill');

    if (skill_list) {
        let session_id = document.getElementById('session_id').value;;
        let id = op_field.options[op_field.selectedIndex].value;

        if (id !== "None") {
            skill_list.options[0] = new Option("fetching...");
            let data = 'session_id=' + session_id + '&id=' + id;
            ajax.post("update_contest.py", data, updateOpSkills);
        }
    }
}

function updateOpSkills(xmldoc)
{
    // For example:
    //  <skill id="s,47">Breathe Air</skill>
    //  node.nodeName = "skill"
    //  node.childNodes[0].nodeValue = "Breathe Air"
    //  node.getAttribute("id") = "s,47"
    let sel_skills = document.getElementById('sel_op_skill');
    try {
        sel_skills.options.length = 1
        sel_skills.options[0] = new Option("Select a skill...", "None")
        let root = xmldoc.getElementsByTagName('skill_list').item(0);
        let cnt = 1;
        for (let iNode = 0; iNode < root.childNodes.length; iNode++) {
            let node = root.childNodes.item(iNode);
            if (node.nodeType === 1 && node.childNodes.length > 0) {
                sel_skills.options[cnt] = new Option(node.childNodes[0].nodeValue, node.getAttribute("id"));
                cnt++;
            }
        }
    } catch (e) {
        sel_skills.options[0] = new Option('Error');
        alert(e);
    }
    sel_skills.selectedIndex = 0;
}
