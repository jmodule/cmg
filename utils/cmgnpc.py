#!/usr/bin/env python
"""NPC Generator for use with CMG

Creates a generic NPC using the GURPS random characters rules.  The
user is then given the option to save the new character to the
database or to just print out a record sheet.

Usage: cmgnpc.py

"""

import os
import random
import re
import site
from pathlib import Path

import sqlobject

cmg_path = Path('./cmg').resolve()
site.addsitedir(str(cmg_path))
site.addsitedir(".")

from model.connect import (init_db, load_db_config)

import model
import rules
from dice import Dice

m_dashes = '-' * 79
m_prompt = m_dashes + "\n%s (0-%d,q): "
m_pause = "Press <enter> to continue..."
m_comment = "Enter a comment or <enter> to finish: "

debug = False

re_skill = re.compile("(.*?)\((\w+)\)")

class CharGen:
    """Creates the temporary Character record in memory"""

    def __init__(self, name="", genre="a", use_db = True):
        self.name = name
        self.genre = genre
        self.traits = dict()
        self.skills = dict()

        self.cnt = 0
        self.max = 0

        self.use_db = use_db
    
        self.create()

    def _twoMore(self):
        self.max += 2

    def addTrait(self, is_func, name, level=1):
        """Add the selected trait to the list if it is new

        There is also the option that it is not a trait, but is
        instead the function to add to the counter.

        """
        if not is_func:
            if not name in self.traits:
                self.traits[name] = level
                self.cnt += 1
            else:
                self.traits[name] += 1
        else:
            name()

    def addSkill(self, name, genre):
        """Add the given trait to the list, return 1 if it is new"""
        global debug
        cnt = 0
        level = Dice("1d4-1")
        level.roll()
        if not name in self.skills:
            if self._checkGenre(genre):
                self.skills[name] = level.result()
                cnt = 1
            else:
                if debug: print("Wrong genre:", name)
        else:
            self.skills[name] += 1
        return cnt
        
    def genAttr(self):
        """randomly set the attribute values"""
        d = Dice('1d6+7')

        for attr in rules.attributes:
            d.roll()
            self.traits[attr] = d.result()

    def _checkGenre(self, genre):
        """See if the skill or trait is in the right genre"""
        okay = True
        if self.genre != 'a' and genre != 'g':
            if self.genre != genre:
                okay = False
        return okay

    def selTraits(self):
        """Make a random selection of traits from the master list"""
        adv = { 3: (True, self._twoMore),
                4: (False, "Voice"),
                5: (False, "Charisma", 6),
                6: (False, "Alertness", 4),
                7: (False, "Common Sense"),
                8: (False, "Magical Aptitude", 2),
                9: (False, "Acute Vision", 5),
                10: (False, "Alertness", 2),
                11: (False, "Charisma", 3),
                12: (False, "Acute Taste/Smell", 5),
                13: (False, "Danger Sense"),
                14: (False, "Attractive appearance"),
                15: (False, "Acute Hearing", 5),
                16: (False, "Appearance: Beautiful"),
                17: (True, self._twoMore),
                18: (True, self._twoMore) }

        disad = { 3: (1, self._twoMore),
                  4: (False, "Poor"),
                  5: (False, "Cowardly"),
                  6: (False, "Odious Personal Habit", 2),
                  7: (False, "Odious Personal Habit", 2),
                  8: (False, "Bad Temper"),
                  9: (False, "Unlucky"),
                  10: (False, "Greedy"),
                  11: (False, "Overconfident"),
                  12: (False, "Honest"),
                  13: (False, "Hard of Hearing"),
                  14: (False, "Appearance: Unattractive"),
                  15: (False, "Bad Sight"),
                  16: (False, "Appearance: Hideous"),
                  17: (1, self._twoMore),
                  18: (1, self._twoMore) }

        for traits in (adv, disad):
            self.cnt=0
            self.max=1
            d = Dice("3d6")
            while self.cnt < self.max:
                d.roll()
                self.addTrait(*traits[d.result()])

    def selSkills(self, n):
        """Randomly select up to n skills from the master list"""
        cnt = 0
        total = model.skill.select().count()

        while cnt < random.randint(1, n):
            s = model.skill.get(random.randint(1, total))
            if s is not None:
                cnt = self.addSkill(s.name, s.genre)

    def selSkillsTable(self, n):
        """Select up to n skills from the master list"""
        skills = { 3: ["Calligraphy", "Armoury", "Biochemistry"],
                   4: ["Botany", "Merchant", "Sleight of Hand"],
                   5: ["Diplomacy", "Physician", "Sports (Any)"],
                   6: ["Singing", "Language (Any)", "Veterinary"],
                   7: ["Animal Handling", "Bard", "Acting"],
                   8: ["Stealth", "Scrounging", "First Aid"],
                   9: ["Hand Weapon (any)", "Fast-Draw (any)", "Climbing"],
                   10: ["Hand Weapon (any)", "Traps", "Shield"],
                   11: ["Running", "Brawling", "Driving (any)", "Riding (any)"],
                   12: ["Missile Weapon (any)", "Pilot (any)", "Gunner (any)", "Swimming"],
                   13: ["Carousing", "Law", "Savoir-Faire"],
                   14: ["Gambling", "Streetwise", "Politics"],
                   15: ["Musical Instrument (any)", "Survival (any)", "Lockpicking"],
                   16: ["Forgery", "Disguise", "Mechanic"],
                   17: ["Judo", "Karate", "Naturalist", "Sex Appeal"],
                   18: ["History", "Navigation", "Poisons"] }
        d = Dice("3d6")
        cnt = 0
        while cnt < random.randint(1, n):
            d.roll()
            result = skills[d.result()]
            cnt = self.addSkill(random.choice(result), 'g')
        
    def create(self):
        """Create the character"""

        self.genAttr()
        self.selTraits()
        if self.use_db:
            self.selSkills(3)
        else:
            self.selSkillsTable(3)

    def save(self):
        """Save the character to the database"""
        record = model.charInfo(charName = self.name,
                                playerName = "GM",
                                health = int(self.traits['HT']),
                                fatigue = int(self.traits['HT']),
                                genre = self.genre,
                                status = False)

        for t in self.traits.keys():
            try:
                trait = model.trait.byName(t)
                model.charTraits(charID = record.id,
                                 level = int(self.traits[t]),
                                 traitCode = trait.code,
                                 trait = trait)
            except sqlobject.main.SQLObjectNotFound as val:
                print("No such trait:", val)

        for s in self.skills.keys():
            try:
                result = re_skill.search(s)
                if result is not None:
                    skill_name = result.group(1)
                    detail = result.group(2)
                else:
                    skill_name = s
                    detail = ""
                skill = model.skill.byName(skill_name)
                model.charSkills(charID = record.id,
                                 skill = skill,
                                 detail = detail,
                                 level = self.skills[s])
            except sqlobject.main.SQLObjectNotFound as val:
                print("No such skill:", val)

        print("Character, %s, saved as record %d" % (self.name, record.id))

    def __str__(self):
        """return a string with the text character sheet"""
        fmt_str = "\t%s [%s]%s"
        s = self.name + os.linesep
        s += "Traits:" + os.linesep
        trait_list = sorted(self.traits.keys())
        for k in rules.attributes:
            s += fmt_str % (k, self.traits[k], os.linesep)
        for k in trait_list:
            if k not in rules.attributes:
                s += fmt_str % (k, self.traits[k], os.linesep)

        s += "Skills:" + os.linesep
        for k in self.skills.keys():
            s += fmt_str % (k, self.skills[k], os.linesep)
        return s


def main():
    """Generate a random character"""
    init_db(load_db_config(Path('./test-cmg.conf')), db_driver='pymysql')

    char_name = input("Enter a character's name: ")
    genre = input("Enter the genre ('f'antasy, 's'ci-fi, 'g'eneric): ")
    usr_ans = input("Use skills from the database? ")
    if usr_ans.lower() == 'y':
        use_db = True
    else:
        use_db = False
    npc = CharGen(char_name, genre, use_db)
    print(npc)

    print("\n")
    usr_ans = input("Do you want to save this character? ")
    if usr_ans.lower() == "y":
        npc.save()


if __name__ == "__main__":
    main()
    
