#!/usr/bin/env python

"""A console based (or cli) interface to the decision/matrix game database"""

from pathlib import Path
import site

cmg_path = Path('./cmg').resolve()
site.addsitedir(str(cmg_path))
site.addsitedir(".")

from cli.decision.controller import DecisionController

from model.connect import (init_db, load_db_config)
import datetime
import argument
import const
import model


def main():
    init_db(load_db_config(Path('./test-cmg.conf')), db_driver='pymysql')

    print("""
========================
The Basic Matrix Game
By Chris Engle

Programming by J.Friant
========================
""")
    arg_list = dict()
    finished = False
    while not finished:
        print("Enter Arguments (<Enter> on blank a line to end entry)")
        for which in ('pro', 'con', 'abs'):
            print()
            arg_text = input(str(which) + ": ")
            if arg_text.strip() == "":
                break
            arg_str = input("Enter Strength (1-5): ")
            try:
                arg_str = int(arg_str)
            except ValueError:
                arg_str = 1
            arg_list[which] = argument.Argument(arg_text, arg_str)

        keys = sorted(arg_list.keys())
        if len(keys) > 1:
            if not argument.decide(arg_list):
                print("Unable to come to a decision!")

            print()
            print("Results:")
            print()
            for k in keys:
                if k in arg_list:
                    print(arg_list[k])
            print()
            can_save = input("Save to db (y/N)? ")
            if can_save[:1].lower() == 'y':
                today = datetime.datetime.now()
                for arg in arg_list.values():
                    model.log(
                        charID=None,
                        categ=const.log_id_decision,
                        logtime=today,
                        skill=today.strftime("%Y%m%dT%H%M%S"),
                        level=arg.strength,
                        roll=arg.result,
                        comment=arg.argument)

                print("Saved to log database")

        if input("Exit (n/Y)? ").lower() == 'n':
            finished = True
        else:
            arg_list.clear()


if __name__ == "__main__":
    main()
