--
-- Create the database first
--
create database cmg;

--
-- Then create the default application account
--
create user 'cmgapp'@'%' identified by 'PASSWORD_HERE';
grant all privileges on cmg.* to 'cmgapp'@'%';
