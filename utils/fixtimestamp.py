#!/usr/bin/env python

import model
import datetime

for rec in model.log.select():
    if rec.logtime is None and rec.categID == 'dec':
        print(rec.skill)
        year = int(rec.skill[0:4])
        month = int(rec.skill[4:6])
        day = int(rec.skill[6:8])
        hour = int(rec.skill[9:11])
        minute = int(rec.skill[11:13])
        second = int(rec.skill[13:15])

        print(year, month, day, hour, minute, second)

        rec.logtime = datetime.datetime(year,
                                        month,
                                        day,
                                        hour,
                                        minute,
                                        second)
