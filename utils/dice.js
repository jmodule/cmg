//-*- Java -*-
//
// Utilities to call dice rolling functions from the html page
//
function doRoll()
{
    var ajax = new Ajax();
    var element = document.getElementById('txt_result');
    if (element) { element.value = "rolling..."; }

    var form = document.getElementById('diceroller');
    var session_id = form.session_id.value;
    var die_code = form.txt_die_code.value;
    data = 'session_id=' + session_id + '&die_code=' + die_code;
    ajax.post('update_dice.py', data, getRollResults);
}

function getRollResults(xmldoc)
{
    try {
        var element = document.getElementById('txt_result');
        var node = xmldoc.getElementsByTagName('roll').item(0);
        result = node.firstChild.data;
        element.value = result;
    } catch (e) {
        element.value = "Error!\n";
        alert(e);
    }
}
