#!/usr/bin/env python

from pathlib import Path
import site

cmg_path = Path('./cmg').resolve()
site.addsitedir(str(cmg_path))
site.addsitedir(".")

from cli.decision.controller import DecisionController

from model.connect import (init_db, load_db_config)

init_db(load_db_config(Path('./test-cmg.conf')), db_driver='pymysql')
DecisionController().main()
