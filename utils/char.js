// -*- Java -*-
// This file contains java script for dynamically adding and updating 
// fields with character data
//
// Something like the following must be declared on the page that imports
// this file:
// ------------------------------------------------------------------
// trait_names = new Array("Choose a new trait...", "Night Vision")
// trait_base_cost = new Array("", "5")
// trait_incr_cost = new Array("", "0")
// trait_max = new Array("", "1")
// -------------------------------------------------------------------
//

function checkTrait(form)
{
    tidx = form.new_trait.selectedIndex;
    if (tidx != 0)
	{
            max_level = trait_max[tidx];
            
            // for the traits with no maximum we'll use 10
            //
            if ( max_level == 0 ) { max_level = 10; }
            
            for ( i=0; i<max_level; i++ )
		{
                    var newOption = document.createElement("OPTION");
                    newOption.text=i+1;
                    newOption.value=i+1;
                    form.new_level.options[i] = new Option(newOption.text, newOption.value);
		}
            form.new_level.length = i;
	}
}

function newTraitCost(which, level)
{
    cost = 0;
    if (which != 0)
        {
            cost = trait_base_cost[which] + trait_incr_cost[which] * level;
        }
}

function syncSkillCost(which)
{
    // The form should be set up as:
    // var costs = array(array("4", "6", "8"), array(...))
    //
    // <select onChange="syncSkillCosts(0)" name="sel_cost" id="s0">
    // ...
    // </select>
    //
    // <input type="text" id="t0" name="txt_cost" readonly="readonly" />
    //
    
    selobj = document.getElementById('s'+which);
    txtobj = document.getElementById('t'+which);
    var total_points = document.getElementById('total_points');
    var unspent_left = document.getElementById('unspent_left');
        
    txtobj.value = costs[which][selobj.selectedIndex];
    
    total_points.value = totalCost();
    
    unspent_left.value = unspentLeft();
    
    selobj.focus();
}

function totalCost()
{
    // We need to have the total cost of the character, the total cost
    // of the skills, and the unspent points as variables declared
    // in the header section of this page, then we can run through
    // the current costs and get a comparison
    //
    new_total = 0;
    new_skill_tot = 0;
    i = 0;
    while ((obj = document.getElementById("t"+i)) != null) {
        //alert(obj.name + ": " + obj.type + ", " + obj.value);
        new_skill_tot += parseFloat(obj.value);
        i++;
    }
    new_total = total + (new_skill_tot - total_skills);
    return new_total;
}

function unspentLeft()
{
    // Return the number of unspent points left.
    // Requires that the form have a variable called unspent that has the
    // number of unspent points, and a total variable with the original
    // total number of points
    //
    new_total = document.getElementById("total_points").value;
    new_unspent = unspent - (new_total - total);
    return new_unspent;
}

function checkPoints()
{
    // return true if the unspent balance is 0 or more, false if not
    left = unspentLeft();
    if ( left >= 0 )
	{
            return true;
	}
    else
	{
            over = -left;
            alert("Stop! You have overspent by " + over + " points");
            return false;
	}
}
