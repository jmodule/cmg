#!/usr/bin/env python
"""An update script that uses SQLObject to create tables"""

import getopt
from pathlib import Path
import site
import sys

cmg_path = Path('./cmg').resolve()
site.addsitedir(str(cmg_path))
site.addsitedir(".")

from model.connect import (init_db, load_db_config)
import model


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def updateUsernames():
    for c in model.charInfo.select():
        print("Updated:", c.userName(True))


def main(argv=None):
    init_db(load_db_config())
    if argv is None:
        argv = sys.argv
        try:
            try:
                opts, args = getopt.getopt(argv[1:], "hdu", ["help", "debug"])
            except getopt.error as msg:
                raise Usage(msg)

            for o, a in opts:
                if o in ('-u', '--usernames'):
                    updateUsernames()
                if o in ('-d', '--debug'):
                    model.log._connection.debug = True
                if o in ('-h', '--help'):
                    raise Usage("sqlobj_update.py [-d|--debug] [-u|--usernames]")

            model.log.createTable(ifNotExists=True)  # 2005/11/30
            model.weaponSkills.createTable(ifNotExists=True)  # 2006/02/12

        except Usage as err:
            print(err.msg, file=sys.stderr)
            print("for help use --help", file=sys.stderr)
            return 2


if __name__ == "__main__":
    sys.exit(main())
