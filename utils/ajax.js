/** -*-java-*-
 * Ajax class for easy ajax communication
 *
 * The latest version of this script can be found at:
 * http://www.senare.nu/ajax/ajax.js
 *
 * This script is released under the LGPL (which
 * basically allows you to do whatever you want with it).
 *
 * For details on LGPL, visit http://www.gnu.org/licenses/lgpl.html
 *
 * @author Mikael Karlsson <mk@se.linux.org>
 *
 */

/*
Usage example:

var ajax = new Ajax();
ajax.get('request.php', 'a=1&b=2', 'handle_response');

The Ajax class will fetch the response from request.php and then
call the function handle_response() with the response as first
and only argument.

The callback function may look like this:

function handle_response(response) {
	alert('Response: ' + response);
}

********** IMPORTANT *************

See http://www.quirksmode.org/dom/importxml.html for a XML processing HowTo.

**********************************

*/

function Ajax() {
	this.xmlhttp = create_xmlhttp();
	
	// methods
	this.request = ajax_request;
	this.get = ajax_get;
	this.post = ajax_post;
	
	this.callback = '';

        this.UseXml = false;
	
	function create_xmlhttp() {
		var req = '';
		if(window.XMLHttpRequest) {
			// w3c browsers
			req = new XMLHttpRequest();
		} else if(window.ActiveXObject) {
			// IE
			req = new ActiveXObject("Microsoft.XMLHTTP");
		}
		return req;
	}
	
	function ajax_request(type, url, query, callback) {
		
		type = type.toLowerCase();
		
		if(type != 'get' && type != 'post') {
			return false;
		}
		
		this.callback = callback;
		
		var this_obj = this;
		
		this.xmlhttp.onreadystatechange = function() {
			if(this_obj.xmlhttp.readyState == 4) {
				if(this_obj.xmlhttp.status == 200) {
                                    var content = this_obj.xmlhttp.getResponseHeader('Content-Type');
                                    if(content == 'application/xml') {
                                        this_obj.callback(this_obj.xmlhttp.responseXML);
                                    } else {
                                        this_obj.callback(this_obj.xmlhttp.responseText);
                                    }
				} else {
                                    alert('There was a problem with the request. (' + this_obj.xmlhttp.status + ')');
				}
			}
		}
		
		// add a random number to prevent caching
		var rand = parseInt(Math.random() * 99999999);
		if(query.length > 0) {
			query += '&';
		}
		query += 'rand=' + rand;
		
		if(type == 'get') {
			// do a GET request
			if(query.length > 0) {
				url = url + '?' + query;
			}
			this.xmlhttp.open("GET", url, true);
			this.xmlhttp.send(null);
		} else {
			// do a POST request
			this.xmlhttp.open('POST', url);
			this.xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			this.xmlhttp.send(query);
		}
		return true;
	}
	
	function ajax_get(url, query, callback) {
		return this.request('get', url, query, callback);
	}
	
	function ajax_post(url, query, callback) {
		return this.request('post', url, query, callback);
	}
}

function alertContents(response) {
    alert(response);
}

function makeRequest(url, msg, func) {
    /* Depricated, here for backwards compatiblity. */

    if (!func) {
        func = alertContents;
    }

    var ajax = new Ajax();
    ajax.post(url, msg, func);
}

/* This next function comes from sitepoint.com
 * <http://www.sitepoint.com/blogs/2004/08/05/quick-tip-xmlhttprequest-and-innerhtml/>
 * function loadFragmentInToElement(fragment_url, element_id) {
 *     var element = document.getElementById(element_id);
 *     element.innerHTML = '<p><em>Loading ...</em></p>';
 *     xmlhttp.open("GET", fragment_url);
 *     xmlhttp.onreadystatechange = function() {
 *         if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
 *             element.innerHTML = xmlhttp.responseText;
 *         }
 *     }
 *     xmlhttp.send(null);
 * }
 */

// ------------------------------------------------------------------------
// Taken from: http://www.sitepoint.com/article/remote-scripting-ajax/3
//
// a custom function that dynamically creates a status label for a
// form element, and positions it visually adjacent to the related
// element
//
function message(element, classString, errorMessage) 
{ 
    var messageDiv = document.createElement("div"); 
    
    element.parentNode.insertBefore(messageDiv, element); 
    messageDiv.className = classString; 
    messageDiv.appendChild(document.createTextNode(errorMessage)); 
    
    return true; 
}
