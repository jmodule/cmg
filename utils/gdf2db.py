#!/usr/bin/env python
"""Converts GCA gdf data files to records suitable for the CMG database."""

import os
import sys
import re
import sqlobject

import ConfigParser
# Now we need to find the config file so we'll know where CMG was installed
#
config = ConfigParser.ConfigParser()
config.read(os.path.expanduser('~/.cmg'))
sys.path.append(config.get('DEFAULT', 'prefix'))

try:
    import const
    import model
except ImportError as val:
    print "Unable to import modules.  Check the config file."
    print "Failed on:", val
    sys.exit(1)

re_comment = re.compile('^(\s*\*|\s+)')
re_category = re.compile('^\[([\w]+)\]')
re_section = re.compile('^\<([\w\s\-:&]+)\>')
re_cost = re.compile('castingcost[(](\d+)\,(\d+)[)]')
re_cost_alt = re.compile('castingcost[(](\d+).*?[)]')
re_notes = re.compile('notes[(](.*?)[)]')
re_time = re.compile('time[(](.*?)[)]')
re_duration = re.compile('duration[(](.*?)[)]')
re_type = re.compile('type[(](\w)\/(\w)\w?[)]')

skip_categories = ('GROUPS', 'MODS')

db_categ = model.category.byDescription('Magic')

def main():
    """Load and parse a given file"""

    curr_category = None
    curr_section = None

    for arg in sys.argv[1:]:
        if os.access(arg, os.R_OK):
            filename = arg
            break

    gdf_file = file(filename, 'r')

    print "Reading", filename

    sec_name_len = 0
    section_list = []

    cnt = 0
    for line in gdf_file:
        cnt += 1
        name = ""
        casting_cost = 0
        maintain_cost = 0
        casting_time = ""
        duration = ""
        notes = ""
        attrib = 'm'
        difficulty = 'h'

        if line[:3] == 'Ver':
            continue

        if re_comment.search(line) != None:
            continue

        match = re_category.search(line)
        if match != None:
            curr_category = match.group(1)
            continue
        
        match = re_section.search(line)
        if match != None:
            curr_section = match.group(1).split(":")[0]
            section_list.append(curr_section)
            if len(curr_section) > sec_name_len:
                sec_name_len = len(curr_section)
            continue

        if curr_category in skip_categories:
            continue

        print cnt, curr_category, curr_section,
        
        try:
            name = line[:line.index(',')]
        except ValueError:
            print "\nError getting name on line %s\n\tContents: %s" % (cnt, line)
            sys.exit(1)

        match = re_cost.search(line)
        if match is not None:
            casting_cost = int(match.group(1))
            maintain_cost = int(match.group(2))
        else:
            match = re_cost_alt.search(line)
            if match is not None:
                casting_cost = int(match.group(1))

        match = re_notes.search(line)
        if match is not None:
            notes = match.group(1)

        match = re_duration.search(line)
        if match is not None:
            duration = match.group(1)

        match = re_time.search(line)
        if match is not None:
            casting_time = match.group(1)

        match = re_type.search(line)
        if match is not None:
            attrib = match.group(1).lower()
            difficulty = match.group(2).lower()

        if curr_category == 'SPELLS':
            records = model.skill.select(
                sqlobject.AND(model.skill.q.name == name,
                              model.skill.q.categoryID == db_categ.id)
                )
            if len(list(records)) > 0:
                print "Updating the skill:",
                records[0].set(
                    college = curr_section,
                    traitType = attrib,
                    diffCode = difficulty,
                    castingCost = casting_cost,
                    castingTime = casting_time,
                    maintainCost = maintain_cost,
                    duration = duration,
                    notes = notes
                    )
            else:
                print "Adding skill:",
                model.skill(
                    name = name,
                    category = db_categ,
                    genre = 'f',
                    college = curr_section,
                    traitType = attrib,
                    diffCode = difficulty,
                    castingCost = casting_cost,
                    castingTime = casting_time,
                    maintainCost = maintain_cost,
                    duration = duration,
                    notes = notes
                    )
        print name, casting_cost, maintain_cost, attrib, difficulty
##, notes, duration, casting_time

    print "Longest category:", sec_name_len

    print "Sections:", section_list

if __name__ == '__main__':
    main()
