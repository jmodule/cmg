// -*-java-*-
// This kicks off a window to enter comments when logging a skill roll
//
function showLogWindow(url)
{
    // display the log window
    //
    log_win = window.open(url, 
                          'Log Roll', 
                          'toolbar=no,scrollbars=yes,width=512,height=320');
    log_win.focus();
}

function doLogSkill()
{
    // get the form variables we need and build the url
    url = 'log_dice.py?session_id=' + document.skillroll.session_id.value;
    url += '&id=' + document.skillroll.id.value;
    url += '&skill=' + document.lastroll.skill.value;
    url += '&level=' + document.lastroll.level.value;
    url += '&roll=' + document.lastroll.roll.value;
    url += '&fatigue=' + document.lastroll.fatigue.value;
    showLogWindow(url)
}

function doLogWpn()
{
    // build the url
    url = 'log_dice.py?session_id=' + document.wpnroll.session_id.value;
    url += '&id=' + document.skillroll.id.value;
    url += '&skill=' + document.lastroll.skill.value;
    url += '&level=' + document.lastroll.level.value;
    url += '&roll=' + document.lastroll.roll.value;
    url += '&damage=' + document.lastroll.damage.value;
    url += '&target=' + document.lastroll.target.value;
    url += '&defense=' + document.lastroll.defense.value;
    showLogWindow(url)
}

function fatigue(session_id, char_id, element_id)
{
    var ajax = new Ajax();
    var element = document.getElementById(element_id);
    data = 'session_id=' + session_id + '&id=' + char_id + '&fatigue=' + element.value;
    ajax.post('update_fatigue.py', data, displayFatigue);
}

function displayFatigue(xmldoc)
{
    var result = xmldoc.getElementsByTagName('fatigue').item(0).firstChild.data;
    var cmd = document.getElementById('_cmd_fatigue');
    cmd.value = "Done";
    var element = document.getElementById('_fatigue_result');
    message(element, 'fail', result + ' points remain');
}
