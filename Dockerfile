FROM httpd:2.4

ARG GIT_HASH
ENV GIT_HASH=$(GIT_HASH:-dev)

RUN sed -i \
    -e 's/^#\(LoadModule cgi_module modules\/mod_cgi.so\)/\1/' \
    conf/httpd.conf

RUN apt-get update && apt-get install -y --no-install-recommends \
    python3 \
    python3-pip \
    python3-cheetah \
    python3-mysqldb

COPY utils/index.html htdocs/
COPY cmg cgi-bin/cmg
COPY requirements.txt .
COPY lib/* lib/
COPY public/* htdocs/cmg/
COPY utils/*.js htdocs/cmg/

RUN pip install -r requirements.txt \
    && mkdir /usr/local/apache2/cgi-bin/cmg/instance \
    && mkdir /usr/local/apache2/cgi-bin/cmg/data \
    && chmod o+w,o+t /usr/local/apache2/cgi-bin/cmg/data \
    && chmod +x /usr/local/apache2/cgi-bin/cmg/*.py \
    && cheetah3 compile /usr/local/apache2/cgi-bin/cmg/templates/*.tmpl

CMD httpd-foreground -c "LoadModule cgid_module modules/mod_cgid.so"
