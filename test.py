"""This is a test character sheet using the sqlobject model"""
import unittest

import profile
import pstats
import os
from pathlib import Path

import sqlobject
import model
import model.connect
import rules
from db import form


def sortByName(a, b):
    """helper function for the list sort"""
    
    if a.trait.name > b.trait.name:
        return 1
    elif a.trait.name < b.trait.name:
        return -1
    else:
        return 0


class TestModel(unittest.TestCase):
    def testCategory(self):
        """Find all that match the Attributes category"""
        for t in model.trait.select(model.trait.q.categoryID == 'abs'):
            self.assertEqual(t.category.description, "Attributes")

    def testGameLevel(self):
        char_id = 4
        skill_name = 'Create Air'
        skill_detail = ''

        rec = model.charSkills.select(
            sqlobject.AND(model.charSkills.q.charID == char_id,
                          model.skill.q.name == skill_name,
                          model.charSkills.q.detail == skill_detail,
                          model.charSkills.q.skillID == model.skill.q.id))

        skill = list(rec)[0]

        try:
            level = int(skill.gameLevel())
        except (ValueError, IndexError):
            level = 0

        self.assertEqual(level, 14)
        self.assertEqual(skill.gameLevel(reverse=level), 3)
        self.assertEqual(skill.gameLevel(reverse=level + 1), 4)

    def testTraitCost(self):
        x = model.charTraits.get(14)
        self.assertEqual(x.cost(), 40)
        y = model.charTraits.get(21)
        self.assertEqual(y.cost(), 45)

    def testSkillList(self):
        i = model.charInfo.get(3)
        self.assertNotEqual(str(i.getAllSkills()), "")


class TestView(unittest.TestCase):
    def testForm(self):
        result = form.fill(model.charInfo)
        self.assertNotEqual(result, "")

    def testDefaultPage(self):
        from page import DefaultPage
        DefaultPage().main()


def testSheet(info):
    """Print out a text character sheet"""
    trait_fmt = "%-25s  %7s  %7s"
    print("-" * 79)
    print("Character Sheet for %s" % info.charName)
    print("")
    print("Description: %s" % info.description) 
    print() 
    print("Player: %s" % info.playerName)
    print("Race: %s" % info.race)
    print("Age: %s" % info.age)
    print("Unspent Points: %s" % info.unspentPoints)
    # print "Total Points: %s" % rules.totalPoints(info)
    print()
    print("Attributes")
    print()
    print(trait_fmt % ("Name","Level", "Points"))
    try:
        for t in info.myTraits:
            if t.traitCode not in rules.attributes:
                continue
            print(trait_fmt % (t.trait.name,
                               t.level,
                               t.cost()))
    except sqlobject.main.SQLObjectNotFound as ex:
        print("Error selecting record: %s" % ex)
    print()
    print("Advantages")
    print()
    print(trait_fmt % ("Name","Level", "Points"))
    for t in info.advantages():
        print(trait_fmt % (t.trait.name,
                           t.level,
                           t.cost()))
    print()
    print("Disadvantages")
    print()
    print(trait_fmt % ("Name","Level", "Points"))
    for t in info.disadvantages():
        print(trait_fmt % (t.trait.name,
                           t.level,
                           t.cost()))
    print()
    print("Quirks")
    print()
    try:
        print("Name" + ' ' * 51 + 'Points')
        for q in info.myQuirks:
            print("%-55s -1" % q.description)
    except sqlobject.main.SQLObjectNotFound as ex:
        print("Error selecting record: %s" % ex)
    print()
    print("Skills")
    print()
    print(trait_fmt % ("Name", "Level", "Points"))

    try:
        skill_list = model.charSkills.select(
            sqlobject.AND(model.charSkills.q.skillID == model.skill.q.id,
                          model.charSkills.q.charID == info.id),
            orderBy=model.skill.q.name)
        for s in skill_list:
            print(trait_fmt % (s.skill.name,
                               s.gameLevel(),
                               s.pointCost()))
    except sqlobject.main.SQLObjectNotFound as ex:
        print("Error on record: %s" % ex)
    print()
    print("Inventory")
    print()
    try:
        for i in info.myInv:
            print("%s (%s) - %s" % (i.eqpt.description,
                                    i.qty,
                                    i.notes))
    except sqlobject.main.SQLObjectNotFound as ex:
        print("Error on record: %s" % ex)

    print()
    print("Notes")
    print()
    print(info.notes)
    print()

    print(info.cost())


class TestPerformance(unittest.TestCase):
    def testProfilingResults(self):
        # This requires profiling code to be enabled for the test page.
        logfile = 'profile.tmp'
        if not os.access(logfile, os.F_OK):
            fd = open(logfile, 'w')
            fd.close()
        try:
            p = pstats.Stats(logfile)
            p.sort_stats('cumulative').print_stats(10)
        except EOFError as val:
            self.fail("Profiling must be enabled on the test page")
            

def main():
    config_path = Path('./test-cmg.conf')

    print("config_path =", config_path)

    db_uri = model.connect.load_db_config(config_path)
    model.connect.init_db(db_uri, 'pymysql')

    all_tests = unittest.TestSuite([
            unittest.TestLoader().loadTestsFromTestCase(TestModel),
            unittest.TestLoader().loadTestsFromTestCase(TestView),
            unittest.TestLoader().loadTestsFromTestCase(TestPerformance)
            ])
    # for i in model.charInfo.select():
    #    testSheet(i)
    testSheet(model.charInfo.get(3))
    unittest.TextTestRunner(verbosity=2).run(all_tests)


if __name__ == '__main__':
    main()
