app_name=cmg
export SERVICE_NAME=$(app_name)

.PHONY: dist docs clean pro dev version build tags

all: cheetah

#README: readme.pod
#	pod2text readme.pod > README

#CHANGES: changes.pod
#	pod2text changes.pod > CHANGES

#docs: README tags

#tags:
#	ctags-exuberant -e -R

#
# The next two targets allow me two switch from a development environment to
# a production environment and back
#
pro:
	psed -e 's/_release = "-dev"/_release = ""/' const.py |\
	psed -e 's/\~jfriant\//' > const.py.new
	mv const.py.new const.py
	psed -e 's/\(.*development.*\)/##\1/' page.py > page.py.new
	mv page.py.new page.py

dev:
	psed -e 's/_release = ""/_release = "-dev"/' const.py |\
	psed -e 's/\"\/\(cmg\/\)/\"\/~jfriant\/\1/' > const.py.new
	mv const.py.new const.py
	psed -e 's/^\#\#*\([ ]*self.defaults.*\)/\1/' page.py > page.py.new
	mv page.py.new page.py
#	cp public/*.css public/*.png public/*.gif utils/*.js $$HOME/public_html/cmg/

version:
	perl -ne 'if (/_build = (\d+)/) { $$new = $$1 + 1; s/\d+/$$new/; }; print $$_' const.py > const.new && mv const.new const.py

build: pro docs
	python setup.py sdist

dist: build version dev

send: clean dist
	scp dist/cmg-*.tar.gz nuit:downloads/

clean:
	-rm README
	-rm CHANGES
	-rm MANIFEST
	-rm -r dist
	-rm -r build
	find . -name "*.pyc" -uid `id -u` -print -exec rm {} \;
	rmb

PREFIX=/usr/lib/cgi-bin/cmg
PUBLIC=/var/www/cmg

install:
	cp -r ./* $(PREFIX)
	cp public/* $(PUBLIC)
	cp utils/*.js $(PUBLIC)
	rm -f templates/*.py[oc]
	cheetah compile $(PREFIX)/templates/*.tmpl
	-cp utils/cmg*.py $$HOME/bin && chmod 755 $$HOME/bin/cmg*.py
	-cp utils/cmg*.sh $$HOME/bin && chmod 755 $$HOME/bin/cmg*.sh
	if [[ ! -s $$HOME/.cmg ]]; then sed 's#PATH#$(PREFIX)#' utils/dot_cmg > $$HOME/.cmg; fi
	if [[ ! -s cmg.conf ]]; then sed 's#PATH#$(PREFIX)#' utils/dot_cmg > cmg.conf; fi
	egrep -q '^[^- ]' utils/update.sql; \
	if [ "$$?" = "0" ]; then \
	mysql -u jfriant -p cmg < utils/update.sql; \
	fi
	python utils/sqlobj_update.py -d

cheetah:
	rm -f templates/*.py[oc]
	cheetah3 compile templates/*.tmpl

test:
	python test.py

%.tmpl:
	cheetah compile templates/$@

dice:
	python setup_dice.py install --prefix=$$HOME

docker-build:
	@build-image.sh

docker-run:
	docker run --detach -p 8080:80 -v $(HOME)/src/cmg/instance:/instance/:ro $(app_name)

docker-kill:
	@echo 'Killing container...'
	@docker ps | grep $(app_name) | awk '{print $$1}' | xargs docker stop
rebuild: docker-kill docker-build docker-run
