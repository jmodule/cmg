#!/usr/bin/env python3
"""Special ditribution file for the dice module"""

__author__ = "Jakim Friant <jakim@friant.org>"
__version__ = "$Revision: 1.6 $"

#$Source: /opt/cvsroot/spigot/cmg/setup.py,v $

# first make sure that we're using the production constants for creating
# the distribution

import const as const
from distutils.core import setup

setup(name="dice",
      version = const.version,
      author = const.author,
      author_email = const.email,
      url = const.url,
      description = const.summary,
      classifiers = const.trove_list,
      py_modules=['dice'],
      )
