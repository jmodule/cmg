#!/usr/bin/env python3
"""Create the distribution packages and install the program"""

import const as const
from distutils.core import setup

__author__ = "Jakim Friant <jakim@friant.org>"
__version__ = "$Revision: 1.6 $"

# $Source: /opt/cvsroot/spigot/cmg/setup.py,v $

# first make sure that we're using the production constants for creating
# the distribution

setup(name="cmg",
      version=const.version,
      author=const.author,
      author_email=const.email,
      url=const.url,
      description=const.summary,
      classifiers=const.trove_list,
      packages=['db', 'model', 'auth', 'cli', 'cli.decision', 'cli.roll'], requires=['sqlobject', 'pyyaml']
      )
