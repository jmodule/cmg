# Configuring Docker for Development

## Create folders for persistent data

Edit the `.env` file and add the path to the docker config files, for example:

    DOCKERDIR=/c/Users/jfriant80/docker

In the "$DOCKERDIR" I create subfolders:

 * cmg
   * `cmg.conf`
   * `passwords`
 * mysql_data
 * secrets
   * `mysql_root_password`

Note that Docker Desktop doesn't like to mount WSL files and also struggles with `/mnt/c` so that needs to be changed
too (see: https://nickjanetakis.com/blog/setting-up-docker-for-windows-and-wsl-to-work-flawlessly).

### Create the Password File

    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt
    python utils/add_user.py /path/to/instance/cmg/passwords

## Create the Database

Start a shell in the Mariadb container.

    docker exec -it cmg_db bash

Launch the mysql client and create the database and a user account.

    mysql -u root -p
    create database cmg;
    create user 'USERNAME'@'%' identified by 'MY_PASSWORD';
    grant all privileges on `cmg`.* to 'USERNAME'@'%';

Load the test data from a SQL dump file.

    docker exec -i cmg_db sh -c 'exec mysql -uroot -p"PASSWORD_HERE" cmg' < cmg-20220503.sql
