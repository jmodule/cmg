#!/usr/bin/env python3

import cgi

import model
from model.connect import (load_db_config, init_db)
import const

from page import DefaultPage

from templates.LogShow import LogShow

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__

_title = "Display Log"


class LogPage(DefaultPage):
    def processForm(self):
        entries = []
        filter_list = dict()
        active_filter = None

        for f in model.category.select(model.category.q.validTable == 'log',
                                       orderBy='description'):
            filter_list[f.description] = f.id

        if 'filter' in self.defaults.form:
            active_filter = self.defaults.form['filter'].value
            if active_filter not in filter_list:
                active_filter = None

        if 'del' in self.defaults.form:
            rec_id = self.defaults.form['del'].value
            model.log.delete(rec_id)
            self.defaults.alerts.append("Deleted record %s" % rec_id)

        if active_filter is not None:
            records = model.log.select(
                model.log.q.categID == filter_list[active_filter],
                orderBy='logtime')
        else:
            records = model.log.select(orderBy='logtime')

        for l in records:
            if l.char is not None:
                name = l.char.charName
            else:
                name = l.categ.description
            if l.roll <= l.level:
                comment = '<span class="succeed">' + l.comment + '</span>'
            else:
                comment = l.comment
            entries.append((l.id, name, l.skill, l.level, l.roll, comment))
            
        # if the user requested a limit on records returned, then see
        # if we've reached it.
        try:
            limit = int(self.defaults.form['limit'].value)
            if limit > 0:
                entries = entries[len(entries) - limit:]
            else:
                limit = ""
        except (ValueError, KeyError ):
            limit = ""

        self.myvars = { 'entries': entries,
                        'filters': filter_list,
                        'active_filter': active_filter,
                        'limit': limit,
                        }

        self.defaults.action = my_name
        self.defaults.title = _title


if __name__ == "__main__":
    init_db(load_db_config())
    LogPage(LogShow).main()
