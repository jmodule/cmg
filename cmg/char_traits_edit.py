#!/usr/bin/env python3

import auth
from db import data2js
import model
from model.connect import (load_db_config, init_db)
from page import DefaultPage
from templates.CharTraitsEdit import CharTraitsEdit

try:
    my_name = __file__[__file__.rindex("/") + 1:]
except IndexError:
    my_name = __file__


class EditPage(DefaultPage):

    def processForm(self):
        """Edit the characters' traits."""
        # Get the needed lists
        #
        trait_list = model.trait.select(model.trait.q.invalid == False,
                                        orderBy=[model.trait.q.categoryID,
                                                 model.trait.q.name])

        # build the javascript needed for the header
        #
        jscode = data2js.traitLists(trait_list)

        # See if we need to do anything with the record
        #
        if 'cmd_update' in self.defaults.form:
            if self.update(self.defaults.form):
                self.defaults.alerts.append('Record Updated')

        # check for a delete request
        #
        if 'cmd_del' in self.defaults.form:
            model.charTraits.delete(self.defaults.form['cmd_del'].value)
            self.defaults.alerts.append(
                'Record Deleted (%s)' % self.defaults.form['cmd_del'].value)

        self.defaults.jsfile = "char.js"
        self.defaults.js = jscode
        self.defaults.no_add = True
        self.defaults.action = my_name
        self.defaults.title = 'Edit Traits'

        self.myvars = {'trait_list': trait_list,
                       'char_menu': auth.charMenu(self.defaults.session_id),
                       }

        if self.defaults.id is not None:
            self.myvars['info'] = model.charInfo.get(self.defaults.id)
        else:
            self.myvars['info'] = None

    def update(self, form):
        """Update the records already in the database."""
        if 'id' in form:
            if 'trait_id' in form:
                if isinstance(form['trait_id'], list):
                    for (i, tid) in enumerate(form['trait_id']):
                        t = model.charTraits.get(tid.value)
                        try:
                            l = int(form['level'][i].value)
                        except ValueError:
                            l = 0
                        try:
                            d = int(form['discount'][i].value)
                        except ValueError:
                            d = 0
                        if 'detail_%s' % i in form:
                            e = form['detail_%s' % i].value
                        else:
                            e = ""
                        t.set(level=l, racialDiscount=d, detail=e)
                else:
                    t = model.charTraits.get(form['trait_id'].value)
                    try:
                        l = int(form['level'].value)
                    except ValueError:
                        l = 0
                    try:
                        d = int(form['discount'].value)
                    except ValueError:
                        d = 0
                    if 'detail_0' in form:
                        e = form['detail_0'].value
                    else:
                        e = ""

                    t.set(level=l, racialDiscount=d, detail=e)

            if 'new_trait' in form and 'new_level' in form:
                if form['new_trait'].value != 'Choose a new trait...':
                    trait = model.trait.byName(form['new_trait'].value)
                    try:
                        l = int(form['new_level'].value)
                    except ValueError:
                        l = 0
                    try:
                        d = int(form['new_discount'].value)
                    except (ValueError, KeyError):
                        d = 0
                    if 'new_detail' in form:
                        t = form['new_detail'].value
                    else:
                        t = ""

                    model.charTraits(
                        level=l,
                        racialDiscount=d,
                        detail=t,
                        charID=int(form['id'].value),
                        traitCode=trait.code,
                        trait=trait)
            return True
        else:
            return False


if __name__ == "__main__":
    init_db(load_db_config())
    EditPage(CharTraitsEdit, save_id=True).main()
