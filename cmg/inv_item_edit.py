#!/usr/bin/env python3

import cgi

from jmodule.db import link, table
from jmodule.utils import debug, menu
from jmodule.web import login, html
from db import category, char, equipment
import rules
# using the PythonWeb module to generate Cheetah templates
import web.template

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__
table_name = 'inventory'
page_title = "Inventory Editor"


def main():
    """Edit the characters' traits."""

    session_id = login.check('cmg.py')
    if session_id != -1:
        try:
            db_link = link.cLink(db='cmg')
            access = login.getAccess(session_id)
            db_link.login(access)

            # load the necessary lists and set the current items if
            # they're in the form
            #
            # category.load(db_link, 'equipment')
            html.begin(title=page_title)

            debug.report('Notice',
                         my_name+'.main',
                         'form contents',
                         login.session_file.form)
            
            if 'eqpt_name' in login.session_file.form:
                curr_eqpt = login.session_file.form['eqpt_name'].value
                try:
                    qty = curr_eqpt[curr_eqpt.index('(') + 1:curr_eqpt.index(')')]
                except ValueError:
                    qty = 0
                try:
                    curr_eqpt = curr_eqpt[:curr_eqpt.index('(') - 1]
                except ValueError:
                    pass
            else:
                curr_eqpt = ""
                qty = 0

            if 'char_id' in login.session_file.form:
                char_id = login.session_file.form['char_id'].value
            else:
                char_id = None

            inventory = char.getInvAsClass(db_link, char_id)

            if curr_eqpt in inventory:
                notes = inventory[curr_eqpt].notes
                eqpt_id = inventory[curr_eqpt].id
                if inventory[curr_eqpt].in_use:
                    in_use = "CHECKED"
                else:
                    in_use = ""
            else:
                notes = ""
                eqpt_id = 0
                in_use = ""

            # See if we need to do anything with the record
            #
            record_updated = 0
            if 'cmd_save' in login.session_file.form:
                if char.canUpdate(db_link, session_id, char_id):
                    if 'txt_qty' in login.session_file.form:
                        new_qty = login.session_file.form['txt_qty'].value
                    else:
                        new_qty = 0
                        
                    if 'txt_notes' in login.session_file.form:
                        new_notes = login.session_file.form['txt_notes'].value
                    else:
                        new_notes = ""

                    if 'chk_in_use' in login.session_file.form:
                        new_in_use = 1
                    else:
                        new_in_use = 0
                        
                    record_updated = equipment.updateInventory(
                        db_link,
                        char_id,
                        eqpt_id,
                        new_qty,
                        new_notes,
                        new_in_use
                        )

            if not record_updated:
                myvars = {
                    'char_id': char_id,
                    'curr_eqpt': curr_eqpt,
                    'qty': qty,
                    'action': my_name,
                    'title': page_title,
                    'session_id': session_id,
                    'notes': notes,
                    'in_use': in_use
                    }
                
                print(web.template.parse(
                    type='cheetah',
                    file='templates/inv_item_edit.tmpl',
                    dict=myvars,
                    useCompiled='auto',
                    ))
            else:
                html.p('Record Saved', align="center")
                html.p('<input type="button" value="Close" onClick="javascript:window.close()" />', align="center")
                html.end()
            
        except Exception:
            cgi.print_exception()


if __name__ == "__main__":
    main()
