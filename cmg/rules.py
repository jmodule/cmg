#$Id: rules.py,v 1.47 2006-06-26 20:13:50 jfriant Exp $
"""This has constants, functions, tables, etc., relating to the rules

Point costs especially have been encoded as a table when the values do
not match an obvious (simple?) function.

*TODO*

Ranged Attack Modifiers
Aiming time
snap shot... -4 unless adjusted skill >= weapons ss modifier
aiming...    +acc modifier
  additional turns aiming give +1 to skill up to +3
  bracing a weapon gives an additional +1 when aiming

Opportunity fire
evaluating the target before firing... -2
one or two hexes being watched...      -4
  each addtl up to 10 or more...       -1
pop-up attack...                       -6

Against wrong target (after miss)...    Normal roll, max 9.
Shooting blind...                      -10 or 9 (whichever is worse)

"""

import math
from db import char
from jmodule.utils import debug

from const import armor_attributes, armor_locations

try:
    my_name = __file__[__file__.rindex("/")+1:]
except ValueError:
    my_name = __file__

attributes = ('ST', 'DX', 'IQ', 'HT')

calculated_attributes = {'Perception': { 'base': 'IQ',
                                         'bonus': ('Alertness',),
                                         'penalty': None },
                         'Vision': { 'base': 'IQ',
                                    'bonus': ('Alertness', 'Acute Vision'),
                                    'penalty': ('Bad Sight',) },
                         'Hearing': { 'base': 'IQ',
                                      'bonus': ('Alterness', 'Acute Hearing'),
                                      'penalty': ('Hard of Hearing', 'Deafness')},
                         'Smell/Taste': { 'base': 'IQ',
                                          'bonus': ('Acute Taste and Smell',),
                                          'penalty': ('No Sense of Smell', 'No Sense of Taste') },
                         'Will': { 'base': 'IQ',
                                   'bonus': ('Strong Will',),
                                   'penalty': ('Weak Will',) },
                         }

# NOTE: Skill levels are stored slightly differently than one might
# expect.  The difficulty of the skill determines where one starts out
# calculating the skill level, for example a physical-easy level 0
# skill translates to a DX-1 skill costing a half point.  Another
# example is a mental hard skill at level 2 translates to IQ-1 for 2
# points.
#
# The main thing is that GURPS doesn't explicitly use levels like
# this, but I've derived it from their skill tables.
#

# The following tuples contain: begining skill level, break point
# level after which we shift to a flat point progression, and the
# incremental cost once we're at the flat point progression
#
skill_costs = { 'p' : { 'e': (-1, 5, 8),
                        'a': (-2, 5, 8),
                        'h': (-3, 5, 8),
                        'attr': 'DX' },
                'm' : { 'e': (-1, 3, 2),
                        'a': (-2, 3, 2),
                        'h': (-3, 3, 2),
                        'v': (-4, 4, 4),
                        'attr': 'IQ' },
                'h' : { 'e': (-1, 5, 8),
                        'a': (-2, 5, 8),
                        'h': (-3, 5, 8),
                        'attr': 'HT' },
                's' : { 's': (0, -1, 0),
                        'attr': 'DX' },
                }

encumbrance_levels = [ {'desc' : "None", 'mod' : 0, 'mult': 2 },
                       {'desc' : "Light", 'mod' : 1, 'mult': 4 },
                       {'desc' : "Medium", 'mod' : 2, 'mult': 6 },
                       {'desc' : "Heavy", 'mod' : 3, 'mult': 12 },
                       {'desc' : "X-Heavy", 'mod' : 4, 'mult': 20 },
                       ]

lift_types = [ { 'desc' : "One-Handed Lift", 'mult': 6 },
               { 'desc' : "Two-Handed Lift", 'mult': 25 },
               { 'desc' : "Carry on Back", 'mult': 30 },
               { 'desc' : "Shove and Knock Over", 'mult': 25 },
               { 'desc' : "Running Shove and Knock Over", 'mult': 50 },
               { 'desc' : "Shift Slightly", 'mult': 100 },
               ]

active_defenses = ['Do nothing',
                   'Block',
                   'Dodge',
                   'Parry' ]

# the fields in the tuple are: desc, roll result, difficulty, armor location
hit_location = [ ('Eye', '-', -9, 'no'),
                 ('Skull', '3-4', -7, 'head'),
                 ('Face', '5', -5, 'head'),
                 ('Right Leg', '6-7', -2, 'leg'),
                 ('Right Arm', '8', -2, 'arm'),
                 ('Torso', '9-10', 0, 'torso'),
                 ('Groin', '11', -3, 'torso'),
                 ('Left Arm', '12', -2, 'arm'),
                 ('Left Leg', '13-14', -2, 'leg'),
                 ('Hand', '15', -4, 'arm'),
                 ('Foot', '16', -4, 'leg'),
                 ('Neck', '17-18', -5, 'head'),
                 ('Vitals', '-', -3, 'torso'),
                 ]

cover_modifiers = { "prone behind cover, head down": -7,
                    "can only see head": -5,
                    "head and shoulders out": -4,
                    "behind someone else (each person)": -4,
                    "crawling without cover": -4,
                    "body half exposed": -3,
                    "light cover": -2,
                    "crouching, sitting, or kneeling without cover": -2,
                    }

damage_type_mult = { 'None': 0.0,
                     'imp': 2.0,
                     'cut': 1.5,
                     'cr': 1.0, }

equipment_notes = [
    "For firearms leave the damage base field blank.",
    "Primary damage should be swinging and secondary should be thrusting",
    "For the damage and maximum damage fields use the table below",]

#--------------------------------------------------------------------------
def calcTraitCost(level, base, incr, code):
    """Return the point cost for the trait level for a character
    
    Trait base cost is how much the trait would cost if purchased at
    level 1.  Many traits only have one level.  Traits with more than
    one level have the incremental cost which is each for each point
    purchased after the first.  For example:
    
    Alertness +2/level - base cost: 2, incr_cost: 2, max level: None
    
    Magery 15/25/35 - base cost: 15, incr_cost: 10, max level: 3
    
    Danger Sense 15 - base cost: 15, incr_cost: 0, max level: 1
    
    Attributes are calculated differently from the adv/disad types in
    that the base cost represents the base attribute level where cost
    = 0, i.e. 10.  The incremental cost is also modified by the level
    as well.
    
    Attributes also have the possiblity of having a racial discount
    that is calculated seperately from the levels purchased normally.
    For example Reptilemen get ST +4 as a racial bonus, so the
    discount should be entered as 4.  So for a character with ST 17 it
    will first calculate the points purchased by the character (17 - 4
    = 13) and then calculate the points from the racial package (10 +
    4 = 14).
    
    """
    if code in attributes:
        trait_cost = (level - base) * incr

        # Now we'll do some adjustments that come from the
        # attribute cost table and apparently are arbitrary,
        # except for after level 17 it's a flat 25 points per
        # level (though you can't tell that from the formula -- I
        # worked that one out by trying different things in a
        # spreadsheet).
        #
        if level == 8 or level == 14:
            trait_cost += 5
        elif level <= 7 or level == 15:
            trait_cost += 10
        elif level == 16:
            trait_cost += 20
        elif level >= 17:
            trait_cost += 30 + (level - 17) * 15
    else:
        trait_cost = base + (incr * (level - 1))
    return trait_cost

#--------------------------------------------------------------------------
def traitPointCost(db_link, char_id, trait_code, level):
    """Return the point cost for the trait level

    *DEPRICATED* use model.trait.cost()

    """
    sql = "select base_cost, incr_cost, racial_discount"
    sql += " from traits, char_traits"
    sql += " where code = '%s'" % trait_code
    sql += " and trait_id = traits.id"
    sql += " and char_id = '%s'" % char_id
    result = db_link.query(sql)
    if result:
        discount = int(result[0]['racial_discount'])
        if discount > 0:
            trait_cost = calcTraitCost(level - discount,
                                       result[0]['base_cost'],
                                       result[0]['incr_cost'],
                                       trait_code)
            trait_cost += calcTraitCost(discount + 10,
                                        result[0]['base_cost'],
                                        result[0]['incr_cost'],
                                        trait_code)
        else:
            trait_cost = calcTraitCost(level,
                                       result[0]['base_cost'],
                                       result[0]['incr_cost'],
                                       trait_code)
    else:
        debug.report('Notice',
                     my_name + '.traitPointCost',
                     'trait',
                     trait_code)
        trait_cost = 0
   
    return trait_cost

#--------------------------------------------------------------------------
def skillPointCost(db_link, skill_id, level):
    """Return the calculated point cost of a skill"""
    cost = 0
    attr = None
    start = 0

    sql = "select trait_type, diff_code from skills where id = '%s'" % skill_id
    result = db_link.query(sql)

    if result:
        ttype = result[0]['trait_type']
        dcode = result[0]['diff_code']
        attr = skill_costs[ttype]['attr']
        (start, break_point, incr_cost) = skill_costs[ttype][dcode]
        if level <= break_point:
            cost = (0.5 * 2 ** level)
        else:
            cost = (0.5 * 2 ** break_point) + (level - break_point) * incr_cost
        
        # 0.5 is the floor for costs, if it's less than that,
        # something funky is going on.  Either it's a special
        # skill that doesn't have a point cost, or the user
        # entered a negative level and we don't want to display
        # the point cost for that.
        #
        if cost < 0.5:
            cost = 0
    
    return cost

#--------------------------------------------------------------------------
def skillLevel(db_link, skill, trait_list, **kwds):
    """Return the calculated skill level

    It can also do the reverse calculation to convert a calculated
    skill level back to what is stored in the database by specifying
    the 'reverse' keyword with the calculated skill level.

    """
    level_out = 0
    bonus = 0
    if 'skill_id' in skill:
        sid = skill['skill_id']
    elif 'id' in skill:
        sid = skill['id']
    else:
        sid = None
    sql = "SELECT trait_type, diff_code, category_id FROM skills"
    sql += " WHERE id = '%s'" % sid
    result = db_link.query(sql)

    if result:
        ttype = result[0]['trait_type']
        dcode = result[0]['diff_code']
        categ = result[0]['category_id']
        attr = skill_costs[ttype]['attr']
        (start, break_point, incr_cost) = skill_costs[ttype][dcode]
        
        # Look for bonuses from advantages
        #
        if categ == 'mag':
            if 'Magery' in trait_list:
                bonus = trait_list['Magery']['level']

        if 'reverse' in kwds:
            level_out = kwds['reverse'] - start - trait_list[attr]['level'] - bonus
        else:
            level_out = start + trait_list[attr]['level'] + skill['level'] + bonus
    return level_out

#--------------------------------------------------------------------------
def totalPoints(db_link, char_id):
    "Returns the calculated total points in the character"
    cost = 0
    quirk_list = char.getCharQuirks(db_link, char_id)

    cost += totalSkillPoints(db_link, char_id)
    cost += totalTraitPoints(db_link, char_id)
    cost += (0 - len(quirk_list))

    return cost

#--------------------------------------------------------------------------
def totalTraitPoints(db_link, char_id):
    """Return the total points spent on traits"""
    cost = 0
    trait_master = char.getTraits(db_link)
    trait_list = char.getCharTraits(db_link, char_id)
    for trait in list(trait_list.keys()):
        cost += traitPointCost(db_link,
                               char_id,
                               trait_master[trait],
                               trait_list[trait]['level'])
    return cost

#--------------------------------------------------------------------------
def totalSkillPoints(db_link, char_id):
    """Return the total points spent on skills"""
    cost = 0
    skill_list = char.getCharSkills(db_link, char_id)
    for (name, sdata) in list(skill_list.items()):
        cost += skillPointCost(db_link, sdata['id'], sdata['level'])
    return cost

#--------------------------------------------------------------------------
def parry(db_link, addtl_list, char_skill_list, trait_list):
    """Return the parry score for the character

    This requires that the user has specified which weapon will be
    used to parry.

    Note: some weapons, such as quarterstaffs and fencing weapons,
    parry at 2/3 the skill level, so the modifier is stored along with
    each combat skill and will need to be entered to make the skill
    avaiable for parrying.

    """
    level = 0
    if 'parry_name' in addtl_list:
        if addtl_list['parry_name'] in char_skill_list:
            modifier = char_skill_list[addtl_list['parry_name']]['parry']
            level = skillLevel(db_link,
                               char_skill_list[addtl_list['parry_name']],
                               trait_list)
            level = int(math.floor(float(level) * modifier))

    return level

#--------------------------------------------------------------------------
def block(db_link, char_skill_list, trait_list):
    """Return the block score for the character

    Block is always half of the shield skill, which also defaults to
    DX-4.

    """
    try:
        if 'Shield' in char_skill_list:
            level = skillLevel(db_link,
                               char_skill_list['Shield'],
                               trait_list)
        else:
            level = trait_list['DX']['level'] - 4
        level = int(round(level / 2))
    except:
        level = 0
    return level

#--------------------------------------------------------------------------
def base_damage(dmg_base, trait_list, **keywords):
    """Return a tuple of the basic damage for thrusting or swinging

    base_damage('s', dict()) -> (dice, modifier)

    Takes an optional keyword paramater, 'modifier', to add in the
    weapon modifier (equipment.priDmg or equipment.secDmg).

    A side effect when calculating non-strength based weapons is if
    you leave the *DmgBase field blank then the strength won't be
    added to the amount.  For testing you can set the trait_list to
    None and leave the dmg_base blank and get the same effect.
    
    """
    dice = 0
    modifier = 0
    if trait_list is not None:
        st = trait_list['ST']['level']
    else:
        st = 0
    amt = 0
    if dmg_base == 's':
        amt = st - 10
    elif dmg_base == 't':
        if st < 6:
            amt = math.ceil(float(st) / 2) - 8
        else:
            amt = math.ceil(float(st) / 2) - 7
    
    if 'modifier' in keywords:
        amt += int(keywords['modifier'])
            
    if amt >= -5:
        if amt < 1:
            dice = 1
            modifier = amt
        else:
            # note this formula for the number of dice breaks when the
            # character's ST is over 28 or the weapon damage is over 19
            #
            dice = math.ceil(float(amt) / 3.75)
            modifier = float(amt) % 4
            if modifier == 3:
                dice += 1
                modifier = -1
    return (dice, modifier)

#--------------------------------------------------------------------------
def dice2str(value, show_sides = True):
    """Returns the string representation of the dice tuple

    If you want to suppress the printing of the 6 (it's understood in
    GURPS notation) you can set show_sides to false.
    """
    dice = value[0]
    modifier = value[1]
    if show_sides:
        sides = "6"
    else:
        sides = ""
    dmg = "%dd%s" % (dice, sides)
    if modifier != 0:
        dmg += "%+d" % modifier
    return dmg

#--------------------------------------------------------------------------
def height(trait_list, sex='m', format=False):
    """Return the average height for the ST level

    The formula is: 69 inches - 10 + ST

    """
    try:
        h = trait_list['ST']['level'] + 59
    except:
        h = -1
    if sex == 'f':
        h -= 2
    # should we format the output?
    if format:
        return "%s'%s\"" % divmod(h, 12)
    else:
        return h

#--------------------------------------------------------------------------
def weight(trait_list, sex='m'):
    """Return the average weight for a given height (based on ST)"""
    h = height(trait_list, sex)
    weight_chart = { 62 : 120,
                     63 : 130,
                     64 : 130,
                     65 : 135,
                     66 : 135,
                     67 : 140,
                     68 : 145,
                     69 : 150,
                     70 : 155,
                     71 : 160,
                     72 : 165,
                     73 : 170,
                     74 : 180,
                     75 : 190,
                     76 : 200,
                     77 : 210,
                     78 : 220,
                     79 : 230 }
    try:
        w = weight_chart[h]
    except:
        w = -1
    if sex == 'f':
        w -= 10
    return w

#--------------------------------------------------------------------------
def calc_attr(name, trait_list):
    """Calculate and return the derived attributes"""

    bonus = 0
    penalty = 0
    base = 0

    if name in calculated_attributes:
        if calculated_attributes[name]['bonus'] != None:
            for trait in calculated_attributes[name]['bonus']:
                if trait in trait_list:
                    bonus += int(trait_list[trait]['level'])
        if calculated_attributes[name]['penalty'] != None:
            for trait in calculated_attributes[name]['penalty']:
                if trait in trait_list:
                    penalty += int(trait_list[trait]['level'])
        base = int(trait_list[calculated_attributes[name]['base']]['level'])
    
    return base + bonus - penalty

#--------------------------------------------------------------------------
def rowHighlight():
    '''return a string with an alternate highlight CSS class name
    
    Call this from a Cheetah template, for example:
        <p class="$rules.highlight()">

    This is here to test using a function to generate the highlight name.

    '''
    line_class = "color_two"
    while True:
        if line_class == "color_two":
            line_class = "color_one"
        else:
            line_class = "color_two"
        yield line_class
        
# call this from the template, ex: class="$rules.highlight()"
highlight = rowHighlight().__next__

#--------------------------------------------------------------------------
def testDamage(max=28, min=0):
    """Print out a range of damages to see if the function works.

    For example: testDamage(40, -5) -> printed output

    """
    for i in range(min, max):
        print("priDmg %d = %s" % (i, dice2str(base_damage("", None, modifier=i), False)))
    # Test the strength
    print("Strength = 29:", end=' ')
    print(dice2str(base_damage("s", {'ST': {'level': 29}}), False))

#==========================================================================
if __name__ == "__main__":
    testDamage(40, -5)
