#!/usr/bin/env python3

import auth
from os import linesep as eol
from page import DefaultPage
from templates.Edit import Edit


class CharPage(DefaultPage):
    def processForm(self):
        html = "<ul>" + eol
        for c in auth.charMenu(self.defaults.session_id):
            html += "<li>" + c.charName + eol
            html += "<ul>" + eol
            html += '<li><a href="show.py?id=%s">Character Sheet</a>' % c.id
            html += '<li><a href="openrpg1.py?id=%s">OpenRPG Sheet</a>' % c.id
            html += '</ul>' + eol
        html += '</ul>' + eol
        self.defaults.no_add = True
        self.myvars = { 'title': 'Display Character Sheet',
                        'char_menu': [],
                        'bodycontent': html,
                        }


if __name__ == "__main__":
    CharPage(Edit).main()
