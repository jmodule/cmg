"""Functions for rolling dice and parsing die codes

The parse and roll functions were originally from the GUI vdice
project, but have been expanded for use as a generic dice module.  The
vdice project is now defunct and a separate script has been created to
roll dice from the command prompt that uses this module.

"""
from random import seed, randint
from os import linesep
import re

__author__ = "Jakim Friant <jakim@friant.org>"
__version__ = "$Revision: 1.7 $"


# $Source: /opt/cvsroot/spigot/cmg/dice.py,v $


class Dice:
    """This class represents a set of dice"""

    def __init__(self, dice_code=None, level=None, modifier=0):
        """Initialize the class

        The dice_code is for reference and should be parsed by the
        helper function below to generate the roll.

        The level is the number being rolled against, and the modifier
        is a number that, when added to the level, produces a target
        number (as opposed to the modifier in the dice code that
        modifies the roll, such as 1d-1 which has a -1 modifier on the
        die)

        """
        self.level = None
        self.target_mod = None
        self.target = 0
        self.roll_result = None
        self.last = 0
        self.roll_history = []

        self.set(dice_code, level, modifier)

    def set(self, dice_code=None, level=None, modifier=0):
        """This sets up the dice, returns nothing

        This can be used to make changes to the dice without losing
        the history.

        """
        (self.number, self.sides, self.modifier, self.multiplier) = parse(dice_code)
        self.level = level
        self.target_mod = modifier
        if level is not None:
            self.target = level + modifier
        else:
            self.target = 0

    def __str__(self):
        """Return a string of the last dice roll"""
        if len(self.roll_history) > 0:
            return self.history()[self.last]
        else:
            return self.history()[0]

    def history(self):
        """return a list of string of the dice rolls

        history() => list(strings)

        """
        r_code = str(self.number) + 'd' + str(self.sides)
        if self.modifier != 0:
            if self.modifier > 0:
                r_code += "+"
            r_code += str(self.modifier)
        if self.multiplier != 1:
            r_code += "*" + str(self.multiplier)
        r_skill = ""
        if self.level is not None:
            if self.target_mod == 0:
                r_skill += ' vs ' + str(self.level)
            if self.target_mod != 0:
                r_skill += ' vs %d (modified from %d)' % (self.target,
                                                          self.level)
        result = []
        if len(self.roll_history) > 0:
            for roll in self.roll_history:
                result.append(r_code + ' = ' + str(roll) + r_skill)
        else:
            result.append(r_code)

        return result

    def roll(self):
        """returns nothing, generate and store a 'roll' of dice"""
        self.roll_history.append(0)
        self.last = len(self.roll_history) - 1
        self.roll_history[self.last] = dieRoll(self.number,
                                               self.sides,
                                               self.modifier,
                                               self.multiplier)
        # *DEPRICATED* we'll make a link here but from now on code
        # should use the result() function to get the last roll
        self.roll_result = self.roll_history[self.last]

    def result(self):
        """return the last roll"""
        if len(self.roll_history) > 0:
            return self.roll_history[self.last]
        else:
            return 0

    def success(self, below=True):
        """Return true or false based on if the roll was successful

        success([below]) -> Boolean

        The below flag defaults to True, indicating indicating that
        you need to roll the number or less.  Set to False to compare
        rolling against the number or above.

        """

        if below:
            return self.roll_history[self.last] <= (self.target)
        else:
            return self.roll_history[self.last] >= (self.target)

    def resultMsg(self, below=True):
        """Return a string indicating success or failure

        A little redundant, but it makes it more convient to log the
        result of a roll.  Message is in past tense.

        """
        if self.success(below):
            return "succeded"
        else:
            return "failed"


def dieRoll(number, sides, modifier=0, multiplier=1):
    """A generic function to simulate rolling dice with any number of sides"""
    result = 0
    for i in range(number):
        result += randint(1, sides)
    result += modifier
    result *= multiplier
    return result


def parse(die_code):
    """return a tuple of the number of dice, sides, and modifier

    Handles any number of sides and will parse FUDGE dice too (4dF)

    Ex:
      (3, 6, 0, 1) = parseDieCode('3d6')
      
    """
    # set up the defaults
    modifier = 0
    number = 1
    sides = 6
    multiplier = 1

    if die_code is not None:
        re_num = re.compile("([1-9][1-9]*)[Dd]")
        re_mod = re.compile("[Dd][0-9]*([\-|\+]\d+)")
        re_mult = re.compile("[*](\d+)")
        re_side = re.compile("[Dd]([1-9][0-9]*)")
        re_fudge = re.compile("[Dd][fF]")

        # find the number of dice to roll
        #
        result = re_num.search(die_code)
        if result:
            try:
                number = int(result.group(1))
            except ValueError:
                number = 0

        # find any modifiers
        #
        result = re_mod.search(die_code)
        if result:
            try:
                modifier = int(result.group(1))
            except ValueError:
                modifier = 0

        # special modifier to multiply the result
        #
        result = re_mult.search(die_code)
        if result:
            try:
                multiplier = int(result.group(1))
            except ValueError:
                multiplier = 0

        # find the number of sides
        #
        result = re_side.search(die_code)
        if result:
            try:
                sides = int(result.group(1))
            except ValueError:
                sides = 0
        else:
            result = re_fudge.search(die_code)
            if result:
                sides = 3
                modifier = -2 * int(number)

    return number, sides, modifier, multiplier


def test():
    """Test function"""

    print("dice.test():")
    for code in ('1d4', '3d6', '1d10', '4d20', '1d100', '4dF', '1d+1', 'd8*5'):
        d = Dice(code)
        d.roll()
        print("\t", d)

    d = Dice('2d6', 6)
    d.roll()
    print("\t", d, "Rolled above:", d.success(False))

    d = Dice('3d6', 12, -2)
    d.roll()
    print("\t", d, "Successful:", d.success())
    print("\t\t", "The roll", d.resultMsg())

    print("\t", "Using history:")
    d = Dice('3d6')
    for x in range(5):
        d.roll()
    d.set('3d6-4')
    for x in range(5):
        d.roll()
    for x in d.history():
        print("\t\t", x)


if __name__ == "__main__":
    test()
