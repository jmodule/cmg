#!/usr/bin/env python3
"""Generate the HTML character sheet"""

import cgi
import sqlobject

import const
import model
from model.connect import (load_db_config, init_db)
from jmodule.web import login, html
from page import DefaultPage

from templates.CharSheet import CharSheet

try:
    my_name = __file__[__file__.rindex("/") + 1:]
except IndexError:
    my_name = __file__


class ShowPage(DefaultPage):
    def main(self):
        """Display the character sheet in guest mode only

        Since I don't need the extra processing done by default in the
        parent class, and the login requirements are quite different,
        I'm overriding the main function.

        """

        session_id = login.check(guest=True)
        try:
            if 'id' in login.session_file.form:
                char_id = int(login.session_file.form['id'].value)
            else:
                raise Exception('No Char Id')

            info = model.charInfo.get(char_id)
            point_totals = info.cost()

            if info.portraitLink is not None \
                    and info.portraitLink != "None" \
                    and info.portraitLink.strip() != "":
                portrait_link = info.portraitLink
            else:
                portrait_link = const.public + "blank_portrait.png"

            skill_list = {}
            for categ in (('reg', 'cbt'), ('mag',), ('psi',)):
                name = model.category.get(categ[0]).description
                skill_list[name] = model.charSkills.select(
                    sqlobject.AND(model.charSkills.q.skillID == model.skill.q.id,
                                  model.charSkills.q.charID == info.id,
                                  sqlobject.IN(model.skill.q.categoryID, categ)),
                    orderBy=model.skill.q.name)

            all_inv = info.getInventory()

            wpn_inv = []
            for i, item in enumerate(all_inv):
                if item.eqpt.category.id in _weapons:
                    wpn_inv.append(item)

                # spells are not actually _in_ the inventory, so I'm
                # deleting them from the equipment list that is
                # displayed.
                #
                if item.eqpt.category.id == 'spl':
                    del (all_inv[i])

            self.defaults.css_links.append(const.css['char_sheet'])
            self.defaults.title = "Character Sheet"
            self.defaults.action = my_name

            self.myvars = {
                'info': info,
                'trait_list': info.traitList(),
                'skill_list': skill_list,
                'footer': const.footer,
                'points': point_totals,
                'portrait': portrait_link,
                'armor': info.armor(),
                'weapons': wpn_inv,
                'all_inv': all_inv,
            }

            print(CharSheet(searchList=[self.myvars, self.defaults]))
        except Exception:
            html.begin(title="Error on Character page",
                       stylesheet=const.css['cmg'])
            cgi.print_exception()

        html.end()


if __name__ == "__main__":
    init_db(load_db_config())
    _weapons = [c.id for c in model.category.select(model.category.q.groupName == 'combat')]
    ShowPage(CharSheet).main()
