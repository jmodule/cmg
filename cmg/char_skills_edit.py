#!/usr/bin/env python3

from page import DefaultPage
from templates.CharSkillsEdit import CharSkillsEdit

import auth
from jmodule.web import html
import model
from model.connect import (load_db_config, init_db)
import rules
import sqlobject as db

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__


class SkillPage(DefaultPage):

    def buildSkillList(self, genre = 'a'):
        """Return a hash of skills and IDs"""
        skills_out = {}

        if genre != 'a':
            skill_list = model.skill.select(
                db.AND(model.skill.q.invalid == False,
                       db.OR(model.skill.q.genre == genre,
                             model.skill.q.genre == 'g')))
        else:
            skill_list = model.skill.select(model.skill.q.invalid == False)
            
        for rec in skill_list:
            skills_out[rec.name] = rec.id
        return skills_out
    
    def update(self, **kwargs):
        """Update the records already in the database.
        :param **kwargs: table name is not used
        """
        
        if 'id' in self.defaults.form:
            skill_list = self.buildSkillList()
        
            try:
                total = int(self.defaults.form['total_skills'].value)
            except ValueError:
                total = 0

            for i in range(total):
                level_key = "level_%s" % i
                sid_key = "skill_id_%s" % i
                detail_key = "detail_%s" % i
                if sid_key in self.defaults.form and level_key in self.defaults.form:
                    skill = model.charSkills.get(self.defaults.form[sid_key].value)
                    skill.level = int(self.defaults.form[level_key].value)
                    if detail_key in self.defaults.form:
                        skill.detail = self.defaults.form[detail_key].value

            if 'new_skill' in self.defaults.form and 'new_level' in self.defaults.form:
                name = self.defaults.form['new_skill'].value
                if 'new_detail' in self.defaults.form:
                    detail = self.defaults.form['new_detail'].value
                else:
                    detail = ""
                if name in skill_list:
                    model.charSkills(
                        charID = int(self.defaults.form['id'].value),
                        skillID = int(skill_list[name]),
                        detail = detail,
                        level = int(self.defaults.form['new_level'].value)
                        )
            self.defaults.alerts.append("Updated Record")

    def processForm(self):
        """Edit the characters' skills."""
        # See if we need to do anything with the record
        #
        if 'cmd_update' in self.defaults.form:
            self.update()

        # check for a delete request
        #
        if 'cmd_del' in self.defaults.form:
            try:
                rec_id = int(self.defaults.form['skill_id'].value)
                model.charSkills.delete(rec_id)
                self.defaults.alerts.append("Record Deleted")
            except Exception as val:
                self.defaults.alerts.append("Error deleting record: %s" % val)

        self.defaults.action = my_name
        self.defaults.title = "Edit Skills"
        self.defaults.no_add = True

        if self.defaults.id is not None:
            info = model.charInfo.get(self.defaults.id)

            self.defaults.title += ' for ' + info.charName
            
            self.myvars['info'] = info

            self.myvars['char_skill_list'] = model.charSkills.select(
                db.AND(model.charSkills.q.charID==self.defaults.id,
                       model.charSkills.q.skillID==model.skill.q.id),
                orderBy=model.skill.q.name)

            self.myvars['skill_list'] = self.buildSkillList(info.genre)
            self.myvars['skill_names'] = sorted(self.myvars['skill_list'].keys())

        self.myvars['char_menu'] = auth.charMenu(self.defaults.session_id)
        

if __name__ == "__main__":
    init_db(load_db_config())
    SkillPage(CharSkillsEdit, save_id=True).main()
