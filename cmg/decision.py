#!/usr/bin/env python3

from page import DefaultPage
from templates.Decision import Decision

import argument

import const
import datetime
import model
from model.connect import (load_db_config, init_db)

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__

_title = "Decision"


class DecisionPage(DefaultPage):
    def update(self, table=None):
        """Save the results of a decision roll"""
        today = datetime.datetime.now()
        for (i, this_argument) in enumerate(self.defaults.form['argument']):
            level = int(self.defaults.form['strength'][i].value)
            roll = int(self.defaults.form['result'][i].value)
            model.log(
                charID = None,
                categ = const.log_id_decision,
                logtime = today,
                skill = today.strftime("%Y%m%dT%H%M%S"),
                level = level,
                roll = roll,
                comment = this_argument.value)
        self.defaults.alerts.append('Saved to log')

    def processForm(self):
        """Display and process the decision page"""

        args = {}
        if 'cmd_save' in self.defaults.form:
            self.update()
        
        if 'submit' in self.defaults.form:

            for side in argument.sides:
                args[side] = argument.Argument()
                for part in argument.parts:
                    control = "%s_%s" % (part, side)
                    if control in self.defaults.form:
                        setattr(args[side],
                                part,
                                self.defaults.form[control].value)
            argument.decide(args)

        self.defaults.action = my_name
        self.defaults.title = _title
        self.myvars = { 'arg_sides': argument.sides,
                        'argument': args,
                        'tries': argument.tries,
                        'max_tries': argument.max_tries,
                        }


if __name__ == "__main__":
    init_db(load_db_config())
    DecisionPage(Decision).main()
