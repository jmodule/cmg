import sqlobject as db
import rules


class inventory(db.SQLObject):
    """manages the chararacter inventory table"""
    
    char = db.ForeignKey('charInfo')
    eqpt = db.ForeignKey('equipment')
    qty = db.IntCol()
    notes = db.StringCol(length=50)
    inUse = db.IntCol()

    def dmgDice(self, secondary = False):
        trait_list = self.char.traitList()

        if secondary and self.eqpt.secDmgType != "":
            dmg_type = self.eqpt.secDmgBase
            dmg_amt = self.eqpt.secDmg
        else:
            dmg_type = self.eqpt.priDmgBase
            dmg_amt = self.eqpt.priDmg

        return rules.dice2str(rules.base_damage(dmg_type,
                                                trait_list,
                                                modifier=dmg_amt))

    def damage(self):
        """Return a list of damage strings"""
        
        dmg_str = [self.eqpt.priDmgType + " " + self.dmgDice()]

        if self.eqpt.secDmg != 0:
            dmg_str.append(self.eqpt.secDmgType + " " + self.dmgDice(True))
        return dmg_str

    def range(self, *arg, **kwarg):
        """Return the calculated range

        keywords:
          half -- calculate the half damage range
          max -- calculate the max range (Default)

        """

        trait_list = self.char.traitList()

        if 'half' in kwarg:
            base = self.eqpt.halfDmg
        else:
            base = self.eqpt.maxRng

        if self.eqpt.strForRng:
            if 'ST' in trait_list:
                range = float(trait_list['ST']['level']) * base
            else:
                range = -1.0
        else:
            range = base
        return range
            
    def dmgMult(self, secondary=False):
        """Return the damage multiplier"""
        if secondary and self.eqpt.secDmgType != "":
            m = rules.damage_type_mult[self.eqpt.secDmgType]
        else:
            m = rules.damage_type_mult[self.eqpt.priDmgType]
        return m
