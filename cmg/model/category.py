import sqlobject as db


class category(db.SQLObject):
    """manage the category table

    Note that with a non-integer key, the constructor requires the
    'id' keyword argument.  For example: category(id='ENG').
    
    """
    class sqlmeta:
        idType = str
    
    description = db.StringCol(length=50, alternateID=True, default="")
    validTable = db.StringCol(length=10, default="")
    groupName = db.StringCol(length=10, default="")
