import sqlobject as db
import rules


class charTraits(db.SQLObject):
    """manages the chararacter traits table"""

    # this has an auto-gen id
    char = db.ForeignKey("charInfo")
    traitCode = db.StringCol(length=10, default="")
    trait = db.ForeignKey("trait")
    level = db.IntCol()
    racialDiscount = db.IntCol(default=0)
    detail = db.StringCol(length=30, default="")
    
    def cost(self):
        """Return the point cost for the trait level for a character

        Trait base cost is how much the trait would cost if purchased
        at level 1.  Many traits only have one level.  Traits with
        more than one level have the incremental cost which is each
        for each point purchased after the first.  For example:

        Alertness +2/level - base cost: 2, incr_cost: 2, max level: None

        Magery 15/25/35 - base cost: 15, incr_cost: 10, max level: 3

        Danger Sense 15 - base cost: 15, incr_cost: 0, max level: 1

        Attributes are calculated differently from the adv/disad types
        in that the base cost represents the base attribute level
        where cost = 0, i.e. 10.  The incremental cost is also
        modified by the level as well.

        Attributes also have the possiblity of having a racial
        discount that is calculated seperately from the levels
        purchased normally.  For example Reptilemen get ST +4 as a
        racial bonus, so the discount should be entered as 4.  So for
        a character with ST 17 it will first calculate the points
        purchased by the character (17 - 4 = 13) and then calculate
        the points from the racial package (10 + 4 = 14).

        """
        if self.racialDiscount > 0:
            trait_cost = rules.calcTraitCost(self.level - self.racialDiscount,
                                             self.trait.baseCost,
                                             self.trait.incrCost,
                                             self.trait.code)
            # I'm adding 10 here because when the bonus says ST +4 it
            # means to add 4 to the base attribute of 10 and that's
            # what the cost function expects.
            trait_cost += rules.calcTraitCost(self.racialDiscount + 10,
                                              self.trait.baseCost,
                                              self.trait.incrCost,
                                              self.trait.code)
        else:
            trait_cost = rules.calcTraitCost(self.level,
                                             self.trait.baseCost,
                                             self.trait.incrCost,
                                             self.trait.code)
        return trait_cost

    def fullName(self):
        """Return the name and detail as a string"""
        name_out = self.trait.name
        if self.detail.strip() != "":
            name_out += " (%s)" % self.detail
        return name_out
