import sqlobject as db


class log(db.SQLObject):
    """Logs for die rolls and eventually other things"""
    
    logtime = db.DateTimeCol()
    char = db.ForeignKey('charInfo')
    categ = db.ForeignKey('category', sqlType='STR')
    skill = db.StringCol(length=55)
    level = db.IntCol()
    roll = db.IntCol()
    comment = db.StringCol(default = "")
