import sqlobject as db


class trait(db.SQLObject):
    """manages the traits master table"""
    class sqlmeta:
        table = "traits"

    code = db.StringCol(length=4, default="")
    name = db.StringCol(length=50, alternateID=True, default="")
    dependsOn = db.StringCol(length=25, default=None)
    canDisplay = db.StringCol(length=1, default="Y")
    baseCost = db.IntCol(default=0)
    incrCost = db.IntCol(default=0)
    maxLevel = db.IntCol(default=1)
    category = db.ForeignKey('category', default="")
    invalid = db.BoolCol(default = 0)
