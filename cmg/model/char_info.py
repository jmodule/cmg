import sqlobject as db
import math
from const import armor_attributes, armor_locations, armor_bonuses
import rules


def sortInventory(a, b):
    """Sorts the inventory by the equipment description"""

    a_desc = a.eqpt.description
    b_desc = b.eqpt.description
    
    if a_desc > b_desc:
        result = 1
    elif a_desc < b_desc:
        result = -1
    else:
        result = 0

    return result


class pointCost:
    """A helper class that stores all the point costs"""
    def __init__(self):
        self.totalAttr = 0.0
        self.totalAdv = 0.0
        self.totalDis = 0.0
        self.totalQuirks = 0.0
        self.totalSkills = 0.0
        self.unspent = 0.0

    def __str__(self):
        return "Total Points: " + str(self.total())

    def total(self):
        total = self.totalAttr + self.totalAdv + self.totalDis + self.totalQuirks + self.totalSkills
        return total


class charInfo(db.SQLObject):
    """manages the chararacter basic info table"""

    charName = db.StringCol(default='', length=50, alternateID=True)
    playerName = db.StringCol(length=50, default='')
    username = db.StringCol(length=15, default='')
    portraitLink = db.StringCol(length=200, default='')
    description = db.StringCol(default='')
    notes = db.StringCol(default='')
    race = db.StringCol(length=50, default='')
    sex = db.StringCol(length=1, default='')
    age = db.IntCol(default=20)
    unspentPoints = db.IntCol(default=0)
    parry = db.ForeignKey('charSkills', default=None)
    block = db.ForeignKey('charSkills', default=None)
    health = db.IntCol(default=0)
    fatigue = db.IntCol(default=0)
    title = db.StringCol(length=30, default='')
    genre = db.StringCol(length=1, default='a')
    campaign = db.StringCol(length=30, default='')
    # createdOn = db.DateCol(default=0)
    size = db.IntCol(default=0)
    tl = db.IntCol(default=0)
    religion = db.StringCol(length=30, default='')
    hair = db.StringCol(length=15, default='')
    eyes = db.StringCol(length=15, default='')
    skin = db.StringCol(length=15, default='')
    handed = db.StringCol(length=1, default='R')
    profession = db.StringCol(length=30, default='')
    status = db.BoolCol(default=True) # active/inactive
    # isNpc = db.BoolCol(default=False)

    mySkills = db.MultipleJoin('charSkills',
                               joinColumn="char_id",
                               joinMethodName="mySkills")
    myInv = db.MultipleJoin('inventory',
                            joinColumn="char_id",
                            joinMethodName="myInv")
    myTraits = db.MultipleJoin('charTraits',
                               joinColumn="char_id",
                               joinMethodName="myTraits")
    myQuirks = db.MultipleJoin('charQuirks',
                               joinColumn="char_id",
                               joinMethodName="myQuirks")

    def traitList(self):
        """Return a hash of traits and levels for backwards compatability"""
        trait_list = {}
        for t in self.myTraits:
            trait_list[t.trait.name] = {'level': t.level,
                                        'category': t.trait.categoryID}
        return trait_list

    def advantages(self):
        """Return a list of traits in the advantage category"""

        adv_out = []
        for t in self.myTraits:
            if t.trait.categoryID == 'adv':
                adv_out.append(t)

        return adv_out

    def disadvantages(self):
        """Return a list of disadvantage traits"""
        dis_out = []
        for t in self.myTraits:
            if t.trait.categoryID == 'dis':
                dis_out.append(t)

        return dis_out

    def cost(self):
        """Return a pointCost object with all the totals"""
        points = pointCost()
        for t in self.myTraits:
            if t.trait.categoryID == 'abs':
                points.totalAttr += t.cost()
            elif t.trait.categoryID == 'adv':
                points.totalAdv += t.cost()
            elif t.trait.categoryID == 'dis':
                points.totalDis += t.cost()

        for s in self.mySkills:
            points.totalSkills += s.pointCost()

        points.totalQuirks -= len(list(self.myQuirks))

        return points

    def armor(self):
        """Return a dict of armor locations with their PD and DR values"""
        armor = dict()
        for attr in armor_attributes:
            armor[attr] = dict()
            for loc in armor_locations:
                armor[attr][loc] = 0

        # special case for places with no armor
        armor['dr']['no'] = 0
        armor['pd']['no'] = 0

        for item in self.myInv:
            if item.eqpt.category.id == 'arm' and item.inUse == 1:
                for attr in armor_attributes:
                    for loc in armor_locations:
                        rec_key = loc + attr.capitalize()
                        armor[attr][loc] += getattr(item.eqpt, rec_key, 0)

        # traits can affect armor values
        bonuses = list(armor_bonuses.keys())
        for t in self.myTraits:
            if t.trait.name in bonuses:
                attr = armor_bonuses[t.trait.name]
                for loc in armor_locations:
                    armor[attr][loc] += t.level
        
        return armor

    def getInventory(self):
        """Return a sorted list of displayable inventory"""
        all_inv = []
        for i in self.myInv:
            if i.eqpt.showOnInv == 1:
                all_inv.append(i)
        # all_inv.sort(sortInventory)
        all_inv.sort(key=lambda a: a.eqpt.description)
        return all_inv

    def invWt(self, *args, **kwargs):
        """Return the total weight in the inventory

        keywords:
        * carried - count only the items marked as in-use.

        """
        if 'carried' in kwargs:
            carried_only = kwargs['carried']
        else:
            carried_only = False
        
        wt = 0
        for item in self.myInv:
            if not carried_only or item.inUse:
                wt += item.eqpt.weight * item.qty
        return wt

    def invCost(self):
        """Return the total cost of the inventory"""
        cost = 0
        for item in self.myInv:
            cost += item.eqpt.cost * item.qty
        return cost

    def invList(self, categ=None):
        """return a dictionary of the inventory name and object"""
        inv_list = {}

        for item in self.myInv:
            if categ is None or item.eqpt.categoryID in categ:
                inv_list[item.eqpt.description] = item

        return inv_list

    def encumbrance(self):
        """return the encumbrance move penalty"""

        trait_list = self.traitList()
        inv_wt = self.invWt(carried=True)

        penalty = 0
        for enc in rules.encumbrance_levels:
            load = enc['mult'] * trait_list['ST']['level']
            if inv_wt <= load:
                penalty = enc['mod']
                break
        return penalty

    def speed(self):
        """calculate and return the basic speed of the character
        
        Normally this could be modified by advantages or skills, but we'll
        ignore that and calculate the most basic speed based only on
        attributes
        
        """
        trait_list = self.traitList()
        ht = trait_list['HT']['level']
        dx = trait_list['DX']['level']
        speed = float(ht + dx) / 4
        return speed

    def move(self, use_enc=True):
        """return the move value for the character

        For the character sheet I can turn off the encumbrance penalty.

        """
        m = int(math.floor(self.speed()))
        if use_enc:
            m -= self.encumbrance()
        return m

    def _armorPd(self, location):
        """return the passive defence for the location

        Internal function used for calculating the active defenses
        below.

        """
        armor_mod = 0
        armor = self.armor()
        if 'pd' in armor:
            if location in armor['pd']:
                armor_mod = armor['pd'][location]
        return armor_mod

    def dodge(self, *args, **kwargs):
        """Return the dodge score for the character

        dodge() -> dodge_value

        keywords:
          location: hit location, defaults to torso
          encumbrance: include penalty in the total, default True

        """

        if 'location' in kwargs:
            location = kwargs['location']
        else:
            location = 'torso'

        if 'encumbrance' in kwargs:
            use_enc = kwargs['encumbrance']
        else:
            use_enc = True

        return self.move(use_enc) + self._armorPd(location)

    def parryName(self):
        """Return the name of the skill used to parry with"""
        n = "None"
        if self.parry is not None:
            n = self.parry.skill.name
        return n

    def parryLevel(self):
        """return the parry level"""
        p = 0
        try:
            if self.parry is not None:
                p = (self.parry.gameLevel() / 2) + self._armorPd('torso')
        except db.SQLObjectNotFound:
            pass
        return p

    def blockLevel(self):
        """return the block level"""
        b = 0
        try:
            if self.block is not None:
                b = (self.block.gameLevel() / 2) + self._armorPd('torso')
        except db.SQLObjectNotFound:
            pass
        return b

    def userName(self, force=False):
        """return a username based on player name

        Requires that the player's name be 'first last', if not it
        returns the whole username and hopes for the best.
        """
        if self.username == "" or force:
            try:
                u = self.playerName[0:1] + self.playerName[self.playerName.index(" ") + 1:]
                self.username = u.lower()
            except:
                self.username = self.playerName.lower()
        return self.username

    def getAllSkills(self, as_list=True):
        """Return a hash the skills and attributes with the skill level

        Precalculates the to-hit level based on attribute so the
        calling process can then use the level in a die roll.
        
        """
        trait_list = self.traitList()
        skill_list = dict()
        for s in self.mySkills:
            skill_list[s.fullName()] = s.gameLevel()

        for a in rules.attributes:
            skill_list[a] = trait_list[a]['level']

        for a in list(rules.calculated_attributes.keys()):
            level = rules.calc_attr(a, trait_list)
            skill_list[a] = level

        skill_list['Defense: Dodge'] = self.dodge()
        skill_list['Defense: Parry'] = self.parryLevel()
        skill_list['Defense: Block'] = self.blockLevel()
        
        return skill_list
