import sqlobject as db
from rules import skill_costs
import model


class charSkills(db.SQLObject):
    """manages the chararacter skills table"""

    char = db.ForeignKey('charInfo')
    skill = db.ForeignKey('skill')
    level = db.IntCol()
    detail = db.StringCol(length=30)

    def fullName(self):
        """Return the name and detail as a string"""
        name_out = self.skill.name
        if self.detail.strip() != "":
            name_out += " (%s)" % self.detail
        return name_out

    def pointCost(self, modifier=0):
        """Return the calculated cost of this skill

        The modifier argument can be used to find the cost of
        increasing the skill without modifying the skill.

        """
        cost = 0

        level = self.level + modifier

        attr = skill_costs[self.skill.traitType]['attr']
        (start, break_point, incr_cost) = skill_costs[self.skill.traitType][self.skill.diffCode]
        if level <= break_point:
            cost = (0.5 * 2 ** level)
        else:
            cost = (0.5 * 2 ** break_point) + (level - break_point) * incr_cost
        
        # 0.5 is the floor for costs, if it's less than that,
        # something funky is going on.  Either it's a special
        # skill that doesn't have a point cost, or the user
        # entered a negative level and we don't want to display
        # the point cost for that.
        #
        if cost < 0.5:
            cost = 0
    
        return cost

    def gameLevel(self, *arg, **kwds):
        """Return the calculated skill level

        It can also do the reverse calculation to convert a calculated
        skill level back to what is stored in the database by
        specifying the 'reverse' keyword with the calculated skill
        level.

        """
        level_out = 0
        bonus = 0

        trait_list = self.char.traitList()

        ttype = self.skill.traitType
        dcode = self.skill.diffCode
        categ = self.skill.categoryID
        attr = skill_costs[ttype]['attr']
        (start, break_point, incr_cost) = skill_costs[ttype][dcode]
        
        base_trait = model.charTraits.select(
            db.AND(model.charTraits.q.traitCode == attr,
                   model.charTraits.q.charID == self.charID))
        if len(list(base_trait)) > 0:
            trait_level = list(base_trait)[0].level
        else:
            trait_level = 0

        # Look for bonuses from advantages
        #
        ## DEBUG ##
        #print trait_list
        ## DEBUG ##
        if categ == 'mag':
            if 'Magery' in trait_list:
                bonus = trait_list['Magery']['level']

        if 'reverse' in kwds:
            level_out = kwds['reverse'] - start - trait_level - bonus
        else:
            level_out = start + trait_level + self.level + bonus
        return level_out
        
    def fatigue(self, size=1):
        """return the amount of fatigue caused by casting a spell"""
        used = 0
        if self.skill.category.description == 'Magic':
            used = self.skill.castingCost * size
            level = self.gameLevel()
            
            # these are the break-points where, when the skill reaches
            # this level, the character gets a fatigue bonus when
            # casting a spell.
            #
            if level >= 20:
                used -= 2
            elif level >= 15:
                used -= 1
              
            if used < 0:
                used = 0
        return used
