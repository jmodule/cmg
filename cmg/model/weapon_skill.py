import sqlobject as db


class weaponSkills(db.SQLObject):
    """weapon skills link a weapon back to the skills that can use it

    weaponSkills(equipment, skill, modifier) => record

    The modifier is for other defaults, ex: DX-5, Shortsword-3

    """

    equipment = db.ForeignKey('equipment')
    skill = db.ForeignKey('skill')
    modifier = db.IntCol(default=0)
    
