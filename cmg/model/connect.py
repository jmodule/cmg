"""Sets up the connection to the database"""
import sys

import sqlobject
import configparser
from pathlib import Path


def load_db_config(alt_path=None):
    if alt_path is None:
        config_path = Path("./instance/cmg.conf")
        # here = pathlib.Path(__file__).parent.resolve()
        # config_path = here.joinpath('instance').joinpath('cmg.conf')
    else:
        config_path = Path(alt_path)

    config = configparser.ConfigParser()
    config.read(str(config_path))

    try:
        uri = "mysql://{0}:{1}@{2}/{3}".format(config.get('DATABASE', 'username'),
                                               config.get('DATABASE', 'password'),
                                               config.get('DATABASE', 'hostname'),
                                               config.get('DATABASE', 'database'))

    except configparser.NoSectionError as exc:
        uri = None
        print("Unable to read database configuration:", exc, file=sys.stderr)
    return uri


def init_db(db_uri, db_driver=None):
    if db_driver is not None:
        connection = sqlobject.connectionForURI(db_uri, driver=db_driver)
    else:
        connection = sqlobject.connectionForURI(db_uri)
    sqlobject.sqlhub.processConnection = connection
