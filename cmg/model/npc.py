import sqlobject as db


class npc(db.SQLObject):
    """table to hold NPC specific data

    NPC's are not as detailed as PC's so I've created a seperate table
    to track the few attributes that they need.

    """

    _WEAK = 0
    _AVERAGE = 1
    _STRONG = 2

    _levels = { 'm': [12, 13, 14],
                'p': [12, 13, 14] }
    
    char = db.ForeignKey("charInfo");
    physical = db.IntCol(default = 1);
    mental = db.IntCol(default = 1);

    def default(self, which="p"):
        """Returns a default level based on the physical level"""
        level = 0

        if which in list(self._levels.keys()):
            if which == 'p':
                l_indx = self.physical
            else:
                l_indx = self.mental
            level = self._levels[which][l_indx]

        return level
