import sqlobject as db


class equipment(db.SQLObject):
    """manages the equipment table"""

    description = db.StringCol(default='', length=50, alternateID=True)
    category = db.ForeignKey('category', default="")
    weight = db.FloatCol(default=0.0)
    cost = db.FloatCol(default=0.0)
    notes = db.StringCol(default="")
    
    malf = db.StringCol(length=8, default="")
    reach = db.IntCol(default=0)
    
    ss = db.IntCol(default=0)
    acc = db.IntCol(default=0)
    halfDmg = db.FloatCol(default=0.0)
    maxRng = db.FloatCol(default=0.0)
    rof = db.IntCol(default=0)
    shots = db.IntCol(default=0)
    st = db.IntCol(default=0)
    recoil = db.IntCol(default=0)
    strForRng = db.BoolCol(default=False)
    
    tl = db.IntCol(default=0)
    
    priDmg = db.IntCol(default=0)
    priDmgBase = db.StringCol(length=1, default="")
    priDmgMax = db.IntCol(default=0)
    priDmgType = db.StringCol(length=3, default="")
    
    secDmg = db.IntCol(default=0)
    secDmgBase = db.StringCol(length=1, default="")
    secDmgMax = db.IntCol(default=0)
    secDmgType = db.StringCol(length=3, default="")

    headPd = db.IntCol(default=0)
    torsoPd = db.IntCol(default=0)
    armPd = db.IntCol(default=0)
    legPd = db.IntCol(default=0)

    headDr = db.IntCol(default=0)
    torsoDr = db.IntCol(default=0)
    armDr = db.IntCol(default=0)
    legDr = db.IntCol(default=0)

    showOnInv = db.IntCol(default=1)

    mySkills = db.MultipleJoin('weaponSkills',
                               joinMethodName="mySkills")
