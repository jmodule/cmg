import sqlobject as db


class skill(db.SQLObject):
    """manages the skill master table"""
    class sqlmeta:
        table = "skills"

    name = db.StringCol(length=25, alternateID = True, default=None)
    requiresTable = db.StringCol(length=10, default=None)
    requiresId = db.StringCol(length=10, default=None)
    traitType = db.StringCol(length=1, default="")
    diffCode = db.StringCol(length=1, default="")
    altAttr = db.StringCol(length=2, default="")
    category = db.ForeignKey('category', default="")
    college = db.StringCol(length=30, default="")
    genre = db.StringCol(length=1, default="g")
    parry = db.FloatCol(default=0.0)
    castingCost = db.IntCol(default=0)
    castingTime = db.StringCol(length=10, default="")
    maintainCost = db.IntCol(default=0)
    duration = db.StringCol(length=10, default="")
    notes = db.StringCol(default="")
    invalid = db.BoolCol(default=0)
    
