import sqlobject as db


class charQuirks(db.SQLObject):
    """manages the chararacter quirks table"""
    class sqlmeta:
        idName = "quirk_id"

    charId = db.IntCol()
    # quirkId = db.IntCol()
    description = db.StringCol(length=50)
