#!/usr/bin/env python3

"""This is a template class for all the pages.

It has all the defaults that a page needs to display the master template.

"""

import cgi

import cgitb
cgitb.enable()

from jmodule.web import html, login
from my_menu import menu
import auth
import const
import os
from os import linesep as eol
import sqlobject
from templates.Index import Index


class DefaultPage:
    """This is a default page that individual pages should be based on"""

    class Placeholders:
        """This holds the default values that every page needs"""
        action = 'cmg.py'
        logout_link = 'cmg.py?logout=True&session_id=%s'
        title = 'Character Manager For GURPS'
        css_links = [const.css['cmg'], ]
        footer = const.footer
        session_id = None
        form = None
        jsfile = None
        js = None
        no_add = False
        id = None
        alerts = []
        always_show_menu = False
        public = const.public

    def __init__(self, template_class=None, *arg, **kwarg):
        """Set up the default values"""
        self.defaults = self.Placeholders()
        self.template = template_class
        self.myvars = {}
        if 'save_id' in kwarg:
            self.can_save_id = kwarg['save_id']
        else:
            self.can_save_id = False

        # comment this out for production
        self.defaults.css_links.append(const.css['development'])

    def login(self):
        """return true if the session is active"""
        self.defaults.session_id = login.check('cmg.py', timeout=30)
        self.defaults.username = login.getUsername(self.defaults.session_id)
        self.defaults.role = login.getAccess(self.defaults.session_id)
        self.defaults.form = login.session_file.form
        self.defaults.logout = self.defaults.logout_link % self.defaults.session_id
        if self.can_save_id:
            self.defaults.id = auth.getId(self.defaults.session_id)
        return self.defaults.session_id != -1

    def processForm(self):
        """evaluate the form variables and update the searchlist"""
        pass

    def parseJavascript(self):
        """Prepare any javascript to the header"""
        script = ""
        if self.defaults.js is not None:
            script += '<script type="text/javascript" language="javascript">' + eol
            script += '<!-- hide script' + eol
            script += "// length = %s%s" % (len(self.defaults.js), eol)
            if self.defaults.js is list:
                for line in self.defaults.js:
                    script += line + eol
            else:
                script += self.defaults.js + eol
            script += '// end hide script -->' + eol
            script += '</script>' + eol

        if self.defaults.jsfile is not None:
            if isinstance(self.defaults.jsfile, str):
                self.defaults.jsfile = (self.defaults.jsfile,)
            for filename in self.defaults.jsfile:
                link = const.public + filename
                script += '<script language="javascript" src="%s">' % link
                script += '</script>' + eol
        if script != "":
            self.defaults.javascript = script

    def update(self, table):
        """This can be used to update an anonymous sqlobject table"""

        if self.defaults.id is not None:
            try:
                record = table.get(self.defaults.id)
            except sqlobject.SQLObjectNotFound:
                record = table(id=self.defaults.id)
        else:
            record = table()
        
        for key in table.sqlmeta.columns:
            if key in self.defaults.form:
                col = table.sqlmeta.columns[key].columnDef
                if isinstance(col, sqlobject.IntCol):
                    value = int(self.defaults.form[key].value)
                elif isinstance(col, sqlobject.DateTimeCol):
                    # also need to parse the date here
                    try:
                        day = parseDate(kwd['day'])
                    except Exception:
                        day = parseDate('today')
                    date_parts = day.split('-')
                    value = datetime.date(int(date_parts[0]),
                                          int(date_parts[1]),
                                          int(date_parts[2]))
                    #raise Exception('Date: %s' % value)
                elif isinstance(col, sqlobject.FloatCol):
                    value = float(self.defaults.form[key].value)
                else:
                    value = self.defaults.form[key].value
                    # convert a few special types back to what they should be
                    #
                    if value == "None":
                        value = None
                    elif value == "False":
                        value = False
                    elif value == "True":
                        value = True

                setattr(record, key, value)

    def main(self):
        """render and print the page

        If a new menu has been selected, then we'll display the menu
        summary/help as the main content instead.

        """
        menu_help = None

        if self.login():
            try:
                # see if the user has selected a character
                #
                for name in ('show', 'id', 'char_id', 'sel_id'):
                    if name in self.defaults.form:
                        self.defaults.id = self.defaults.form[name].value
                        break

                # make sure that blanks and the None string get converted
                # back to the None type.
                if self.defaults.id == "" or self.defaults.id == "None":
                    self.defaults.id = None

                if self.can_save_id:
                    login.setId(self.defaults.session_id, self.defaults.id)
                else:
                    login.setId(self.defaults.session_id, "")
                    
                self.processForm()
                self.parseJavascript()

                # set up the menu items
                if 'menu' in self.defaults.form:
                    login.setTable(self.defaults.session_id,
                                   self.defaults.form['menu'].value)
                    try:
                        menu_help = int(self.defaults.form['menu'].value)
                    except ValueError:
                        menu_help = 0

                self.defaults.tabs = menu.tabs(
                    self.defaults.session_id,
                    self.defaults.action,
                    login.getTable(self.defaults.session_id))

                self.defaults.menu = menu.sidebar(
                    self.defaults.session_id,
                    login.getTable(self.defaults.session_id))

                # Show the page template now, unless the user selected
                # a new menu tab at the top, and then I want to show a
                # summary of the menu items, or something.  Also, if
                # there is an error and the template has not been set,
                # then we still need to show a default index page.
                #
                if menu_help is None and self.template is not None:
                    print((self.template(searchList=[self.myvars,self.defaults])))
                else:
                    self.myvars['bodycontent'] = menu.help(self.defaults.session_id, menu_help)
                    print((Index(searchList=[self.myvars,self.defaults])))
            except Exception:
                html.begin(title=self.defaults.title,
                           stylesheet=const.css['cmg'])
                cgi.print_exception()


if __name__ == "__main__":
    DefaultPage().main()
