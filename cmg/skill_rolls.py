#!/usr/bin/env python3

import auth
from dice import Dice
import math
import model
from model.connect import (load_db_config, init_db)
from page import DefaultPage
import rules
import const
from sqlobject import AND, IN

from templates.SkillRolls import SkillRolls

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__

_title = "Skill Rolls"


class WeaponSkillError(Exception):
    def __init__(self, wpn_name):
        self.name = wpn_name

    def __str__(self):
        return "No skills found for " + self.name


def skillRoll(char_id, full_name, modifier, spell_size):
    """return a dice object with the result of the roll

    Important: in the GURPS rules the modifier is applied to the skill
    level and not the die roll.  For example: rolling against a skill
    of 10 for a difficult task with a modifier of -4 would have a
    formula of 3d6 vs (10 - 4 = 6).

    A data member called outstanding is added to the dice object.
    
    """

    fatigue = -1
    skill_parts = full_name.split('(')
    skill_name = skill_parts[0].strip()
    if len(skill_parts) > 1:
        skill_detail = skill_parts[1][:-1].strip()
    else:
        skill_detail = ""

    if skill_name in rules.attributes:
        rec = model.charTraits.select(
            AND(model.charTraits.q.charID == char_id,
                model.charTraits.q.traitCode == skill_name))
        try:
            level = int(rec[0].level)
        except (ValueError, IndexError):
            level = 0
    elif skill_name in list(rules.calculated_attributes.keys()):
        rec = model.charInfo.get(char_id)
        level = rules.calc_attr(skill_name, rec.traitList())
    elif skill_name == 'Dodge':
        rec = model.charInfo.get(char_id)
        level = rec.dodge()
    else:
        try:
            rec = model.charSkills.select(
                AND(model.charSkills.q.charID == char_id,
                    model.skill.q.name == skill_name,
                    model.charSkills.q.detail == skill_detail,
                    model.charSkills.q.skillID == model.skill.q.id))[0]
            level = rec.gameLevel()
            fatigue = rec.fatigue(spell_size)
        except (ValueError, IndexError ):
            level = 0
    try:
        modifier = int(modifier)
    except ValueError:
        modifier = 0

    dice = Dice('3d6', level, modifier)
    dice.roll()

    # I'm adding the outstanding result to the dice object so I only have
    # to return one variable. :-)
    #
    if dice.roll_result <= (dice.target - 10) or dice.roll_result >= (dice.target + 10):
        dice.outstanding = True
    else:
        dice.outstanding = False

    # I'm also adding a value for the amount of fatigue caused by the task
    dice.fatigue = fatigue

    return dice


class SkillRollsPage(DefaultPage):

    def processForm(self):
        global _title

        wpn_list = {}
        
        # if there is a valid char id, then grab the record
        if self.defaults.id is not None:
            char = model.charInfo.get(self.defaults.id)
            # create a hash of the weapons by name for later
            wpn_list = char.invList(_weapons)
        else:
            char = None

        # see if the other fields are set
        if 'sel_skill' in self.defaults.form:
            sel_skill = self.defaults.form['sel_skill'].value
        else:
            sel_skill = None

        if "sel_weapon" in self.defaults.form:
            sel_weapon = self.defaults.form['sel_weapon'].value
        else:
            sel_weapon = None

        if 'modifier' in self.defaults.form:
            try:
                modifier = int(self.defaults.form['modifier'].value)
            except ValueError:
                modifier = 0
        else:
            modifier = 0

        if 'sel_target' in self.defaults.form:
            target_name = self.defaults.form['sel_target'].value
        else:
            target_name = None

        if 'sel_defense' in self.defaults.form:
            defense = self.defaults.form['sel_defense'].value
        else:
            defense = None

        spell_size = 0
        if 'sel_spell_size' in self.defaults.form:
            try:
                spell_size = int(self.defaults.form['sel_spell_size'].value)
            except ValueError:
                pass

        # if we need to "roll dice"
        if 'cmd_roll' in self.defaults.form:
            dice = skillRoll(self.defaults.id, sel_skill, modifier, spell_size)
        else:
            dice = None

        if 'cmd_weapon' in self.defaults.form:
            wpn_dice = self.weaponRoll(self.defaults.id,
                                       sel_weapon,
                                       wpn_list,
                                       target_name,
                                       defense,
                                       spell_size)
        else:
            wpn_dice = None

        self.myvars = {
            'info': char,
            'char_menu': auth.charMenu(self.defaults.session_id),
            'dice': dice,
            'show_delete': False,
            'sel_skill': sel_skill,
            'sel_weapon': sel_weapon,
            'title': _title,
            'weapons': list(wpn_list.keys()),
            'wpn_dice': wpn_dice,
            'modifiers': list(rules.cover_modifiers.items()),
            'sel_target': target_name,
            'sel_defense': defense,
            'sel_spell_size': spell_size,
            }

        self.defaults.action = my_name
        self.defaults.jsfile = ('log.js', 'ajax.js', 'dice.js')
        self.defaults.no_add = True

    def weaponRoll(self, char_id, wpn_name, wpn_list, target_name, defense, spell_size):
        """return a Dice object with the results of an attack

        weaponAttack(char_id, wpn_name, wpn_list, target_name, defense)
            -> Dice tuple

        The skill with a given weapon is taken from the weaponSkills
        table.  Then the weapon roll will include rolling for a hit as
        well as rolling for damage.  But I guess there also needs to
        be a 'dodge' roll somewhere in there too.  This probably
        should go in the rules module so I can use it for the web and
        a CUI client.

        """

        info = model.charInfo.get(char_id)
        item = wpn_list[wpn_name]
        try:
            possible_skills = []
            modifiers = {}
            for s in item.eqpt.mySkills:
                possible_skills.append(s.skill.id)
                modifiers[s.skill.id] = s.modifier
            if len(possible_skills) > 0:
                skill = model.charSkills.select(
                    AND(IN(model.charSkills.q.skillID, possible_skills),
                        model.charSkills.q.charID == info.id))[0]
                level = int(skill.gameLevel())
            else:
                raise WeaponSkillError(wpn_name)
            if skill.skill.id in modifiers:
                level += modifiers[skill.skill.id]
            name = skill.skill.name
        except (ValueError, IndexError):
            level = 0
            name = ""
        except WeaponSkillError as val:
            name = "DX"
            try:
                level = int(info.traitList()['DX']['level'])
            except ValueError:
                level = 0
            self.defaults.alerts.append(str(val))

        # determine the modifiers related to (based on) the weapon/attacker
        wpn_mod = 0

        if 'chk_aim' in self.defaults.form:
            aiming = True
            wpn_mod += item.eqpt.acc
        else:
            aiming = False

        secondary = False
        if 'rdo_dmg_type' in self.defaults.form:
            if self.defaults.form['rdo_dmg_type'].value == const.secondary:
                secondary = True

        # skill must be higher than snap shot requirement for unaimed shots
        # (assuming that hand weapons will have a ss value of 0)
        if level < item.eqpt.ss and not aiming:
            wpn_mod -= 4

        # now determine the target's modifiers
        tgt_mod = 0
        if 'rdo_cover' in self.defaults.form:
            try:
                tgt_mod += int(self.defaults.form['rdo_cover'].value)
            except ValueError:
                pass

        # see if the user specified a target character set up
        # defaults, including the active defense (if any)
        hit_location = 'torso'
        act_def = 5 # default for a character with attributes all 10
        dr_mod = 0
        if target_name is not None:
            if target_name != const.anonymous_target:
                try:
                    target = model.charInfo.select(
                        model.charInfo.q.charName == target_name)[0]
                    armor = target.armor()
                    dr_mod = armor['dr'][hit_location]
                    if defense == 'Dodge':
                        act_def = target.dodge(location=hit_location)
                    elif defense == 'Parry':
                        act_def = target.parryLevel()
                    elif defense == 'Block':
                        act_def = target.blockLevel()
                    else:
                        act_def = 0
                except IndexError:
                    pass
                
        # add all the modifiers together
        modifier = wpn_mod + tgt_mod

        to_hit = Dice('3d6', level, modifier)
        to_hit.skill = name
        to_hit.roll()

        to_dmg = Dice(item.dmgDice(secondary))
        #
        # Modify the dice for spells (hackish)
        #
        # I don't like hard coding this here, but I don't have a good
        # way at the moment to check for required categories.
        if item.eqpt.category.id == 'spl':
            to_dmg.number *= spell_size
            to_dmg.modifier *= spell_size
        to_dmg.weapon = item
        to_dmg.net_dmg = 0

        to_dodge = Dice('3d6', act_def)
        to_dodge.roll()
        to_dodge.skill = defense

        if to_hit.success() and not to_dodge.success():
            to_dmg.roll()
            to_dmg.net_dmg = int(math.floor((to_dmg.result() - dr_mod) * item.dmgMult(secondary)))

        return to_hit, to_dmg, to_dodge


if __name__ == "__main__":
    # ######################## TESTING #########################
    # import profile
    # profile.run('main()', 'profile.tmp')
    # ######################## TESTING #########################
    init_db(load_db_config())
    _weapons = [c.id for c in model.category.select(model.category.q.groupName == 'combat')]
    SkillRollsPage(SkillRolls, save_id=True).main()
