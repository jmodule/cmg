#!/usr/bin/env python3

"""This is a limited character information editor for use by players"""

from page import DefaultPage
from templates.CharEdit import CharEdit

import auth
import model
from model.connect import (load_db_config, init_db)
from db import form
from jmodule.web import html

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__

hide_fields = ['height',
               'weight',
               'unspentPoints',
               'parryID',
               'health',
               'genre',
               'fatigue',
               'username',
               'blockID',
               'charName',
               'playerName',
               'portraitLink',
               'status',
               'campaign',
               'tl',
               'size']


class EditPage(DefaultPage):
    def processForm(self):
        self.defaults.action = my_name
        self.defaults.no_add = True
        self.defaults.title = "Edit Character Information"
        
        content = ""

        if self.defaults.id is not None:
            info = model.charInfo.get(self.defaults.id)
        else:
            info = None
    
        # See if we need to do anything with the record
        #
        if 'cmd_update' in self.defaults.form and info is not None:
            # if the user is restricted then their username must match
            # the player name to be able to update the character
            #
            if auth.canUpdate(self.defaults.session_id, self.defaults.id):
                self.update(info)
                self.defaults.alerts.append("Record Updated")
            else:
                self.defaults.alerts.append("You are not allowed to update this character!")
            
        content += '<form name="info" action="' + my_name + '" method="post">'
        content += form.fill(model.charInfo,
                             id=self.defaults.id,
                             hide=hide_fields)
        content += html.hidden('session_id', self.defaults.session_id)
        content += '</form>'

        self.myvars = {
            'info': info,
            'bodycontent': content,
            'char_menu': auth.charMenu(self.defaults.session_id),
            }


if __name__ == "__main__":
    init_db(load_db_config())
    EditPage(CharEdit, save_id=True).main()
