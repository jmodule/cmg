#!/usr/bin/env python3

import auth
import const
import datetime
import model
from model.connect import (load_db_config, init_db)
from os import linesep as eol
from page import DefaultPage
import rules
import sqlobject as db
from templates.SpendPoints import SpendPoints

try:
    my_name = __file__[__file__.rindex("/") + 1:]
except IndexError:
    my_name = __file__


class PointsPage(DefaultPage):
    def update(self, info):
        """Update the skill levels and unspent points"""

        if 'skill_id' in self.defaults.form:
            if isinstance(self.defaults.form['skill_id'], list):
                for (i, sid) in enumerate(self.defaults.form['skill_id']):
                    skill = model.charSkills.get(sid.value)
                    new_level = int(self.defaults.form['sel_level'][i].value)
                    new_level = skill.gameLevel(reverse=new_level)
                    level_change_amt = new_level - skill.level
                    if level_change_amt > 0:
                        # save a log entry for each update
                        today = datetime.datetime.now()
                        model.log(
                            charID=skill.char.id,
                            categ=const.log_id_spend_points,
                            logtime=today,
                            skill=skill.skill.name,
                            level=level_change_amt,
                            roll=0,
                            comment="Increased skill level")

                    skill.level = new_level
                self.defaults.alerts.append('Record Updated')
        if 'unspent_left' in self.defaults.form:
            info.unspentPoints = int(self.defaults.form['unspent_left'].value)

    def processForm(self):
        js_out = ""
        title = "Spend Points"

        if self.defaults.id is not None:
            info = model.charInfo.get(self.defaults.id)
        else:
            info = None

        # See if we need to do anything with the record
        #
        if 'cmd_update' in self.defaults.form and info is not None:
            # if the user is restricted then their username must match
            # the player name to be able to update the character
            #
            if auth.canUpdate(self.defaults.session_id, self.defaults.id):
                self.update(info)
            else:
                self.defaults.alerts.append("You are not allowed to update this character!")

        # start the search list dict here so I can add more below
        #
        self.myvars = {'char_menu': auth.charMenu(self.defaults.session_id),
                       'title': title,
                       'info': info,
                       }

        # prepare the javascript arrays with skill levels/points
        #
        if info is not None:
            skill_list = model.charSkills.select(
                db.AND(model.charSkills.q.charID == info.id,
                       model.charSkills.q.skillID == model.skill.q.id),
                orderBy=model.skill.q.name)

            # save for the template
            self.myvars['skill_list'] = skill_list

            # create the javascript variables for the header
            #
            js_out = 'var costs = new Array('
            first = True
            for skill in skill_list:
                if first:
                    first = False
                else:
                    js_out += ', '
                js_out += 'Array('
                second = True
                for j in range(5):
                    if second:
                        second = False
                    else:
                        js_out += ", "
                    js_out += '"%s"' % skill.pointCost(j)
                js_out += ")"
            js_out += ")"

            # and add the totals as js variables too
            #
            points = info.cost()
            js_out += eol + "var total = %s;%s" % (points.total(), eol)
            js_out += "var total_skills = %s;%s" % (points.totalSkills, eol)
            js_out += "var unspent = %s;%s" % (info.unspentPoints, eol)

        self.defaults.no_add = True
        self.defaults.js = js_out
        self.defaults.jsfile = "char.js"
        self.defaults.action = my_name


if __name__ == "__main__":
    init_db(load_db_config())
    PointsPage(SpendPoints, save_id=True).main()
