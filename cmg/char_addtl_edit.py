#!/usr/bin/env python3
"""This page is to finish up the character

Primarily the user assigns the parry and block skills
"""

from page import DefaultPage
from templates.CharAddtl import CharAddtl

import auth
import model
from model.connect import (load_db_config, init_db)
import sqlobject as db

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__


class AddtlPage(DefaultPage):

    def update(self, info):
        """Update the records in the database."""
        if 'sel_parry' in self.defaults.form:
            try:
                skill = model.skill.byName(
                    self.defaults.form['sel_parry'].value)
                char_skill = model.charSkills.select(db.AND(
                    model.charSkills.q.skillID == skill.id,
                    model.charSkills.q.charID == info.id))[0]
                info.parryID = char_skill.id
            except db.SQLObjectNotFound:
                info.parry = None

        if 'sel_block' in self.defaults.form:
            try:
                skill = model.skill.byName(
                    self.defaults.form['sel_block'].value)
                char_skill = model.charSkills.select(db.AND(
                    model.charSkills.q.skillID == skill.id,
                    model.charSkills.q.charID == info.id))[0]
                info.blockID = char_skill.id
            except db.SQLObjectNotFound:
                info.block = None

        if 'txt_unspent' in self.defaults.form:
            info.unspentPoints = int(self.defaults.form['txt_unspent'].value)

        if 'txt_health' in self.defaults.form:
            info.health = int(self.defaults.form['txt_health'].value)

        if 'txt_fatigue' in self.defaults.form:
            info.fatigue = int(self.defaults.form['txt_fatigue'].value)

        if 'sel_genre' in self.defaults.form:
            c = model.category.byDescription(
                self.defaults.form['sel_genre'].value)
            info.genre = c.id

        self.defaults.alerts.append("Updated Record")

    def processForm(self):
        """Edit the characters' additional information."""
        self.defaults.no_add = True
        self.defaults.action = my_name
        self.defaults.title = 'Additional Info'

        self.myvars['char_menu'] = auth.charMenu(self.defaults.session_id)

        addtl_list = {}

        if self.defaults.id is not None:
            info = model.charInfo.get(self.defaults.id)
            self.myvars['info'] = info
            
            trait_list = info.traitList()
            if 'HT' in trait_list:
                self.myvars['my_health'] = trait_list['HT']['level']

            # See if we need to do anything with the record
            #
            if 'cmd_update' in self.defaults.form:
                self.update(info)
        else:
            self.myvars['info'] = None

        # Get the default values
        #
        self.myvars['parry_skills'] = model.charSkills.select(
            db.AND(model.skill.q.categoryID=='cbt',
                   model.charSkills.q.charID==self.defaults.id,
                   model.charSkills.q.skillID==model.skill.q.id))

        self.myvars['genres'] = model.category.select(
            model.category.q.validTable=='char_info')


if __name__ == "__main__":
    init_db(load_db_config())
    AddtlPage(CharAddtl, save_id=True).main()

