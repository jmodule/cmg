#!/usr/bin/env python3

"""The GM control sheet that displays character summaries.

"""

import auth
import model
from model.connect import (load_db_config, init_db)
import rules

from page import DefaultPage
from templates.CharControlSheet import CharControlSheet

import sqlobject as db

try:
    my_name = __file__[__file__.rindex("/") + 1:]
except IndexError:
    my_name = __file__


class MainPage(DefaultPage):
    def processForm(self):
        status = None
        if 'ck_char_type' in self.defaults.form:
            char_type = self.defaults.form['ck_char_type'].value
        else:
            char_type = 'a'

        if char_type == 'p':
            char_list = model.charInfo.select(model.charInfo.q.status == True)
        elif char_type == 'n':
            char_list = model.charInfo.select(model.charInfo.q.status == False)
        else:
            char_list = model.charInfo.select()

        self.defaults.no_add = True
        self.defaults.action = my_name
        self.defaults.title = "GM Control Sheet"
        self.myvars = {
            'char_menu': auth.charMenu(self.defaults.session_id),
            'player_characters': char_list,
            'char_type': char_type,
            'health_bp': {"reeling": 3, "collapse": 1},
            'fatigue_bp': {"tired": 5, "collapse": 0, "unconscious": -10},
        }


if __name__ == "__main__":
    init_db(load_db_config())
    MainPage(CharControlSheet, save_id=True).main()
