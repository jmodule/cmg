#!/usr/bin/env python3

import auth
import const
from jmodule.web import html
import model
from model.connect import (load_db_config, init_db)
from page import DefaultPage
from templates.CharEdit import CharEdit

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__


class QuirkPage(DefaultPage):

    def update(self, **kwargs):
        """Update the records already in the database.

        :param **kwargs: table name is ignored
        """

        if 'quirk_id' in self.defaults.form:
            if isinstance(self.defaults.form['quirk_id'], list):
                for (i, qid) in enumerate(self.defaults.form['quirk_id']):
                    quirk = model.charQuirks.get(int(qid.value))
                    quirk.description = self.defaults.form['desc'][i].value
            else:
                quirk = model.charQuirks.get(self.defaults.form['quirk_id'].value)
                quirk.description = self.defaults.form['desc'].value
            self.defaults.alerts.append('Updated quirks')

        if 'new_desc' in self.defaults.form:
            model.charQuirks(
                charId=int(self.defaults.form['id'].value),
                description=self.defaults.form['new_desc'].value)
            self.defaults.alerts.append('Added a quirk')

    def delete(self):
        """Delete a quirk record from the database."""
        if 'id' in self.defaults.form:
            which = self.defaults.form['cmd_del'].value[13:].strip()
            model.charQuirks.delete(which)
            return True
        else:
            return False

    def processForm(self):
        """Edit the character quirks."""
        self.defaults.action = my_name
        self.defaults.no_add = True
        self.defaults.title = 'Edit Quirks'

        # See if we need to do anything with the record
        #
        if 'cmd_update' in self.defaults.form:
            if self.update():
                self.defaults.alerts.append('Record Updated')

        # check for a delete request
        #
        if 'cmd_del' in self.defaults.form:
            if self.delete():
                self.defaults.alerts.append('Record Deleted')

        html_out = ""
        if self.defaults.id is not None:
            info = model.charInfo.get(self.defaults.id)
            html_out += '<form name="standard" action="' + my_name + '" method="post">'
            for q in info.myQuirks:
                html_out += html.hidden('quirk_id', q.id)
                html_out += '<p><input type="text" name="desc" value="%s" size="60"> <input type="submit" name="cmd_del" value="Delete Quirk %s"></p>' % (q.description, q.id)

            html_out += html.hidden('id', self.defaults.id)

            html_out += '<input type="text" name="new_desc" value="" size="60">'
            html_out += html.hidden('session_id', self.defaults.session_id)
            html_out += '<p><input type="submit" name="cmd_update" value="Update"><p>'
            html_out += '</form>'
        else:
            info = ""

        self.myvars = {'bodycontent': html_out,
                       'char_menu': auth.charMenu(self.defaults.session_id),
                       'info': info,
                       }


if __name__ == "__main__":
    init_db(load_db_config())
    QuirkPage(CharEdit, save_id=True).main()
