#!/usr/bin/env python3
"""Return an XML record string with the new set of skills for an opponent.

"""
import model
from model.connect import (load_db_config, init_db)
import rules
from update import XmlResponse
from os import linesep as eol


class ContestResp(XmlResponse):
    def processForm(self):
        self.response += "<skill_list>" + eol
        if 'id' in self.defaults.form:
            info = model.charInfo.get(self.defaults.form['id'].value)
            if info is not None:
                for a in rules.attributes:
                    self.response += "<skill id=\"a,%s\">%s</skill>%s" % (a, a, eol)
                for a in list(rules.calculated_attributes.keys()):
                    self.response += "<skill id=\"c,%s\">%s</skill>%s" % (a, a, eol)
                for s in info.mySkills:
                    self.response += "<skill id=\"s,%s\">%s</skill>%s" % (s.id, s.skill.name, eol)
        else:
            self.response += "<skill>None</skill>" + eol
        self.response += "</skill_list>"


if __name__ == "__main__":
    init_db(load_db_config())
    ContestResp().main()
