from .const import *
from jmodule.web import login

class menuItem:
    """Holds the information need for one menu item link"""
    #----------------------------------------------------------------------
    def __init__(self, name, link, perms, help, submenu=None):
        """Creates a menu entry.

        If a submenu is supplied, then this will be used as a tab.

        """
        self.name = name
        self.link = link
        self.active = False
        self.permissions = perms
        self.help = help
        self.items = submenu
        self.active = False

class mainMenu:
    """Holds the main menu with items for the tabs and side bar"""
    #----------------------------------------------------------------------
    def __init__(self, menu_items):
        """Takes a list of menu items to create the menu tree.

        The 'top most' items are used for the top tab menu (I haven't
        experimented with anything other than 3 tabs, but I suppose
        it's possible) and each tab has a list of items for the side
        bar menu.
        
        """
        self._menu = menu_items
        
    #----------------------------------------------------------------------
    def tabs(self, session_id, action, which=0):
        """Return a list of menu items for the tabs

        I'm creating a new menuItem object because the link must be
        generated dynamically.

        """
        tabs = []
        for line, item in enumerate(self._menu):
            tabs.append(menuItem(item.name,
                                 item.link % (action, session_id, line),
                                 item.permissions,
                                 item.help))
        try:
            which = int(which)
        except ValueError:
            which = 0
            
        if which >= 0 and which < len(tabs):
            tabs[which].active = True

        return tabs

    #----------------------------------------------------------------------
    def sidebar(self, session_id, which):
        """Returns a list of active menu objects for the side menu"""
        access = login.getAccess(session_id)
        new_menu = []
        try:
            which = int(which)
        except ValueError:
            which = 0
        if which >= 0 and which < len(self._menu):
            for line in self._menu[which].items:
                if access in line.permissions:
                    new_menu.append(line)
        return new_menu

    #----------------------------------------------------------------------
    def help(self, session_id, which):
        """Return a string with HTML describing the selected menu items."""
        access = login.getAccess(session_id)
        html = "<div class=\"help\">"
        if which >= 0 and which < len(self._menu):
            html += "<p>%s</p>\n" % (self._menu[which].help,)
            html += "<dl>\n"
            for i in self._menu[which].items:
                if access in i.permissions:
                    html += "<dt>%s</dt><dd>%s</dd>\n" % (i.name, i.help)
        else:
            html = "<p><b>ERROR: invalid menu: %s</b></p>\n" % (which,)
        html += "</div>"

        return html
