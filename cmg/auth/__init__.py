"""This module holds authentication routines that relate to security

Also the menus are here since they depend on authentication to
determine what to display.

"""

__all__ = ['menu', 'const']

import model
from . import menu
from jmodule.web import login
from .const import *

#--------------------------------------------------------------------------
def charMenu(session_id):
    """Return a list of charInfo records for the character menu"""

    role = login.getAccess(session_id)

    if role == 'A': 
        c = model.charInfo.select(orderBy=model.charInfo.q.charName)
    else:
        c = model.charInfo.select(model.charInfo.q.status == True,
                            orderBy=model.charInfo.q.charName)
    return c

#--------------------------------------------------------------------------
def canUpdate(session_id, char_id):
    """Return true if the user is allowed to update the character's record

    The player name must match the login name.  So for example use
    Jakim instead of Jakim Friant as the player to make it easy to
    match up.  It's not case sensitive.

    """
    name = model.charInfo.get(char_id).userName()
    if login.getAccess(session_id) == 'R':
        if login.getUsername(session_id).lower() == name.lower():
            okay = True
        else:
            okay = False
    elif login.getAccess(session_id) == 'A':
        okay = True
    else:
        okay = False

    return okay

#--------------------------------------------------------------------------
def getId(session_id):
    """return the current record id or None

    getId(session_id) -> [record_id|None]

    The also handles the case when a Restricted user logs in so they
    will be presented with their character first (if it can be found).

    """

    rec_id = login.getId(session_id)

    if rec_id is None:
        if login.getAccess(session_id) == 'R':
            username = login.getUsername(session_id).capitalize()
            try:
                rec_id = model.charInfo.select(
                    model.charInfo.q.username == username)[0].id
            except IndexError:
                pass
    
    return rec_id
