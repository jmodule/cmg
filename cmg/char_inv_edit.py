#!/usr/bin/env python3

import auth
import model
from model.connect import (load_db_config, init_db)
from page import DefaultPage
from templates.CharInvEdit import CharInvEdit

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__

_m_add = '<b>%d item(s) added to the inventory</b>'
_m_del = '<b>%d item(s) removed from the inventory</b>'
_m_access_err = "<b>You are not allowed to modify this character</b>"
_m_sel_err = "<b>You must select a character first</b>"

_title = "Edit Inventory"


class EditPage(DefaultPage):
    def processForm(self):
        """Edit the characters' inventory."""
        self.defaults.no_add = True
        self.defaults.action = my_name
        self.defaults.title = _title
        
        access_error = False
        select_error = False
        form = self.defaults.form
        
        # load the necessary lists and set the current items if
        # they're in the form
        #
        if 'sel_category' in form:
            curr_categ = form['sel_category'].value
            sel_category = model.category.byDescription(curr_categ).id
        else:
            curr_categ = ""
            sel_category = 'amm'

        if 'sel_equipment' in form:
            curr_eqpt =  form['sel_equipment'].value
        else:
            curr_eqpt = ""

        if 'txt_qty' in form:
            try:
                qty = int(form['txt_qty'].value)
            except TypeError:
                qty = 0
        else:
            qty = 0

        if 'sel_inventory' in form:
            curr_inv = form['sel_inventory'].value
        else:
            curr_inv = ""

        if 'txt_notes' in form:
            txt_notes = form['txt_notes'].value
        else:
            txt_notes = ""

        if 'chk_in_use' in form:
            in_use = form['chk_in_use']
        else:
            in_use = None

        # See if we need to do anything with the record
        #
        record_updated = 0
        if 'cmd_add' in form and curr_eqpt:
            if self.defaults.id is not None:
                if auth.canUpdate(self.defaults.session_id, self.defaults.id):
                    model.inventory(
                        char = model.charInfo.get(self.defaults.id),
                        eqpt = model.equipment.byDescription(curr_eqpt),
                        qty = qty,
                        inUse = in_use,
                        notes = txt_notes)
                    record_updated = qty
                else:
                    access_error = True
            else:
                select_error = True

        # check for changed records
        #
        if 'cmd_update' in form:
            if auth.canUpdate(self.defaults.session_id, self.defaults.id):
                inv_id = int(form['item_id'].value)
                qty = int(form['item_qty'].value)
                if 'item_notes' in form:
                    notes = form['item_notes'].value
                else:
                    notes = ""
                if 'item_in_use' in form:
                    in_use = 1
                else:
                    in_use = 0
                inv = model.inventory.get(inv_id)
                if inv:
                    inv.set(qty = qty,
                            inUse = in_use,
                            notes = notes)
            else:
                access_error = True

        # check for a delete request
        #
        record_deleted = 0
        if 'cmd_del' in form:
            if self.defaults.id is not None:
                if auth.canUpdate(self.defaults.session_id, self.defaults.id):
                    try:
                        inv_id = int(form['item_id'].value)
                        model.inventory.delete(inv_id)
                        record_deleted = int(form['item_qty'].value)
                    except:
                        access_error = True
                else:
                    access_error = True
            else:
                select_error = True

        if self.defaults.id is not None:
            info = model.charInfo.get(self.defaults.id)
            inventory = info.myInv
        else:
            info = None
            inventory = []

        # create status messages
        #
        if record_updated > 0:
            self.defaults.alerts.append(_m_add % record_updated)
        if record_deleted > 0:
            self.defaults.alerts.append(_m_del % record_deleted)
        if access_error: self.defaults.alerts.append(_m_access_err)
        if select_error: self.defaults.alerts.append(_m_sel_err)

        cat_list = model.category.select(
            model.category.q.validTable == 'equipment',
            orderBy=model.category.q.description)

        eqpt_list = model.equipment.select(
            model.equipment.q.categoryID == sel_category,
            orderBy=model.equipment.q.description)

        self.myvars = { 'char_menu': auth.charMenu(self.defaults.session_id),
                        'show_delete': False,
                        'category': cat_list,
                        'equipment': eqpt_list,
                        'curr_categ': curr_categ,
                        'inventory' : inventory,
                        'info': info,
                        }


if __name__ == "__main__":
    init_db(load_db_config())
    EditPage(CharInvEdit, save_id=True).main()
