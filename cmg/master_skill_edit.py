#!/usr/bin/env python3

import auth
from db import form
from jmodule.web import html
import model
from model.connect import (load_db_config, init_db)
from page import DefaultPage
from templates.TableEdit import TableEdit

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__

disp_field = ('categoryID', 'name')


class EditPage(DefaultPage):
    def processForm(self):
        """Edit the skill master list."""
        content = ""
        
        if 'cmd_update' in self.defaults.form:
            self.update(model.skill)
            self.defaults.alerts.append('Record Updated')

        content = form.fill(model.skill,
                            id=self.defaults.id,
                            )
        content += html.hidden('session_id', self.defaults.session_id)
        
        self.myvars = {
            'form': content,
            'title': 'Master Skill List',
            'record_select': form.select(model.skill, disp_field),
            }
        self.defaults.action = my_name


if __name__ == "__main__":
    init_db(load_db_config())
    EditPage(TableEdit).main()
