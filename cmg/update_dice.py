#!/usr/bin/env python3
"""Return an XML record string with dice roll result

The form/query has one parameter that holds the dice code which is
parsed by the Dice object and a 'roll' result is returned as a string.

"""

import dice
from update import XmlResponse


class DiceResp(XmlResponse):
    def processForm(self):
        if 'die_code' in self.defaults.form:
            d = dice.Dice(self.defaults.form['die_code'].value)
            d.roll()
            
            self.response += "<roll>%s</roll>" % str(d)
        else:
            self.response += "<roll>Nothing to roll</roll>"


if __name__ == "__main__":
    DiceResp().main()
    
