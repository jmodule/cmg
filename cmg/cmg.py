#!/usr/bin/env python3

"""cmg.py is the initial destination, but that's it.

It displays a character summary for now.

"""
from page import DefaultPage

import auth
import model
from model.connect import (load_db_config, init_db)
import rules

from templates.CharSummary import CharSummary

import sqlobject as db

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__

_fmt = "%s-%d [%d]"


class MainPage(DefaultPage):
    def processForm(self):
        if self.defaults.id is not None:
            info = model.charInfo.get(self.defaults.id)
            skill_list = model.charSkills.select(
                db.AND(model.charSkills.q.skillID == model.skill.q.id,
                       model.charSkills.q.charID == info.id),
                orderBy=model.skill.q.name)
            try:
                eqpt = [e.eqpt.description for e in info.getInventory()]
            except db.SQLObjectNotFound:
                eqpt = None
            quirks = [q.description for q in info.myQuirks]
            traits = { 'atr': [_fmt % (t.fullName(), t.level, t.cost()) for t in model.charTraits.select(db.AND(model.charTraits.q.charID == info.id, model.charTraits.q.traitID == model.trait.q.id, db.IN(model.trait.q.name, rules.attributes)))],
                       'adv' : [_fmt % (t.fullName(), t.level, t.cost()) for t in info.advantages()],
                       'dis' : [_fmt % (t.fullName(), t.level, t.cost()) for t in info.disadvantages()]}
            skills = [_fmt % (s.fullName(), s.gameLevel(), s.pointCost()) for s in skill_list]
        else:
            info = None
            skill_list = None
            quirks = None
            traits = None
            skills = None
            eqpt = None

        self.defaults.no_add = True
        self.defaults.action = my_name
        self.defaults.title = "Character Summary"
        self.myvars = {
            'info': info,
            'char_menu': auth.charMenu(self.defaults.session_id),
            'quirks': quirks,
            'traits': traits,
            'skills': skills,
            'eqpt': eqpt,
            }


if __name__ == "__main__":
    db_uri = load_db_config()
    init_db(db_uri)

    MainPage(CharSummary, save_id=True).main()
