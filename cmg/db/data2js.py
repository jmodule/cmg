"""Generate a javascript header sections with lists from the database"""

import sys
sys.path.append('.')
import model


def traitLists(trait_list):
    """Return lists for checking traits"""
    name_line = "trait_names = new Array(\"Choose a new trait...\""
    base_line = "trait_base_cost = new Array(\"\""
    incr_line = "trait_incr_cost = new Array(\"\""
    max_line = "trait_max = new Array(\"\""

    for rec in trait_list:
        name_line += ", \"%s\"" % rec.name
        base_line += ", \"%s\"" % rec.baseCost
        incr_line += ", \"%s\"" % rec.incrCost
        max_line += ", \"%s\"" % rec.maxLevel

    name_line += ");"
    base_line += ");"
    incr_line += ");"
    max_line += ");"

    script_out = "// trait information\n%s\n%s\n%s\n%s\n" % (name_line,
                                                             base_line,
                                                             incr_line,
                                                             max_line)
    return script_out


if __name__ == '__main__':
    print(traitLists())
