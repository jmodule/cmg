"""This has helper functions for the equipment table"""

_desc = None
_ids = None


def getRecord(db_link, id):
    """Return a dictionary of the record or an empty dict on an error"""
    rec = dict()
    sql = "select * from equipment where id = '%s'" % id
    result = db_link.query(sql)
    if len(result) > 0:
        rec = result[0]

    return rec


def load(db_link, categ):
    """Load the IDs and descriptions for items in the category"""
    global _desc
    global _ids

    _desc = dict()
    _ids = dict()

    sql = "select id, description from equipment"
    sql += " where category_id = '%s'" % categ

    result = db_link.query(sql)
    if result:
        for line in result:
            _desc[line['description']] = line['id']
            _ids[line['id']] = line['description']


def get_desc():
    """Return the dictionary keyed by description"""
    if _desc != None:
        return _desc
    else:
        return dict()


def get_id(desc):
    """Return the id for the given description"""
    id_out = ""
    if _desc != None:
        if desc in _desc:
            id_out = _desc[desc]
    return id_out


def addInventory(db_link, char_id, eqpt_id, qty, in_use_flag, **kwd):
    """Update the inventory table and return the quantity added or removed

    Any problem in the sql means the number changed stays at 0 and
    indicates an error.
    """
    number_changed = 0
    curr_qty = 0
    if 'notes' in kwd:
        notes = kwd['notes']
    else:
        notes = ""

    if 'remove' in kwd:
        remove = kwd['remove']
    else:
        remove = False
    
    id_filter = "WHERE char_id='%s' AND eqpt_id='%s'" % (char_id, eqpt_id)
    
    sql = "SELECT qty FROM inventory %s" % id_filter
    result = db_link.query(sql)
    if result:
        curr_qty = result[0]['qty']

    if remove:
        new_qty = curr_qty - int(qty)
    else:
        new_qty = curr_qty + int(qty)

    if in_use_flag is not None:
        in_use = 1
    else:
        in_use = 0

    if curr_qty == 0 and new_qty > 0:
        sql = "INSERT INTO inventory (char_id, eqpt_id, qty, notes, in_use)"
        sql += " VALUES ('%s', '%s', '%s', '%s', '%s')" % (char_id,
                                                           eqpt_id,
                                                           new_qty,
                                                           notes,
                                                           in_use)
    elif curr_qty > 0 and new_qty <= 0:
        sql = "DELETE FROM inventory %s" % id_filter
    else:
        sql = "UPDATE inventory SET qty = '%s', notes = '%s', in_use = '%s' %s" % (new_qty, notes, in_use, id_filter)

    db_link.query(sql)
    number_changed = qty
    return number_changed


def updateInventory(db_link, char_id, eqpt_id, qty, notes, in_use):
    """Update the inventory table, return true on success

    the db_link should throw an exception if something goes wrong with
    the sql

    """
    sql = "UPDATE inventory SET qty = '%s', notes = '%s'" % (qty, notes)
    sql += ", in_use = '%s'" % in_use
    sql += " WHERE char_id='%s' AND eqpt_id='%s'" % (char_id, eqpt_id)

    try:
        db_link.query(sql)
        return True
    except Exception:
        return False



