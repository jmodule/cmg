#!/usr/bin/env python
# $Id: char.py,v 1.40 2005-12-21 20:00:44 jfriant Exp $

import types
from jmodule.utils import debug
from jmodule.web import html, login

from const import armor_attributes, armor_locations

my_name = __file__[__file__.rindex("/")+1:__file__.rindex('.')]


class cInv:

    def __init__(self, id = None, qty = 0, notes = "", in_use = 0):
        self.id = id
        self.qty = qty
        self.notes = notes
        self.in_use = in_use


def delete(db_link, table_name, key_name, form):
    """Delete a record from any table in the database.

    Note that the delete button _must_ be set up as follows:

    <input type=\"submit\" name=\"cmd_del\" value=\"Delete Record 1\">

    or

    <input type=\"submit\" name=\"cmd_del\" value=\"Delete Record LUCK\">
    
    The important part being the record id at the end of the value
    string, since that is what this function will be looking for to
    identify the record.

    """
    if 'id' in form:
        if debug.AtLeast('Notice'):
            html.begin()
        debug.report('Notice', my_name+'.main', 'form contents', form)

        del_name = form['cmd_del'].value
        try:
            which = del_name[del_name.rindex(' '):].strip()
            sql = "delete from " + str(table_name)
            sql += " where char_id = '" + str(form['id'].value) + "'"
            sql += " and " + key_name + " = '" + str(which) + "'"
            debug.report('Notice', my_name+'.delete', 'sql', sql)
            db_link.query(sql)
            return True
        except:
            return False
    else:
        return False


def getSkills(db_link, char_id = None):
    """Build a dict with a list of all the skills.

    The keys and values are reverse so they can be used with web.html.list().

    The char_id parameter is there so we can limit the skills returned to
    what genre the character is a part of.
    """
    skills_out = dict()

    if char_id is not None:
        sql = "select genre from char_info where id = '%s'" % char_id
        result = db_link.query(sql)
        genre = result[0]['genre']
        if genre == 'a':
            genre = None
    else:
        genre = None

    sql = "select id, name from skills"
    sql += " where invalid != 1"
    if genre is not None:
        sql += " and genre = '%s' or genre = 'g'" % genre
    sql += " order by name"
    result = db_link.query(sql)

    for rec in result:
        skills_out[rec['name']] = rec['id']

    return skills_out


def getCharSkills(db_link, char_id, **kwd):
    """Build a dictionary of the character skills.

    Keywords:
      category -- a tuple of categories to limit the selection

    """
    skill_list = dict()

    sql = "select name, level, category_id, parry, detail, skill_id"
    sql += " from char_skills, skills"
    sql += " where char_id = '" + str(char_id) + "'"
    if 'category' in kwd:
        if not isinstance(kwd['category'], tuple):
            raise TypeError("Keyword 'category' must be a tuple")
        sql += " and ("
        end = len(kwd['category']) - 1
        for i in range(end):
            sql += "category_id = '%s' or " % kwd['category'][i]
        sql += "category_id = '%s')" % kwd['category'][end]
    sql += " and skill_id = skills.id"
    result = db_link.query(sql)
    for rec in result:
        if 'id_only' in kwd:
            skill_list[rec['name']] = rec['skill_id']
        else:
            skill_list[rec['name']] = { 'level': rec['level'],
                                        'id': rec['skill_id'],
                                        'category': rec['category_id'],
                                        'parry': rec['parry'],
                                        'detail': rec['detail']}
    return skill_list


def getTraits(db_link):
    """Build a dict with a list of all the traits.

    The keys and values are reverse so they can be used with web.html.select().
    """
    traits_out = dict()

    sql = "select code, name from traits"
    sql += " where invalid != 1"
    sql += " order by category_id, name"
    result = db_link.query(sql)
    debug.report('Notice', my_name + 'getTraits', result)
    for rec in result:
        traits_out[rec['name']] = rec['code']

    return traits_out


def getCharTraits(db_link, char_id):
    """Build a dictionary of the character's traits."""
    trait_list = dict()

    sql = "select name, level, category_id from char_traits, traits"
    sql += " where char_id = '" + str(char_id) + "'"
    sql += " and trait_code = traits.code"
    #debug.report('Debug', my_name+'.getCharTraits', 'query', sql)
    result = db_link.query(sql)
    for rec in result:
        trait_list[rec['name']] = {'level': rec['level'],
                                   'category': rec['category_id']}

    #debug.report('Debug', my_name+'.getCharTraits', 'trait_list', trait_list)
    return trait_list


def getInfo(db_link, char_id):
    """Return a dictionary of the character's basic information."""
    sql = "select * from char_info"
    sql += " where id = '" + str(char_id) + "'"
    result = db_link.query(sql)
    debug.report('Notice', my_name + 'getInfo', 'Query result', result)
    return result[0]


def getCharQuirks(db_link, char_id):
    """Return a list of the characters quirks"""
    quirks = []
    sql = "select description from char_quirks"
    sql += " where char_id = '%s'" % (char_id,)
    result = db_link.query(sql)
    for rec in result:
        quirks.append(rec['description'])
    return quirks


def getInventory(db_link, char_id, show_notes=False):
    """Return a dict of the characters inventory items and their quantity"""

    inv = dict()
    sql = "SELECT description, qty, eqpt_id"
    if show_notes:
        sql += ", inventory.notes"
    sql += " FROM equipment, inventory"
    sql += " WHERE char_id = '%s' AND eqpt_id = equipment.id" % char_id
    result = db_link.query(sql)
    for rec in result:
        desc = "%s (%s)" % (rec['description'], rec['qty'])
        if show_notes and rec['notes'] != "":
            desc += " - %s" % rec['notes']
        inv[desc] = rec['eqpt_id']
            
    return inv


def getArmor(db_link, char_id, location=None):
    """Return a dict of armor locations with their PD and DR values"""
    armor = dict()
    for attr in armor_attributes:
        armor[attr] = dict()
        for loc in armor_locations:
            armor[attr][loc] = 0
    sql = "SELECT description, head_pd, head_dr, torso_pd, torso_dr,"
    sql += " arm_pd, arm_dr, leg_pd, leg_dr"
    sql += " FROM equipment, inventory"
    sql += " WHERE char_id = '%s'" % char_id
    sql += " AND eqpt_id = equipment.id"
    sql += " AND in_use = 1"
    sql += " AND category_id = 'arm'"
    result = db_link.query(sql)
    for rec in result:
        for attr in armor_attributes:
            for loc in armor_locations:
                rec_key = "%s_%s" % (loc, attr)
                armor[attr][loc] += rec[rec_key]
    return armor


def getInvAsClass(db_link, char_id):
    """Return a inventory records as a class"""

    inv = dict()
    
    sql = "SELECT description, qty, eqpt_id, inventory.notes, in_use"
    sql += " FROM equipment, inventory"
    sql += " WHERE char_id = '%s' AND eqpt_id = equipment.id" % char_id
    result = db_link.query(sql)
    for rec in result:
        try:
            quantity = int(rec['qty'])
        except ValueError:
            quantity = 0
        inv[rec['description']] = cInv(qty=quantity,
                                       id=rec['eqpt_id'],
                                       notes=rec['notes'],
                                       in_use=rec['in_use'])
    return inv


def getWeapons(db_link, char_id, trait_list):
    """Return a list of weapon strings with damage codes"""
    wpn_list = []
    
    sql = "SELECT description, pri_dmg, pri_dmg_base, pri_dmg_type,"
    sql += " pri_dmg_max, sec_dmg, sec_dmg_base, sec_dmg_type, sec_dmg_max"
    sql += " FROM equipment, inventory"
    sql += " WHERE char_id = '%s'" % char_id
    sql += " AND equipment.id = eqpt_id"
    sql += " AND (category_id = 'hnd' OR category_id = 'rng')"

    result = db_link.query(sql)
    for rec in result:
        wpn_list.append({'desc': rec['description'],
                         'dmg': rec['pri_dmg'],
                         'dmg_base': rec['pri_dmg_base'],
                         'dmg_type': rec['pri_dmg_type'],
                         })
    return wpn_list


def getAddtlInfo(db_link, char_id):
    """Return a dictionary with the additional character info"""

    sql = "SELECT id, unspent_points, health, fatigue, genre from char_info"
    sql += " WHERE id = '%s'" % char_id

    result = db_link.query(sql)
    if result:
        addtl = result[0]
        sql = "SELECT skills.name"
        sql += " FROM char_info, skills"
        sql += " WHERE char_info.id = '%s' AND parry_id = skills.id" % char_id

        result = db_link.query(sql)
        if result:
            addtl['parry_name'] = result[0]['name']
        else:
            addtl['parry_name'] = 0
    else:
        addtl = dict(unspent_points=0,
                     health=0,
                     parry_name=0,
                     genre=0)
    return addtl


def canUpdate(db_link, session_id, char_id):
    """See if the user is allowed to update the character's record

    The player name must match the login name.  So for example use
    Jakim instead of Jakim Friant as the player to make it easy to
    match up.  It's not case sensitive.

    """
    info = getInfo(db_link, char_id)
    if login.getAccess(session_id) == 'R':
        if login.getUsername(session_id).lower() == info['player_name'].lower():
            okay = True
        else:
            okay = False
    elif login.getAccess(session_id) == 'A':
        okay = True
    else:
        okay = False

    return okay


