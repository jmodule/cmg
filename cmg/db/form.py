from jmodule.db.table import fieldEntry, MEDIUM_TEXT_LEN
from jmodule.web import html
from os import linesep as eol
import sqlobject


class cRecord:
    """Holds the id and description for a database record"""
    def __init__(self, id, desc = "", delete = False):
        self.id = id
        self.description = desc
        self.show_delete = delete


def fill(sqldef, *arg, **kwarg):
    """Returns a HTML form field based on a sqlobject column definition"""
    html_out = ""

    if 'hide' in kwarg:
        fields_to_hide = kwarg['hide']
    else:
        fields_to_hide = []

    rec = None
    if 'id' in kwarg:
        if kwarg['id'] is not None:
            rec = sqldef.get(kwarg['id'])
            if 'show_id' not in kwarg:
                html_out += html.hidden('id', kwarg['id']) + eol

    html_out += '<table border="1" cellpadding="1" cellspacing="1">' + eol

    if 'show_id' in kwarg:
        if rec is not None:
            value = getattr(rec, 'id')
        else:
            value = ""
        html_out += fieldEntry('id', 11, value)

    field_list = list(sqldef.sqlmeta.columns.items())
    field_list.sort(key=lambda x: x[1].creationOrder)

    for (field_name, field_def) in field_list:
        if field_name not in fields_to_hide:
            length = getLength(field_def)
            if rec is not None:
                value = getattr(rec, field_name)
                if isinstance(value, str):
                    value = value.replace('"', "\\\"")
                    value = value.replace("&", "&amp;")
            else:
                value = ''
            html_out += fieldEntry(field_name, length, value)

    html_out += "</table>" + eol
    html_out += '<input type="submit" name="cmd_update" value="Enter Information">' + eol
    return html_out


def getLength(field_def):
    length = getattr(field_def, 'length', 11)
    if length is None:
        if isinstance(field_def.columnDef,
                      sqlobject.StringCol):
            length = MEDIUM_TEXT_LEN
        elif isinstance(field_def.columnDef,
                        sqlobject.IntCol):
            length = 11
        elif isinstance(field_def.columnDef,
                        sqlobject.BoolCol):
            length = 1
    return length


def select(table, fields, *arg, **kwarg):
    """Generate a list of record ID's and descriptions for picking a record"""

    if 'show_delete' in kwarg:
        show_delete = kwarg['show_delete']
    else:
        show_delete = False
    rec_list = []
    for rec in table.select(orderBy=fields):
        try:
            rec_out = cRecord(rec.id,
                              ", ".join([getattr(rec, name) for name in fields]),
                              show_delete)
            rec_list.append(rec_out)
        except TypeError:
            raise Exception("rec=%s, name=%s, fields=%s" % (rec,
                                                            name,
                                                            fields))
    return rec_list
