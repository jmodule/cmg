"""Functions for the category table"""

_desc_hash = None
_id_hash = None


def load(db_link, table):
    """Return a dictionary of the valid categories for the given table"""
    global _desc_hash
    global _id_hash

    _desc_hash = dict()
    _id_hash = dict()
    
    sql = "select id, description from category"
    sql += " where valid_table = '%s'" % table

    result = db_link.query(sql)
    
    if result:
        for line in result:
            _desc_hash[line['description']] = line['id']
            _id_hash[line['id']] = line['description']


def get():
    """Return the hash of descriptions -> ids"""
    global _desc_hash

    return _desc_hash


def get_desc(id):
    """Return a string of the description for a category"""
    global _id_hash
    
    d = ""
    if id in _id_hash:
        d = _id_hash[id]
    return d


def get_id(desc):
    """return the id as a string"""
    global _desc_hash

    i = ""
    if desc in _desc_hash:
        i = _desc_hash[desc]
    return i
    
