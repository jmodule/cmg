#!/usr/bin/env python3

import auth
from db import form
from jmodule.web import html
import model
from model.connect import (load_db_config, init_db)
from page import DefaultPage
from templates.TableEdit import TableEdit

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__

disp_field = ('categoryID', 'name')


def parseName(name):
    """return a default code based on the trait name"""
    first_space = name.find(' ')
    if first_space == -1:
        code_out = name[:4]
    else:
        code_out = name[:2] + name[first_space:2]
    return code_out.upper()


class EditPage(DefaultPage):
    def processForm(self):
        """Edit the trait list"""
        content = ""
        
        if 'cmd_update' in self.defaults.form:
            self.update(model.trait)
            self.defaults.alerts.append('Record Updated')

        content = form.fill(model.trait, id=self.defaults.id)
        content += html.hidden('session_id', self.defaults.session_id)
        
        records = form.select(model.trait, disp_field)
        
        self.myvars = {
            'form': content,
            'title': 'Master Trait List',
            'record_select': records
            }
        self.defaults.action = my_name


if __name__ == "__main__":
    init_db(load_db_config())
    EditPage(TableEdit).main()
