#!/usr/bin/env python3
"""This script lets you edit the category table"""

import auth
from db import form
from jmodule.web import html
import model
from model.connect import (load_db_config, init_db)
from page import DefaultPage
from templates.TableEdit import TableEdit

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__

disp_field = ('validTable', 'description')


class EditPage(DefaultPage):
    def processForm(self):
        """Edit the category list"""
        content = ""
        
        if 'cmd_update' in self.defaults.form:
            self.update(model.category)
            self.defaults.alerts.append('Record Updated')

        content = form.fill(model.category,
                            id=self.defaults.id,
                            show_id=True)
        content += html.hidden('session_id', self.defaults.session_id)
        
        records = form.select(model.category, disp_field)
        
        self.myvars = {
            'form': content,
            'title': 'Category Edit',
            'record_select': records
            }
        self.defaults.action = my_name


if __name__ == "__main__":
    init_db(load_db_config())
    EditPage(TableEdit).main()
