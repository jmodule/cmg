#!/usr/bin/env python3
"""This script lets you link equipment with skills"""

import auth
from db import form
from jmodule.web import html
import model
from model.connect import (load_db_config, init_db)
from os import linesep as eol
from page import DefaultPage
from templates.TableEdit import TableEdit

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__

disp_field = ('categoryID', 'description')


class EditPage(DefaultPage):
    def processForm(self):
        html_out = ""

        skill = None
        if 'sel_skill' in self.defaults.form:
            try:
                skill = model.skill.select(model.skill.q.name == self.defaults.form['sel_skill'].value)[0]
            except IndexError:
                pass

        modifier = 0
        if 'txt_modifier' in self.defaults.form:
            try:
                modifier = int(self.defaults.form['txt_modifier'].value)
            except ValueError:
                pass

        # see if we need to update the records
        if 'cmd_add' in self.defaults.form and skill is not None and self.defaults.id is not None:
            model.weaponSkills(equipment=model.equipment.get(self.defaults.id),
                               skill=skill,
                               modifier=modifier)
            self.defaults.alerts.append('Added record')

        # see if the user wants to delete a record
        if 'del' in self.defaults.form:
            try:
                del_id = int(self.defaults.form['del'].value)
                model.weaponSkills.delete(del_id)
                self.defaults.alerts.append('Deleted record')
            except ValueError as v:
                self.defaults.alerts.append('Error deleting record [%s]' % v)
        
        if self.defaults.id is not None:
            categ = model.category.byDescription('Combat')
            eq_rec = model.equipment.get(self.defaults.id)
            links = model.weaponSkills.select(
                model.weaponSkills.q.equipmentID == self.defaults.id)
            
            html_out += html.hidden('id', self.defaults.id) + eol
            html_out += '<table border="1" cellpadding="4">' + eol
            html_out += '<tr><th>Equipment</th><th>Skills</th>'
            html_out += '<th>Penalty</th><th></th></tr>' + eol
            html_out += '<tr><td>%s</td>' % eq_rec.description
            html_out += '<td><select name="sel_skill">' + eol
            for rec in model.skill.select(model.skill.q.categoryID == categ.id,
                                          orderBy='name'):
                html_out += "<option>%s</option>%s" % (rec.name, eol)
            html_out += '</select></td>' + eol
            html_out += '<td><input type="text" size="2" value="0" name="txt_modifier"></td>'
            html_out += '<td>'
            html_out += '<input type="submit" name="cmd_add" value="Add">'
            html_out += '</td>'
            html_out += '</tr>' + eol
            for rec in links:
                html_out += '<tr><td></td><td>%s</td>' % (rec.skill.name, )
                html_out += '<td>%d</td>' % rec.modifier
                html_out += '<td><a href="%s?del=%s&id=%s&session_id=%s">' % (my_name, rec.id, self.defaults.id, self.defaults.session_id)
                html_out += 'Delete</a></td></tr>' + eol
            html_out += '</table>' + eol
        else:
            pass

        html_out += html.hidden('session_id', self.defaults.session_id) + eol

        self.myvars = {
            'form': html_out,
            'record_select': form.select(model.equipment, disp_field),
            'notes': "",
            }
        self.defaults.action = my_name
        self.defaults.title = 'Weapon Skill Links'


if __name__ == "__main__":
    init_db(load_db_config())
    EditPage(TableEdit).main()
