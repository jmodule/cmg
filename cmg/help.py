#!/usr/bin/env python3

"""Help on the Character Manager for GURPS Application

This application was written to be an online database for GURPS
characters.  It currently supports creating characters, including
traits (attributes, advantages and disadvantages), skills, quirks and
inventory.  Racial packages are partially supported as traits with a
racial discount.

Two types of character sheets can be displayed, first a standard one
to show everything about a character and that can be linked to from a
web site or printed out and used in tabletop games.  The second is an
XML sheet that was designed to be imported into OpenRPG
&lt;http://www.openrpg.com/&gt;, but has a stylesheet so you can view
it in a browser too.

This application is being actively developed as the author uses it in
his own campaigns.

"""

from page import DefaultPage
from templates.Index import Index


class HelpPage(DefaultPage):

    def processForm(self):
        """Generate a help page"""
        self.myvars['bodycontent'] = "<pre>%s</pre>" % __doc__


if __name__ == "__main__":
    HelpPage(Index).main()
