#!/usr/bin/env python3
"""Takes html POST data with the fatigue used and updates the character record

Intended to be used to respond to an AJAX request (see utils/ajax.js
and utils/log.js).

"""

import model
from model.connect import (load_db_config, init_db)
from update import XmlResponse


class Fatigue(XmlResponse):
    def processForm(self):
        fatigue = 0
        if 'fatigue' in self.defaults.form:
            try:
                fatigue = int(self.defaults.form['fatigue'].value)
            except ValueError:
                pass

        if 'id' in self.defaults.form:
            char_id = int(self.defaults.form['id'].value)

            char = model.charInfo.get(char_id)

            char.fatigue = char.fatigue - fatigue

            self.response = """
            <name>%s</name>
            <fatigue>%s</fatigue>""" % (char.charName, char.fatigue)
        

if __name__ == "__main__":
    init_db(load_db_config())
    Fatigue().main()
