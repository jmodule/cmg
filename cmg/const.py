"""Global constants for the program"""

_major = 2
_minor = 5
_build = 1
# Release should be either "-dev" or ""
_release = "-dev"

version = "%d.%d.%d%s" % (_major, _minor, _build, _release)

author = "Jakim Friant"
email = "jakim@friant.org"
url = "http://friant.org/spigot/"
summary = "Character manager for GURPS"

trove_list = ['Development Status :: 4 - Beta',
              'Environment :: Internet',
              'Intended Audience :: End Users/Desktop',
              'License :: OSI Approved :: GNU General Public License (GPL)',
              'Operating System :: Linux',
              'Programming Language :: Python',
              ]

public = "/cmg/"

css = {'char_sheet': public + 'char_sheet.css',
       'openrpg1': public + 'openrpg1.css',
       'cmg': public + 'cmg.css',
       'development': public + 'development.css'}

footer = "Character Manager for GURPS, version %s, by Jakim Friant" % version

armor_attributes = ("pd", "dr")
armor_locations = ("head", "torso", "arm", "leg")
armor_bonuses = {'DR': 'dr',
                 'Toughness': 'dr'}
armor_penalties = {}

log_id_roll = 'rol'
log_id_decision = 'dec'
log_id_spend_points = 'spp'

primary = 's'
secondary = 't'

anonymous_target = 'Anonymous'
