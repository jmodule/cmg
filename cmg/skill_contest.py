#!/usr/bin/env python3

import auth
import const
from dice import Dice
import model
from model.connect import (load_db_config, init_db)
import rules
import sqlobject

from page import DefaultPage
from templates.SkillRollContest import SkillRollContest

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__

_title = "Skill Rolls"


class CustomSkill:

    def __init__(self, char=None, skill_id=None, name=None, level=None, mod=0):
        self.char = char
        self.id = skill_id
        self.name = name
        self.level = level
        self.mod = mod


class ContestResult:

    def __init__(self, protagonist, antagonist, quick=False):
        """Set up the result records"""
        self.skills = {'pro': protagonist,
                       'ant':  antagonist}
        self.quick = quick
        self.dice = dict()
        self.win = None

    def _roll(self):
        for who in 'pro', 'ant':
            self.dice[who].roll()
            self.dice[who].delta = self.dice[who].level - self.dice[who].result()

    def resolve(self):
        for who in 'pro', 'ant':
            self.dice[who] = Dice('3d6', self.skills[who].level, self.skills[who].mod)
        self._roll()

        if self.quick:
            if self.dice['pro'].delta > self.dice['ant'].delta:
                self.win = 'pro'
            else:
                self.win = 'ant'
        else:
            while self.win is None:
                if self.dice['pro'].success(True) and not self.dice['ant'].success(True):
                    self.win = 'pro'
                elif not self.dice['pro'].success(True) and self.dice['ant'].success(True):
                    self.win = 'ant'
                else:
                    self._roll()

    def __str__(self):
        """Return the result as a string"""
        self.resolve()

        s_out = "%s Triumphed!<br>%s: %s<br>%s: %s<br>[quick=%s]" % (self.skills[self.win].char.charName,
                                                                     self.skills['pro'].name,
                                                                     self.dice['pro'],
                                                                     self.skills['ant'].name,
                                                                     self.dice['ant'],
                                                                     self.quick)

        return s_out


class SkillContestPage(DefaultPage):

    def getSkill(self, skill_id, char):
        """return a skill object for the given ID.

        Takes a skill ID from one of the drop-down boxes and converts
        it to a custom skill record based on how the ID is parsed.

        """
        skill_out = None
        if skill_id != "None":
            (rec_type, rec_id) = skill_id.split(',')
            if rec_type == 's':
                try:
                    skill = model.charSkills.get(rec_id)
                    skill_out = CustomSkill(char, skill.id, skill.fullName(), skill.gameLevel())
                except sqlobject.SQLObjectNotFound:
                    self.defaults.alerts.append("Error: charSkill ID %s not found database!" % (rec_id,))
            elif rec_type == 'a':
                try:
                    attr = model.charTraits.select(
                        sqlobject.AND(model.charTraits.q.traitCode == rec_id,
                                      model.charTraits.q.charID == char.id))
                    skill_out = CustomSkill(char, rec_id, attr[0].fullName(), attr[0].level)

                except sqlobject.SQLObjectNotFound:
                    self.defaults.alerts.append("Error: charTrait code %s not found database!" % (rec_id,))
                except IndexError:
                    self.defaults.alerts.append("No skill found for " + rec_id)
            elif rec_type == 'c':
                if rec_id == "dodge":
                    level = char.dodge()
                elif rec_id in list(rules.calculated_attributes.keys()):
                    level = rules.calc_attr(rec_id, char.traitList())
                else:
                    level = -1
                skill_out = CustomSkill(char, rec_id, rec_id, level)
        return skill_out

    def processForm(self):
        global _title

        quick = False
        if "chk_quick" in self.defaults.form:
            quick = (self.defaults.form["chk_quick"].value == 'on')

        # get the character record
        info = None
        my_skill = None
        my_mod = 0
        if self.defaults.id is not None:
            info = model.charInfo.get(self.defaults.id)
            # get the selected skill
            if 'sel_skill' in self.defaults.form:
                my_skill = self.getSkill(self.defaults.form['sel_skill'].value,
                                         info)
            # get the modifier
            if 'txt_my_mod' in self.defaults.form and my_skill is not None:
                try:
                    my_skill.mod = int(self.defaults.form['txt_my_mod'].value)
                except ValueError:
                    pass

        # get the opponent record if one has been selected
        opponent = None
        op_skill = None
        op_mod = 0
        if 'sel_opponent' in self.defaults.form:
            sel_opponent = self.defaults.form['sel_opponent'].value
            if sel_opponent != "None":
                try:
                    opponent = model.charInfo.get(sel_opponent)
                except sqlobject.SQLObjectNotFound:
                    self.defaults.alerts.append("Error: ID %s not found in database" % (sel_opponent, ))
                # get the opponents skill
                if 'sel_op_skill' in self.defaults.form:
                    op_skill = self.getSkill(self.defaults.form['sel_op_skill'].value, opponent)
                # get the modifier
                if 'txt_op_mod' in self.defaults.form and op_skill is not None:
                    try:
                        op_skill.mod = int(self.defaults.form['txt_op_mod'].value)
                    except ValueError:
                        pass

        results = None
        if my_skill is not None and op_skill is not None:
            results = ContestResult(my_skill, op_skill, quick)

        self.myvars = {
            'info': info,
            'my_skill': my_skill,
            'opponent': opponent,
            'op_skill': op_skill,
            'results': results,
            'quick': quick,
            'char_menu': auth.charMenu(self.defaults.session_id),
            }            

        self.defaults.action = my_name
        self.defaults.jsfile = ('log.js', 'ajax.js', 'contest.js')
        self.defaults.no_add = True


if __name__ == '__main__':
    init_db(load_db_config())
    SkillContestPage(SkillRollContest, save_id=True).main()
