#!/usr/bin/env python3
"""base class for scripts responding with XML data

Intended to be used to respond to an AJAX request (see utils/ajax.js
and utils/log.js).

"""

from page import DefaultPage


class XmlResponse(DefaultPage):
    def __init__(self, *arg, **kwarg):
        super().__init__(*arg, **kwarg)
        self.response = ""

    def processForm(self):
        """process incoming CGI data and update the response string"""
        pass

    def headers(self):
        """Print out the XML header"""
        print("Cache-Control: no-cache")
        print("Content-Type: application/xml")
        print("")

    def respond(self):
        print('<?xml version="1.0" ?>')
        print('<response>')
        print(self.response)
        print('</response>')

    def onError(self, val):
        self.response += "<error>%s</error>" % val

    def main(self):
        """Check the session and then process the form to send back"""
        if self.login():
            self.headers()
            try:
                self.processForm()
            except Exception as val:
                self.onError(val)
            self.respond()
