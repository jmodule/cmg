#!/usr/bin/env python3

import cgi
import datetime

import model
from model.connect import (load_db_config, init_db)

from jmodule.utils import debug, menu
from jmodule.web import login, html
from db import category, char, equipment
import rules
import const

from templates.LogDice import LogDice

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__

_title = "Log Dice Roll Results"


def main():
    """Display a form to accept die rolls against a character's skills"""

    session_id = login.check('cmg.py')
    if session_id != -1:
        try:
            # see if the fields are set for the first page this displays
            if 'skill' in login.session_file.form:
                skill = login.session_file.form['skill'].value
            else:
                skill = None

            if 'level' in login.session_file.form:
                level = int(login.session_file.form['level'].value)
            else:
                level = 0

            if 'roll' in login.session_file.form:
                roll = int(login.session_file.form['roll'].value)
            else:
                roll = 0

            if 'id' in login.session_file.form:
                char_id = int(login.session_file.form['id'].value)
            else:
                char_id = 0

            if 'txt_comment' in login.session_file.form:
                comment = login.session_file.form['txt_comment'].value
            else:
                comment = ""

            if 'target' in login.session_file.form:
                comment += 'Attacking %s. ' % login.session_file.form['target'].value

            if 'defense' in login.session_file.form:
                comment += "%s. " % login.session_file.form['defense'].value
            
            for key in ('damage', 'fatigue'):
                if key in login.session_file.form:
                    try:
                        v = int(login.session_file.form[key].value)
                    except ValueError:
                        v = 0
                    if v >= 0:
                        comment += key.capitalize() + "=" + str(v) + ". "

            html.begin(title=_title, stylesheet=const.css['cmg'])

            # debug.report('debug', 'main', 'form', login.session_file.form)

            # See if we need to do anything with the record
            #
            if 'cmd_save' in login.session_file.form:
                model.log(
                    charID = char_id,
                    categID = const.log_id_roll,
                    logtime = datetime.datetime.now(),
                    skill = skill,
                    level = level,
                    roll = roll,
                    comment = comment)
                
                html.p('Record Saved', align="center")
                html.p('<input type="button" value="Close" onClick="javascript:window.close()">', align="center")
                html.end()

            else:
                # otherwise display a comment form
                myvars = {
                    'action': my_name,
                    'id': char_id,
                    'level': level,
                    'roll': roll,
                    'comment': comment,
                    'session_id': session_id,
                    'skill': skill,
                    'logout': "%s?logout=True&session_id=%s" % (my_name, session_id)
                    }
                print(LogDice(searchList=[myvars]))
        except Exception:
            html.begin(title="Error on Log Dice Results page",
                       stylesheet=const.css['cmg'])
            cgi.print_exception()


if __name__ == "__main__":
    init_db(load_db_config())
    main()
