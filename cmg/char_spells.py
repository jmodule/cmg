#!/usr/bin/env python3

import auth
import model
from model.connect import (load_db_config, init_db)
from page import DefaultPage
import sqlobject
from templates.Edit import Edit

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__


class SpellPage(DefaultPage):
    """This displays a detail page with all the spells a character knows"""
    def processForm(self):
        self.defaults.title = "Spell List"
        
        html_out = ""
        if self.defaults.id is not None:
            info = model.charInfo.get(self.defaults.id)
            self.defaults.title += ' for ' + info.charName
            
            categ = model.category.byDescription('Magic')
            for spell in model.charSkills.select(
                sqlobject.AND(model.charSkills.q.charID == self.defaults.id,
                              model.skill.q.categoryID == categ.id,
                              model.charSkills.q.skillID == model.skill.q.id),
                    orderBy=model.skill.q.name):

                html_out += """<p><b>%s</b> - %s/%s - %s<br>
                College: %s<br>
                Mana cost: %s/%s<br>
                Time to cast: %s<br>
                Duration: %s<br>
                Notes: %s</p>""" % (spell.skill.name,
                                    spell.skill.traitType,
                                    spell.skill.diffCode,
                                    spell.gameLevel(),
                                    spell.skill.college,
                                    spell.skill.castingCost,
                                    spell.skill.maintainCost,
                                    spell.skill.castingTime,
                                    spell.skill.duration,
                                    spell.skill.notes)

        self.defaults.action = my_name
        self.defaults.no_add = True

        self.myvars = { 'char_menu': [],
                        'bodycontent': html_out }


if __name__ == "__main__":
    init_db(load_db_config())
    SpellPage(Edit, save_id=True).main()
