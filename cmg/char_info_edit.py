#!/usr/bin/env python3

import auth
from db import form
from jmodule.web import html
import model
from model.connect import (load_db_config, init_db)
from page import DefaultPage
from templates.CharEdit import CharEdit

try:
    my_name = __file__[__file__.rindex("/")+1:]
except IndexError:
    my_name = __file__

hide_fields = ['height',
               'weight',
               'unspentPoints',
               'parryID',
               'health',
               'genre',
               'fatigue',
               'username',
               'blockID']


class EditPage(DefaultPage):
    def processForm(self):
        self.defaults.action = my_name
        self.defaults.always_show_menu = True
        
        content = ""

        if 'session_id' in self.defaults.form:
            session_id = self.defaults.form['session_id'].value
        else:
            session_id = -1

        # See if we need to do anything with the record
        #
        if 'cmd_update' in self.defaults.form:
            self.update(model.charInfo)
            # update the user name now
            u = model.charInfo.get(self.defaults.id).userName()
            self.defaults.alerts.append('Record Updated')
            
        content += '<form name="info" action="' + my_name + '" method="post">'
        content += form.fill(model.charInfo,
                             id=self.defaults.id,
                             hide=hide_fields)
        content += html.hidden('session_id', session_id)
        content += '</form>'

        self.myvars = {
            'bodycontent': content,
            'char_menu': auth.charMenu(self.defaults.session_id),
            'title': 'Edit Character Information',
            }


if __name__ == "__main__":
    init_db(load_db_config())
    EditPage(CharEdit, save_id=True).main()
