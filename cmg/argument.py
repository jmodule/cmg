import random

max_tries = 20000
tries = 0

sides = ('pro', 'con', 'abs')
parts = ('argument', 'strength')


class Argument:
    """Holds the data for one argument"""

    def __init__(self, argument=None, strength=0):
        """Set up the argument with default values"""
        self.argument = argument
        self.strength = strength
        self.result = 0
        self.success = False

    def __str__(self):
        return "%s (str %d) = %s (roll %d)" % (self.argument,
                                               self.strength,
                                               self.success,
                                               self.result)

    def test(self):
        """Generate a success result"""
        if self.argument is not None:
            self.result = random.randint(1, 5)
            self.success = (self.result <= int(self.strength))
        return self.success

    def valid(self):
        """Return true if this argument has some text"""
        if self.argument is not None:
            return True
        else:
            return False


def decide(arguments, debug=False):
    """return false if tries were exceeded when trying to determine winner

    Runs through the list of arguments and tests each for success
    until only one is the 'winner'.  The actual results are stored in
    each object in the argument hash.

    """
    global tries
    num_success = 0
    finished = False
    while tries < max_tries and not finished:
        tries += 1
        for side in list(arguments.keys()):
            if arguments[side].test():
                num_success += 1
        if num_success == 1:
            finished = True
        else:
            num_success = 0
        if debug:
            print("Attempt %d, number succeeded: %d" % (tries, num_success))

    return tries < max_tries


if __name__ == '__main__':
    """For testing purposes"""
    args = {'pro': Argument('This is a test    ', 2),
            'con': Argument('This is not a test', 3),
            'abs': Argument('Who cares!        ', 1)}

    print("-" * 79)
    print()

    result = decide(args, True)

    if not result:
        print("Unable to Determine a result after %d tries!" % tries)

    print()
    print("-" * 79)
    print("Result")
    print()

    for k in list(args.keys()):
        print(k, args[k])

    print("-" * 79)
